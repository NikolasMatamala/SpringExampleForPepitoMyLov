USE [accionistas]
GO
/****** Object:  User [marcelo]    Script Date: 10/26/2017 10:07:49 ******/
CREATE USER [marcelo] FOR LOGIN [marcelo] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Schema [m2ss]    Script Date: 10/26/2017 10:07:49 ******/
CREATE SCHEMA [m2ss] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [accionistas]    Script Date: 10/26/2017 10:07:49 ******/
CREATE SCHEMA [accionistas] AUTHORIZATION [dbo]
GO
/****** Object:  Table [dbo].[Tipogirocomercial]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipogirocomercial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tipogirocomercial] ON
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (1, N'Agricultura, ganaderia, caza y silvicultura')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (2, N'Pesca')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (3, N'Explotacion de minas y canteras')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (4, N'Industrias manufactureras no metalicas')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (5, N'Industrias manufactureras metalicas')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (6, N'Suministro de electricidad, gas y agua')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (7, N'Construccion')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (8, N'Comercio al por mayor y menor; Rep. vehiculos automores/enseres domesticos')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (9, N'Hoteles y restaurantes')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (10, N'Transporte, almacenamiento y comunicaciones')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (11, N'Intermediacion financiera')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (12, N'Actividades inmobiliarias, empresariales y de alquiler')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (13, N'Adm. publica y defensa; Planes de seguridad social, afiliacion obligatoria')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (14, N'Enseñanza')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (15, N'Servicios sociales y de salud')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (16, N'Otras actividades de servicios comunitarias, sociales y personales')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (17, N'Consejo de administracion de edificios y condominios')
INSERT [dbo].[Tipogirocomercial] ([id], [descripcion]) VALUES (18, N'Organizaciones y organos extraterritoriales')
SET IDENTITY_INSERT [dbo].[Tipogirocomercial] OFF
/****** Object:  Table [dbo].[Tipoaccionista]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tipoaccionista](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[descripcion_tipo_accionista] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tipoaccionista] ON
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (1, N'Persona natural nacional')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (2, N'Persona natural extranjera')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (3, N'Sociedad anonima abierta')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (4, N'Otro tipo de sociedad')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (5, N'Fondos mutuos')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (6, N'Persona juridica extranjera')
INSERT [dbo].[Tipoaccionista] ([id], [descripcion_tipo_accionista]) VALUES (7, N'Estado de Chile')
SET IDENTITY_INSERT [dbo].[Tipoaccionista] OFF
/****** Object:  Table [dbo].[Testigo]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Testigo](
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[mail] [varchar](50) NOT NULL,
	[fono] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Testigo] ([rut], [nombre], [apellido_paterno], [apellido_materno], [mail], [fono]) VALUES (N'16987321-4', N'Eric', N'Menke', N'Gonzales', N'emenke@dsic.cl', N'+56932165498')
/****** Object:  Table [dbo].[Representantelegal]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Representantelegal](
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[apellido_paterno] [varchar](20) NOT NULL,
	[apellido_materno] [varchar](20) NULL,
	[direccion] [varchar](100) NOT NULL,
	[mail] [varchar](100) NOT NULL,
	[telefono] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Representantelegal] ([rut], [nombre], [apellido_paterno], [apellido_materno], [direccion], [mail], [telefono]) VALUES (N'18196977-6', N'Manuel', N'Neira', N'Ascencio', N'Illapel 01060', N'manuel.neira@gmail.com', N'+56961497336')
/****** Object:  Table [dbo].[Nacionalidad]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nacionalidad](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nacionalidad] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Nacionalidad] ON
INSERT [dbo].[Nacionalidad] ([id], [nacionalidad]) VALUES (1, N'Chilena')
INSERT [dbo].[Nacionalidad] ([id], [nacionalidad]) VALUES (2, N'Extranjera sin imp')
INSERT [dbo].[Nacionalidad] ([id], [nacionalidad]) VALUES (3, N'Extranjera con imp')
INSERT [dbo].[Nacionalidad] ([id], [nacionalidad]) VALUES (4, N'Sin registro')
SET IDENTITY_INSERT [dbo].[Nacionalidad] OFF
/****** Object:  Table [dbo].[Juntaaccionista]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Juntaaccionista](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ubicacion] [varchar](100) NOT NULL,
	[fecha_realizacion] [date] NOT NULL,
	[hora_inicio] [time](0) NULL,
	[hora_termino] [time](0) NULL,
	[direccion] [varchar](100) NOT NULL,
	[fecha_difusion] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Juntaaccionista] ON
INSERT [dbo].[Juntaaccionista] ([id], [ubicacion], [fecha_realizacion], [hora_inicio], [hora_termino], [direccion], [fecha_difusion]) VALUES (2, N'Las condes', CAST(0x2F330B00 AS Date), CAST(0x00F0D20000000000 AS Time), CAST(0x0000E10000000000 AS Time), N'Los traperos 0143', CAST(0xE3320B00 AS Date))
INSERT [dbo].[Juntaaccionista] ([id], [ubicacion], [fecha_realizacion], [hora_inicio], [hora_termino], [direccion], [fecha_difusion]) VALUES (3, N'Lo prado', CAST(0x6C3D0B00 AS Date), CAST(0x00F0D20000000000 AS Time), CAST(0x00B40E0100000000 AS Time), N'Prueba 1234', CAST(0x6A3D0B00 AS Date))
SET IDENTITY_INSERT [dbo].[Juntaaccionista] OFF
/****** Object:  Table [dbo].[Usuario]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](15) NOT NULL,
	[password] [varchar](15) NOT NULL,
	[nivel] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT [dbo].[Usuario] ([id], [username], [password], [nivel]) VALUES (8, N'admin', N'admin', 1)
INSERT [dbo].[Usuario] ([id], [username], [password], [nivel]) VALUES (9, N'Eduardo', N'edulove', 3)
INSERT [dbo].[Usuario] ([id], [username], [password], [nivel]) VALUES (11, N'erick', N'elmascapo', 5)
INSERT [dbo].[Usuario] ([id], [username], [password], [nivel]) VALUES (12, N'Marcelo', N'marcheloco', 1)
INSERT [dbo].[Usuario] ([id], [username], [password], [nivel]) VALUES (15, N'admin', N'admin', 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  Table [dbo].[users]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users](
	[username] [varchar](45) NOT NULL,
	[enabled] [bit] NOT NULL,
	[password] [varchar](60) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[users] ([username], [enabled], [password]) VALUES (N'admin', 1, N'$2a$10$jXrd8Vp8VbtrSavjycZzD.g1e6P9xCitFfi1HZtXtT4zQp2wy5cN.')
INSERT [dbo].[users] ([username], [enabled], [password]) VALUES (N'framirez', 1, N'$2a$10$hqYTsKSBF9rgDeVpcZW4IuWhZ/TWmjM2ncs.Xc8MpmdLxImD2XgsK')
/****** Object:  Table [dbo].[Votacionjunta]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Votacionjunta](
	[id_votacion] [int] IDENTITY(1,1) NOT NULL,
	[juntaAccionistaid_junta] [int] NOT NULL,
	[cantidad_votantes] [int] NOT NULL,
	[votos_positivos] [int] NOT NULL,
	[votos_negativos] [int] NOT NULL,
	[observacion] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_votacion] ASC,
	[juntaAccionistaid_junta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user_roles]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_roles](
	[user_role_id] [int] IDENTITY(1,1) NOT NULL,
	[role] [varchar](45) NOT NULL,
	[username] [varchar](45) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UKstlxfukw77ov5w1wo1tm3omca] UNIQUE NONCLUSTERED 
(
	[role] ASC,
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[user_roles] ON
INSERT [dbo].[user_roles] ([user_role_id], [role], [username]) VALUES (3, N'ROLE_USER', N'admin')
SET IDENTITY_INSERT [dbo].[user_roles] OFF
/****** Object:  Table [dbo].[Empresa]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut] [varchar](12) NOT NULL,
	[razon_social] [varchar](70) NOT NULL,
	[logo] [varchar](255) NULL,
	[web] [varchar](100) NOT NULL,
	[cantidad_acciones_seriea] [int] NOT NULL,
	[cantidad_acciones_serieb] [int] NOT NULL,
	[cantidad_accionistas] [int] NOT NULL,
	[mail] [varchar](100) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[telefono] [varchar](20) NULL,
	[representante_legalrut] [varchar](12) NOT NULL,
	[tipo_giro_comercialid] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [representante_legalrut] UNIQUE NONCLUSTERED 
(
	[representante_legalrut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [tipo_giro_comercialid] UNIQUE NONCLUSTERED 
(
	[tipo_giro_comercialid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Empresa] ON
INSERT [dbo].[Empresa] ([id], [rut], [razon_social], [logo], [web], [cantidad_acciones_seriea], [cantidad_acciones_serieb], [cantidad_accionistas], [mail], [direccion], [telefono], [representante_legalrut], [tipo_giro_comercialid]) VALUES (2, N'99999999-9', N'Sociedad Hipodromo Chile S.A', N'', N'www.hipodromo.cl', 60000, 300, 450, N'contacto@hipodromochile.cl', N'Av. Hipodromo chile N° 1715', N'227369275', N'18196977-6', 16)
SET IDENTITY_INSERT [dbo].[Empresa] OFF
/****** Object:  Table [dbo].[Acta]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Acta](
	[id_acta] [int] IDENTITY(1,1) NOT NULL,
	[juntaAccionistaid_junta] [int] NOT NULL,
	[titulo_acta] [varchar](255) NOT NULL,
	[detalle_acta] [varchar](255) NOT NULL,
	[url_documento] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_acta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accion]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[valora] [real] NOT NULL,
	[valorb] [real] NULL,
	[empresaid_empresa] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Accion] ON
INSERT [dbo].[Accion] ([id], [valora], [valorb], [empresaid_empresa]) VALUES (4, 270000, 3000000, 2)
SET IDENTITY_INSERT [dbo].[Accion] OFF
/****** Object:  Table [dbo].[Accionista]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionista](
	[rut] [varchar](12) NOT NULL,
	[empresaid_empresa] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[cantidad_acciones_serie_a] [int] NOT NULL,
	[cantidad_acciones_serie_b] [int] NOT NULL,
	[porcentaje_participacion] [real] NOT NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[es_valido] [varchar](10) NOT NULL,
	[mail] [varchar](150) NOT NULL,
	[direccion] [varchar](100) NULL,
	[fono] [varchar](20) NULL,
	[nacionalidadid] [int] NOT NULL,
	[tipo_accionistaid] [int] NOT NULL,
	[ciudad] [varchar](100) NULL,
	[comuna] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Accionista] ([rut], [empresaid_empresa], [nombre], [apellido_paterno], [apellido_materno], [cantidad_acciones_serie_a], [cantidad_acciones_serie_b], [porcentaje_participacion], [fecha_nacimiento], [es_valido], [mail], [direccion], [fono], [nacionalidadid], [tipo_accionistaid], [ciudad], [comuna]) VALUES (N'11111111-1', 2, N'Jonathan', N'Gutierrez', N'Sandoval', 427, 0, 0.5, CAST(0x361F0B00 AS Date), N'S         ', N'j.gutierrez@gmail.com', N'Mi casa 0990', N'+56988554466', 1, 1, N'Santiago', N'Santiago')
INSERT [dbo].[Accionista] ([rut], [empresaid_empresa], [nombre], [apellido_paterno], [apellido_materno], [cantidad_acciones_serie_a], [cantidad_acciones_serie_b], [porcentaje_participacion], [fecha_nacimiento], [es_valido], [mail], [direccion], [fono], [nacionalidadid], [tipo_accionistaid], [ciudad], [comuna]) VALUES (N'22222222-2', 2, N'Pedro', N'Manriquez', N'Guzman', 6153, 1823, 1.5, CAST(0xF6150B00 AS Date), N'S         ', N'p.manriquez@gmail.com', N'tu casa 112', N'+56988336611', 1, 2, N'Temuco', N'Temuco')
INSERT [dbo].[Accionista] ([rut], [empresaid_empresa], [nombre], [apellido_paterno], [apellido_materno], [cantidad_acciones_serie_a], [cantidad_acciones_serie_b], [porcentaje_participacion], [fecha_nacimiento], [es_valido], [mail], [direccion], [fono], [nacionalidadid], [tipo_accionistaid], [ciudad], [comuna]) VALUES (N'3333333-3', 2, N'admin', N'Fuentes', N'asd', 5535, 3213, 0.5, CAST(0x5C3D0B00 AS Date), N'Si', N'cfuntes@gmail.com', N'volcan llaima #599', N'+56911223214', 2, 1, N'Santiago', N'Talagante')
/****** Object:  Table [dbo].[Traspasoacciones]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Traspasoacciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut_accionista_vendedor] [varchar](12) NOT NULL,
	[rut_accionista_comprador] [varchar](12) NOT NULL,
	[fecha_venta] [date] NOT NULL,
	[cantidad_acciones_vendidas_a] [int] NOT NULL,
	[cantidad_acciones_vendidas_b] [int] NOT NULL,
	[monto] [real] NOT NULL,
	[es_sucesion] [varchar](10) NOT NULL,
	[testigorut] [varchar](12) NULL,
	[observacion] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Traspasoacciones] ON
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (1, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (2, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (3, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (4, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (5, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (6, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (7, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (8, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (9, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (10, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (11, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (12, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (13, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (14, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (15, N'11111111-1', N'22222222-2', CAST(0x633D0B00 AS Date), 0, 1, 105023, N'N         ', N'16987321-4', N'Primer traspaso')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (18, N'11111111-1', N'22222222-2', CAST(0x5F3D0B00 AS Date), 10, 10, 1000, N'No', N'16987321-4', N'El mas capo')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (19, N'11111111-1', N'22222222-2', CAST(0x653D0B00 AS Date), 10, 20, 1000, N'Si', N'16987321-4', N'El mas capo')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (20, N'11111111-1', N'11111111-1', CAST(0x5B950A00 AS Date), 10, 10, 1000, N'Si', N'16987321-4', N'Prueba')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (21, N'11111111-1', N'22222222-2', CAST(0x663D0B00 AS Date), 10, 0, 1000, N'Si', N'16987321-4', N'El mas capo')
INSERT [dbo].[Traspasoacciones] ([id], [rut_accionista_vendedor], [rut_accionista_comprador], [fecha_venta], [cantidad_acciones_vendidas_a], [cantidad_acciones_vendidas_b], [monto], [es_sucesion], [testigorut], [observacion]) VALUES (22, N'11111111-1', N'3333333-3', CAST(0x653D0B00 AS Date), 70, 0, 100, N'Si', N'16987321-4', N'El mas capo 2')
SET IDENTITY_INSERT [dbo].[Traspasoacciones] OFF
/****** Object:  Table [dbo].[Dividendo]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dividendo](
	[accionistarut] [varchar](12) NOT NULL,
	[fecha_declaracion] [date] NOT NULL,
	[fecha_registro] [date] NOT NULL,
	[dividendo_por_accion] [real] NOT NULL,
	[dividendo_total] [real] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cuentacorriente]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cuentacorriente](
	[numero_cuenta] [varchar](20) NOT NULL,
	[banco] [varchar](30) NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[numero_cuenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Apoderado]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Apoderado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut] [varchar](12) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[mail] [varchar](100) NOT NULL,
	[fono] [varchar](20) NULL,
	[accionistarut] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accionesconprohibicion]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionesconprohibicion](
	[accionistarut] [varchar](12) NOT NULL,
	[cantidad_acciones_prohibicion] [int] NOT NULL,
	[cantidad_acciones_prohibicion_b] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Heredero]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Heredero](
	[rut] [varchar](12) NOT NULL,
	[Empresaid_empresa] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido_paterno] [varchar](50) NOT NULL,
	[apellido_materno] [varchar](50) NULL,
	[cantidad_acciones_serie_a] [int] NULL,
	[cantidad_acciones_serie_b] [int] NULL,
	[porcentaje_participacion] [real] NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[es_valido] [varchar](10) NOT NULL,
	[mail] [varchar](150) NOT NULL,
	[direccion] [varchar](100) NULL,
	[fono] [varchar](20) NULL,
	[ciudad] [varchar](100) NOT NULL,
	[comuna] [varchar](100) NOT NULL,
	[nacionalidadid] [int] NOT NULL,
	[tipoAccionistaid] [int] NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[rut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Accionista_juntaaccionista]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accionista_juntaaccionista](
	[accionistarut] [varchar](12) NOT NULL,
	[junta_accionista_junta] [int] NOT NULL,
	[asistencia_accionista] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[accionistarut] ASC,
	[junta_accionista_junta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Historialtitulo]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Historialtitulo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
	[detalle_solicitud] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mandatario]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mandatario](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rut_mandatario] [varchar](12) NOT NULL,
	[razon_social] [varchar](150) NULL,
	[fecha_nacimiento] [date] NOT NULL,
	[fecha_ingreso] [date] NOT NULL,
	[fecha_salida] [date] NULL,
	[cantidad_acciones] [int] NOT NULL,
	[procentaje_participacion] [real] NOT NULL,
	[accionistarut] [varchar](12) NOT NULL,
	[porcentaje_participacion] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Mandatario] ON
INSERT [dbo].[Mandatario] ([id], [rut_mandatario], [razon_social], [fecha_nacimiento], [fecha_ingreso], [fecha_salida], [cantidad_acciones], [procentaje_participacion], [accionistarut], [porcentaje_participacion]) VALUES (1, N'3333333-3', N'Los mandamaz S.A.', CAST(0x20330B00 AS Date), CAST(0x8D340B00 AS Date), CAST(0x5B950A00 AS Date), 3500, 0.6, N'11111111-1', NULL)
SET IDENTITY_INSERT [dbo].[Mandatario] OFF
/****** Object:  Table [dbo].[Juntaaccionista_apoderado]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Juntaaccionista_apoderado](
	[juntaAccionistaid_junta] [int] NOT NULL,
	[apoderadoid] [int] NOT NULL,
	[asistencia_apoderado] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[juntaAccionistaid_junta] ASC,
	[apoderadoid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Historialdividendo]    Script Date: 10/26/2017 10:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Historialdividendo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dividendoAccionistarut] [varchar](12) NOT NULL,
	[fecha_delaracion] [timestamp] NOT NULL,
	[fecha_registro] [datetime] NOT NULL,
	[dividendo_por_accion] [real] NOT NULL,
	[dividendo_total] [real] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[dividendoAccionistarut] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_empresaid_empresa_id]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accion]  WITH CHECK ADD  CONSTRAINT [FK_empresaid_empresa_id] FOREIGN KEY([empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Accion] CHECK CONSTRAINT [FK_empresaid_empresa_id]
GO
/****** Object:  ForeignKey [FKaccionesCo852416]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionesconprohibicion]  WITH CHECK ADD  CONSTRAINT [FKaccionesCo852416] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Accionesconprohibicion] CHECK CONSTRAINT [FKaccionesCo852416]
GO
/****** Object:  ForeignKey [FKaccionista93694]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista93694] FOREIGN KEY([nacionalidadid])
REFERENCES [dbo].[Nacionalidad] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista93694]
GO
/****** Object:  ForeignKey [FKaccionista940287]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista940287] FOREIGN KEY([empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista940287]
GO
/****** Object:  ForeignKey [FKaccionista99327]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista99327] FOREIGN KEY([tipo_accionistaid])
REFERENCES [dbo].[Tipoaccionista] ([id])
GO
ALTER TABLE [dbo].[Accionista] CHECK CONSTRAINT [FKaccionista99327]
GO
/****** Object:  ForeignKey [FKaccionista489118]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionista_juntaaccionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista489118] FOREIGN KEY([junta_accionista_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Accionista_juntaaccionista] CHECK CONSTRAINT [FKaccionista489118]
GO
/****** Object:  ForeignKey [FKaccionista879535]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Accionista_juntaaccionista]  WITH CHECK ADD  CONSTRAINT [FKaccionista879535] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Accionista_juntaaccionista] CHECK CONSTRAINT [FKaccionista879535]
GO
/****** Object:  ForeignKey [FKacta278928]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Acta]  WITH CHECK ADD  CONSTRAINT [FKacta278928] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Acta] CHECK CONSTRAINT [FKacta278928]
GO
/****** Object:  ForeignKey [FKapoderado107487]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Apoderado]  WITH CHECK ADD  CONSTRAINT [FKapoderado107487] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Apoderado] CHECK CONSTRAINT [FKapoderado107487]
GO
/****** Object:  ForeignKey [FKcuentaCorr897980]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Cuentacorriente]  WITH CHECK ADD  CONSTRAINT [FKcuentaCorr897980] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Cuentacorriente] CHECK CONSTRAINT [FKcuentaCorr897980]
GO
/****** Object:  ForeignKey [FKdividendo866504]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Dividendo]  WITH CHECK ADD  CONSTRAINT [FKdividendo866504] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Dividendo] CHECK CONSTRAINT [FKdividendo866504]
GO
/****** Object:  ForeignKey [FK_representante_legalrut_rut]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_representante_legalrut_rut] FOREIGN KEY([representante_legalrut])
REFERENCES [dbo].[Representantelegal] ([rut])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_representante_legalrut_rut]
GO
/****** Object:  ForeignKey [FK_tipo_giro_comercialid_id]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_tipo_giro_comercialid_id] FOREIGN KEY([tipo_giro_comercialid])
REFERENCES [dbo].[Tipogirocomercial] ([id])
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_tipo_giro_comercialid_id]
GO
/****** Object:  ForeignKey [FK_Heredero_Accionista]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Accionista] FOREIGN KEY([rut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Accionista]
GO
/****** Object:  ForeignKey [FK_Heredero_Nacionalidad]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Nacionalidad] FOREIGN KEY([nacionalidadid])
REFERENCES [dbo].[Nacionalidad] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Nacionalidad]
GO
/****** Object:  ForeignKey [FK_Heredero_Tipoaccionista]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FK_Heredero_Tipoaccionista] FOREIGN KEY([tipoAccionistaid])
REFERENCES [dbo].[Tipoaccionista] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FK_Heredero_Tipoaccionista]
GO
/****** Object:  ForeignKey [FKheredero240757]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Heredero]  WITH CHECK ADD  CONSTRAINT [FKheredero240757] FOREIGN KEY([Empresaid_empresa])
REFERENCES [dbo].[Empresa] ([id])
GO
ALTER TABLE [dbo].[Heredero] CHECK CONSTRAINT [FKheredero240757]
GO
/****** Object:  ForeignKey [FKhistorialD2868]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Historialdividendo]  WITH CHECK ADD  CONSTRAINT [FKhistorialD2868] FOREIGN KEY([dividendoAccionistarut])
REFERENCES [dbo].[Dividendo] ([accionistarut])
GO
ALTER TABLE [dbo].[Historialdividendo] CHECK CONSTRAINT [FKhistorialD2868]
GO
/****** Object:  ForeignKey [FKhistorialT736912]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Historialtitulo]  WITH CHECK ADD  CONSTRAINT [FKhistorialT736912] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Historialtitulo] CHECK CONSTRAINT [FKhistorialT736912]
GO
/****** Object:  ForeignKey [FKjuntaAccio71301]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Juntaaccionista_apoderado]  WITH CHECK ADD  CONSTRAINT [FKjuntaAccio71301] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Juntaaccionista_apoderado] CHECK CONSTRAINT [FKjuntaAccio71301]
GO
/****** Object:  ForeignKey [FKjuntaAccio915362]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Juntaaccionista_apoderado]  WITH CHECK ADD  CONSTRAINT [FKjuntaAccio915362] FOREIGN KEY([apoderadoid])
REFERENCES [dbo].[Apoderado] ([id])
GO
ALTER TABLE [dbo].[Juntaaccionista_apoderado] CHECK CONSTRAINT [FKjuntaAccio915362]
GO
/****** Object:  ForeignKey [FKmandatario944291]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Mandatario]  WITH CHECK ADD  CONSTRAINT [FKmandatario944291] FOREIGN KEY([accionistarut])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Mandatario] CHECK CONSTRAINT [FKmandatario944291]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Accionista]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Accionista] FOREIGN KEY([rut_accionista_vendedor])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Accionista]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Accionista1]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Accionista1] FOREIGN KEY([rut_accionista_comprador])
REFERENCES [dbo].[Accionista] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Accionista1]
GO
/****** Object:  ForeignKey [FK_Traspasoacciones_Testigo]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Traspasoacciones]  WITH CHECK ADD  CONSTRAINT [FK_Traspasoacciones_Testigo] FOREIGN KEY([testigorut])
REFERENCES [dbo].[Testigo] ([rut])
GO
ALTER TABLE [dbo].[Traspasoacciones] CHECK CONSTRAINT [FK_Traspasoacciones_Testigo]
GO
/****** Object:  ForeignKey [FKcdp2dxqcsdh6rnh6o64rgtcir]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[user_roles]  WITH CHECK ADD  CONSTRAINT [FKcdp2dxqcsdh6rnh6o64rgtcir] FOREIGN KEY([username])
REFERENCES [dbo].[users] ([username])
GO
ALTER TABLE [dbo].[user_roles] CHECK CONSTRAINT [FKcdp2dxqcsdh6rnh6o64rgtcir]
GO
/****** Object:  ForeignKey [FKvotacionJu814254]    Script Date: 10/26/2017 10:07:49 ******/
ALTER TABLE [dbo].[Votacionjunta]  WITH CHECK ADD  CONSTRAINT [FKvotacionJu814254] FOREIGN KEY([juntaAccionistaid_junta])
REFERENCES [dbo].[Juntaaccionista] ([id])
GO
ALTER TABLE [dbo].[Votacionjunta] CHECK CONSTRAINT [FKvotacionJu814254]
GO
