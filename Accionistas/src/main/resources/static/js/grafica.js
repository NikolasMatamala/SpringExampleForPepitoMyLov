
var dataPoints = [];
var dataPoints2 = [];
var dataPoints3 = [];
var dataPoints4 = [];
var datap=[];	
				var dataLength = 0;
                var data = [];
                var updateInterval = 500;
                updateChart();

                function updateChart() {
                    $.getJSON("/accionistas/listarAccionistas", function (result) {  
                    		console.log(result);
                            for (var i = 0; i < result.length; i++) {
                            	if(result[i].nombre == "graficotipoa"){
                            		var tipoA = jQuery.parseJSON(result[i].mensaje);
                            			for(var j = 0; j < tipoA.length; j++){
                            				dataPoints.push({                                   
                                                y: parseInt(tipoA[j].cantidad_acciones_serie_aModel),
                                                label: tipoA[j].nombreModel
                                            });
                            			}
                            		  
                            	}else if(result[i].nombre == "graficotipob"){
                            		var tipoB = jQuery.parseJSON(result[i].mensaje);
                        			for(var k = 0; k < tipoB.length; k++){
                        				
                        				dataPoints2.push({
                                        	y: parseInt(tipoB[k].cantidad_acciones_serie_bModel),
                                            label: tipoB[k].nombreModel
                                        });
                        			}
                            	}else if(result[i].nombre == "graficoTodos"){
                            		var todos = jQuery.parseJSON(result[i].mensaje);
                            		for(var x = 0; x < todos.length; x++){
                            			dataPoints3.push({
                            				y:parseInt(todos[x].cantidad_acciones_serie_aModel),
                            				label: todos[x].nombreModel
                            			});
                            			dataPoints4.push({
                            				y:parseInt(todos[x].cantidad_acciones_serie_bModel),
                            				label: todos[x].nombreModel
                            			});
                            		}
                            		
                            	}
                                                              
                            }  
                            
                            chart.render();
                            chart2.render();
                            chart3.render();
                        });
                }
                        
        	var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				exportEnabled: true,
				theme: "light1", // "light1", "light2", "dark1", "dark2"
				title:{
					text: "Grafico de Acciones Tipo B Top 10"
				},			
				data: [{
					type: "column", //change type to bar, line, area, pie, etc
					color: "#4f81bc",
					indexLabel: "{y}", //Shows y value on all Data Points
					indexLabelFontColor: "#5A5757",
					indexLabelPlacement: "outside",
					dataPoints: dataPoints2
				}]
//				{
//    				type: "column", //change type to bar, line, area, pie, etc
//					color: "#4f81bc",
//    				indexLabel: "{y}", //Shows y value on all Data Points
//    				indexLabelFontColor: "#5A5757",
//    				indexLabelPlacement: "outside",
//    				dataPoints: dataPoints
//    			}],
//				axisY:{
//			      prefix: "$",
//			      suffix: "K"
//			    }
			});
        	
        	var chart2 = new CanvasJS.Chart("chartContainer2", {
    			animationEnabled: true,
    			exportEnabled: true,
    			theme: "light1", // "light1", "light2", "dark1", "dark2"
    			title:{
    				text: "Grafico de Acciones Todos Los Accionistas"
    			},			
    			data: [{
    				type: "column", //change type to bar, line, area, pie, etc
    				color: "#E14345",
    				indexLabel: "{y}", //Shows y value on all Data Points
    				indexLabelFontColor: "#5A5757",
    				indexLabelPlacement: "outside",
    				dataPoints: dataPoints3
    			},
    			
    			{
    				type: "column", //change type to bar, line, area, pie, etc
					color: "#4f81bc",
    				indexLabel: "{y}", //Shows y value on all Data Points
    				indexLabelFontColor: "#5A5757",
    				indexLabelPlacement: "outside",
    				dataPoints: dataPoints4
    			}]
    			
    		});		
        	
        	var chart3 = new CanvasJS.Chart("chartContainer3", {
    			animationEnabled: true,
    			exportEnabled: true,
    			theme: "light1", // "light1", "light2", "dark1", "dark2"
    			title:{
    				text: "Grafico de Acciones Tipo A Top 10"
    			},			
    			data: [{
    				type: "column", //change type to bar, line, area, pie, etc
					color: "#E14345",
    				indexLabel: "{y}", //Shows y value on all Data Points
    				indexLabelFontColor: "#5A5757",
    				indexLabelPlacement: "outside",
    				dataPoints: dataPoints
    			}]
//    			{
//    				type: "column", //change type to bar, line, area, pie, etc
//					color: "#4f81bc",
//    				indexLabel: "{y}", //Shows y value on all Data Points
//    				indexLabelFontColor: "#5A5757",
//    				indexLabelPlacement: "outside",
//    				dataPoints: dataPoints2
//    			}],
//    			axisY:{
//  			      prefix: "$",
//  			      suffix: "K"
//  			    }
    		});