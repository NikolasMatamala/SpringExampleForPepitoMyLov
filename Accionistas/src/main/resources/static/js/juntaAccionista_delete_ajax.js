$(document).ready(function (){
	var juntaActual = -1;
	
	$(document).on('click', '.check', function(){
		var dato = $(this).attr('id');
		var estado = $(this).prop('checked');				
		dato = dato.replace('check','');		
		 var data = {id:dato, estadoActual:estado, idJunta: juntaActual};	
		updateAsistencia(data,dato);
		
	});	
	
	function updateAsistencia(data,dato){
		 $.ajax({
			 url: "/juntaaccionista/actAsistencia",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 $('#error'+dato).css('display','none');
				 $('#loader'+dato).css('display','inline');
			 },
			 success: function(resp){				 
				 respuesta = jQuery.parseJSON(resp);
				 console.log(respuesta);
				 estado = respuesta[0].mensaje;
				 asistencia = respuesta[1].mensaje;
				 if(estado != 'true' && estado != 'false'){
					 $('#error'+dato).css('display','inline');
					 $('#loader'+dato).css('display','none');	
					 
				 }else{
					 $('#loader'+dato).css('display','none');
					 
					 if(estado == 'true'){
						 $('#asistenciaState'+dato).text('Asistido');
					 }else{
						 $('#asistenciaState'+dato).text('No Asistido');
					 }					
				 }
				 console.log(juntaActual);
				 console.log($('#asistenciaActual'+juntaActual));
				 $('#asistenciaActual'+juntaActual).text(asistencia);
					
				
			},
			error: function(jqXHR, estado, error){
				$('#error'+dato).css('display','inline');	
				$('#loader'+dato).css('display','none');
			},
			complete: function(jqXHR, estado){				
				
			},
			timeout: 10000			 
		 });
	}
	
	$(".getasistencia").on('click',function(){
		 var dato = $(this).attr('id');		 
		 dato = dato.replace('asistencia','');
		 juntaActual = dato;		 
		 var data = {id:dato};		 
		 getAsistencia(data, dato);		
		 		 		 		
	});
	function getAsistencia(data, id){
		 $.ajax({
			 url: "/juntaaccionista/getAsistenciaJunta",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 								 				 				 
				 asistencias = jQuery.parseJSON(resp);
				 tabla = "";		
				 console.log(asistencias);
				 for (var i = 0; i < asistencias.length; i++) {
					 tabla += "<tr>";
					 tabla += "<td>"+asistencias[i].accionistaModel.rutModel+"</td>";
					 tabla += "<td>"+asistencias[i].accionistaModel.nombreModel+"</td>";
					 if(asistencias[i].apoderadoModel == null){
						 tabla += "<td>Sin Apoderado</td>";
						 tabla += "<td>Sin Apoderado</td>";
					 }else{
						 tabla += "<td>"+asistencias[i].apoderadoModel.rutModel+"</td>";
						 tabla += "<td>"+asistencias[i].apoderadoModel.nombreModel+"</td>";
					 }
					 
					 
					 if(asistencias[i].estadoAsistenciaJuntaModel == 1){
						 tabla += '<td id="asistenciaState'+asistencias[i].idAsistenciaModel+'" >Asistido</td>';
					 }else{
						 tabla += '<td id="asistenciaState'+asistencias[i].idAsistenciaModel+'">No asistido</td>';
					 }
					 if(asistencias[i].estadoAsistenciaJuntaModel == 0){
						 tabla += '<td style="padding-top:12px;"><input type="checkbox" name="" class="check" id="check'+asistencias[i].idAsistenciaModel+'" style="width: 150px; height:20px;"></input><img id="loader'+asistencias[i].idAsistenciaModel+'" style="display:none;width: 40px; height:40px;" src="/imgs/loader.gif"></img><img id="error'+asistencias[i].idAsistenciaModel+'" style="display:none;width: 40px; height:40px;" src="/imgs/error.png"></img></td>';
					 }else{
						 tabla += '<td style="padding-top:12px;"><input type="checkbox" checked="true" name="" class="check" id="check'+asistencias[i].idAsistenciaModel+'" style="width: 150px; height:20px;"></input><img id="loader'+asistencias[i].idAsistenciaModel+'" style="display:none;width: 40px; height:40px;" src="/imgs/loader.gif"></img><img id="error'+asistencias[i].idAsistenciaModel+'" style="display:none;width: 40px; height:40px;" src="/imgs/error.png"></img></td>';
						 
					 }
					 tabla += "</tr>";
                 }        
				 
				 $( "#cuerpoAsistencias" ).html(tabla);
			},
			
			error: function(jqXHR, estado, error){
				console.log(error);				
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
	
	
	
	
	$(".activar").on('click',function(){
		 var dato = $(this).attr('id');
		 dato = dato.replace('activar','');		 
		 var data = {id:dato};
		 $.confirm({
			    title: 'Junta Accionista',
			    content: '¿Desea Activar la junta de la fila : '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	activateUser: {
			    		text: 'Activar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	activate_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function activate_data(data, id){
		 $.ajax({
			 url: "/juntaaccionista/act",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 
				 $('#estado'+id).text('Activo');
				 $('#desactivar'+id).prop('enabled', true);
				 $('#desactivar'+id).prop('disabled', false);									
				 $('#activar'+id).prop('disabled', true);
				 $('#activar'+id).prop('enabled', false);
				 $('#edit'+id).prop("enabled", true);
				 $('#edit'+id).prop("disabled", false);
					
				 
				 iziToast.success({
					    title: 'Junta Accionista',
					    message: resp,
					});	
				
			},
			error: function(jqXHR, estado, error){
						
			},
			complete: function(jqXHR, estado){
					
			},
			timeout: 10000			 
		 });
	 }
	
	
	$(".eliminar").on('click',function(){
		 var dato = $(this).attr('id');
		 dato = dato.replace('desactivar','');
		 var data = {id:dato};
		 $.confirm({
			    title: 'Junta Accionista',
			    content: '¿Desea eliminar la junta de la fila: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	deleteUser: {
			    		text: 'Eliminar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	delete_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function delete_data(data, id){
		 $.ajax({
			 url: "/juntaaccionista/del",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 				
				$('#estado'+id).text('Eliminado');
			
				$('#desactivar'+id).prop('enabled', false);
				 $('#desactivar'+id).prop('disabled', true);									
				 $('#activar'+id).prop('disabled', false);
				 $('#activar'+id).prop('enabled', true);
				 $('#edit'+id).prop('enabled', false);
				 $('#edit'+id).prop('disabled', true);
				
				 iziToast.success({
					    title: 'Junta accionista',
					    message: resp,
					});	
			},
			error: function(jqXHR, estado, error){
								
			},
			complete: function(jqXHR, estado){
								
			},
			timeout: 10000			 
		 });
	 }
});

