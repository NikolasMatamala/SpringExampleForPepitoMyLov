$(document).ready(function (){
	
	$(".activar").on('click',function(){
		 var dato = $(this).attr('id');
		 dato = dato.replace('activar','');
		 var data = {rut:dato};
		 $.confirm({
			    title: 'Testigo',
			    content: '¿Desea Activar el testigo con rut: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	activateUser: {
			    		text: 'Activar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	activate_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function activate_data(data, rut){
		 $.ajax({
			 url: "/testigos/act",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){	
				 				 
				 
				 $('#estado'+rut).text('Activo');
				 $('#desactivar'+rut).prop('enabled', true);
				 $('#desactivar'+rut).prop("disabled", false);									
				 $('#activar'+rut).prop('disabled', true);
				 $('#activar'+rut).prop("enabled", false);
				 $('#edit'+rut).prop("enabled", true);
				 $('#edit'+rut).prop("disabled", false);
					
				 
				 iziToast.success({
					    title: 'Testigos',
					    message: resp,
					});	
				
			},
			error: function(jqXHR, estado, error){
				console.log(estado+'-'+error);	
				console.log(jqXHR);
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
	
	$(".eliminar").on('click',function(){
	
		 var dato = $(this).attr('id');
		 dato = dato.replace('desactivar','');
		 var data = {rut:dato};		 
		 $.confirm({
			    title: 'Testigos',
			    content: '¿Desea eliminar el testigo con rut: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	deleteUser: {
			    		text: 'Eliminar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	delete_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function delete_data(data, rut){
		
		 $.ajax({
			 url: "/testigos/del",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 
				 $('#estado'+rut).text('Eliminado');
				 $('#desactivar'+rut).prop('enabled', false);
				 $('#desactivar'+rut).prop('disabled', true);									
				 $('#activar'+rut).prop('disabled', false);
				 $('#activar'+rut).prop('enabled', true);
				 $('#edit'+rut).prop('enabled', false);
				 $('#edit'+rut).prop('disabled', true);
					
				 
				 iziToast.success({
					    title: 'Testigos',
					    message: resp,
					});	
			},
			error: function(jqXHR, estado, error){
				console.log(error);				
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
});

