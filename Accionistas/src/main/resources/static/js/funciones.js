(function (d, w){
	'use strict';
	$(document).ready(function(){
		//Generar datatables de tablas de sistema
		$('#tablaaccionistas').DataTable({
	        responsive: true,
	        scrollCollapse: true	        
	    });
		$('#tablaaccion').DataTable({
			responsive: true,
			scrollCollapse: true
		});	
		$('#tabladividendo').DataTable({
			responsive: true,
			scrollCollapse: true
		});
		$('#tablarepresentante').DataTable({
			responsive: true,
			scrollCollapse: true
		});
		$('#tablamandatario').DataTable({
			responsive: true,
			scrollCollapse: true
		});
	    $('#tablatestigos').DataTable({
	        responsive: true,
	        scrollCollapse: true
	    });
	    $('#tablahistorialtraspasos').DataTable({
	    	responsive: true,
	    	"scrollX": true,
	        scrollCollapse: true
	    });
	    $('#tablaheredero').DataTable({
	    	resposive: true,
	    	scrollCollpase: true
	    });
	    $('#tablaapoderado').DataTable({
	        responsive: true,
	        scrollCollapse: true
	    });
	    $('#tablajuntaaccionistas').DataTable({
	        responsive: true,
	        scrollCollapse: true	        
	    });
	    $('#actas').DataTable({
	        responsive: true,
	        scrollCollapse: true,
	    });
	});

})(document, window);

