$(document).ready(function (){
	$(".activar").on('click',function(){
		 var dato = $(this).attr('id');
		 dato = dato.replace('activar','');
		 var data = {rut:dato};
		 $.confirm({
			    title: 'Apoderado',
			    content: '¿Desea Activar el apoderado con rut: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	activateUser: {
			    		text: 'Activar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	activate_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function activate_data(data, rut){
		 $.ajax({
			 url: "/apoderados/act",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){	
				 				 
				 
				 $('#estado'+rut).text('Activo');
				 $('#desactivar'+rut).prop('enabled', true);
				 $('#desactivar'+rut).prop("disabled", false);									
				 $('#activar'+rut).prop('disabled', true);
				 $('#activar'+rut).prop("enabled", false);
				 $('#edit'+rut).prop("enabled", true);
				 $('#edit'+rut).prop("disabled", false);
					
				 
				 iziToast.success({
					    title: 'Apoderado',
					    message: resp,
					});	
				
			},
			error: function(jqXHR, estado, error){
				console.log(estado+'-'+error);	
				console.log(jqXHR);
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
	
	$(".eliminar").on('click',function(){
		 var dato = $(this).attr('id');
		 dato = dato.replace('desactivar','');
		 var data = {rut:dato};
		 $.confirm({
			    title: 'Accion',
			    content: '¿Desea eliminar el accion con rut: '+dato+' ?',
			    type: 'dark',
			    typeAnimated: true,
			    autoClose: 'cancelar|8000',
			    buttons: {
			    	deleteUser: {
			    		text: 'Eliminar',
			    		btnClass: 'btn-red',					    		
			            action: function () {
			            	delete_data(data, dato);
			            }
			        },					        
			        cancelar: function () {
			        	
			        }
			    }
		});
		 		 		 		
	});
	function delete_data(data, rut){
		 $.ajax({
			 url: "/apoderados/delete",
			 type: "GET",
			 data: data,
			 beforeSend:  function(){
				 
				 
			 },
			 success: function(resp){				 				 				 				
				$('#estado'+rut).text('Eliminado');
				
				$('#desactivar'+rut).prop('enabled', false);
				 $('#desactivar'+rut).prop("disabled", true);									
				 $('#activar'+rut).prop('disabled', false);
				 $('#activar'+rut).prop("enabled", true);
				 console.log($('#edit'+rut));
				 
				 $('#edit'+rut).prop('enabled', false);
				 $('#edit'+rut).prop('disabled', true);
				 console.log($('#edit'+rut));
				 iziToast.success({
					    title: 'Apoderado',
					    message: resp,
					});	
			},
			error: function(jqXHR, estado, error){
				console.log(estado+'-'+error);		
				console.log(jqXHR);
			},
			complete: function(jqXHR, estado){
				console.log(estado);				
			},
			timeout: 10000			 
		 });
	 }
});