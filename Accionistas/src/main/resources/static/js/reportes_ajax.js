/*
 * REPORTES PDF
 */

function ReportePFDAccionista(){	
	var Tipofiltro = $('#filtro').val();
	 var data = {filtro:Tipofiltro};
	$.get("/reportePfd/Filtro", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de accionista generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de accionistas.'
			});
			break;
		}			
	});
}

function ReportePFDApoderado(){	
	$.get("/reportePfd/reporteApoderado", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de apoderado generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de apoderado.'
			});
			break;
		}			
	});
}

function ReportePFDHerederos(){	
	$.get("/reportePfd/reporteHerederos", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de heredero generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de heredero.'
			});
			break;
		}			
	});
}

function ReportePFDJunta(){	
	$.get("/reportePfd/reporteJunta", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de junta accionistas generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de junta accionistas.'
			});
			break;
		}			
	});
}

function ReportePFDTraspaso(){	
	$.get("/reportePfd/reporteTraspasoAcciones", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de traspaso acciones generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de traspaso acciones.'
			});
			break;
		}			
	});
}

function ReportePFDDividendo(){	
	$.get("/reportePfd/reporteDividendo", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte de dividendo generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de dividendo.'
			});
			break;
		}			
	});
}


/*
 * REPORTES TEXT
 */

function ReportePFDSVS(){	
	var Tipofiltro = $('#reporteSvs').val();
	var data = {filtroSVS:Tipofiltro};
	$.get("/reporteSVS/FiltroSvs", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte SVS generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte de SVS.'
			});
			break;
		}			
	});
}


/*
 * REPORTES EXCEL
 */

function ReporteExcelTraspaso(){	
	$.get("/reporteExcel/reporteExcelTraspaso", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte excel de traspaso acciones generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte excel de traspaso acciones.'
			});
			break;
		}			
	});
}

function ReporteEXCELDividendo(){	
	$.get("/reporteExcel/reporteExcelDividendo", function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte excel de dividendo generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte excel de dividendo.'
			});
			break;
		}			
	});
}

function ReporteExcelAccionista(){	
	var Tipofiltro = $('#filtroExcel').val();
	 var data = {filtroExcel:Tipofiltro};
	$.get("/reporteExcel/reporteExcelAccionista", data, function(e){
		console.log(e);
		switch (e){
		case "1": 
			iziToast.success({
			    title: 'Exito',
			    message: 'Reporte excel de accionista generado.'
			});
			break;
		default:
			iziToast.error({
			    title: 'Error',
			    message: 'No se realizo reporte excel de accionistas.'
			});
			break;
		}			
	});
}