package com.accionistas.accionsis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AccionsisApplication{

	
	public static void main(String[] args) {
		SpringApplication.run(AccionsisApplication.class, args);
	}

}
