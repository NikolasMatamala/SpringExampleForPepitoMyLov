package com.accionistas.accionsis.utility;

public class JSONResponse {
	
	public static String estado = "estado";
	public static String porcentajeDeAsistencia = "porcentajedeasistencia";
	public static String porcentajeDeAsistenciaTotalJunta = "porcentajedeasistenciatotaljunta";
	public static String valorAccionTipoAActual = "valoracciontipoaactual";
	public static String valorAccionTipoBActual = "valoracciontipobactual";
	public static String graficoTipoA = "graficotipoa";
	public static String graficoTipoB = "graficotipob";
	public static String graficoTodos = "graficoTodos";
	
	private String nombre, mensaje;
	private Object objetoAsociado;

	public JSONResponse(String nombre, String mensaje) {
		super();
		this.nombre = nombre;
		this.mensaje = mensaje;
	}
	public JSONResponse(String nombre, String mensaje, Object objetoAsociado) {
		super();
		this.nombre = nombre;
		this.mensaje = mensaje;
		this.objetoAsociado = objetoAsociado;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
