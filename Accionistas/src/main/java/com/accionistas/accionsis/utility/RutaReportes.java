package com.accionistas.accionsis.utility;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class RutaReportes {
	
	public static final String rutaPrincipal = "C:\\Reportes_Hipodromo_Chile";
	public static final String rutaPDF = "\\Reportes_en_PDF";
	public static final String rutaEXCEL = "\\Reportes_en_EXCEL";
	public static final String rutaTXT = "\\Reporte_SVS";
//	public static final String rutaAccionista = "\\Acionistas";
//	public static final String rutaApoderado = "\\Apoderados";
//	public static final String rutaHeredero = "\\Herederos";
//	public static final String rutaJunta = "\\JuntaAccionistas";
//	public static final String rutaTraspaso = "\\TraspasoAcciones";
	public static final String rutaSVS = "\\ReporteSVS";
	public static final String rutaDividendo = "\\Dividendo";
	
	public static String verificarRuta(String rutaReportes) {
		String resultado ="";
		resultado = carpetaAno(rutaReportes);
		resultado = carpetaMes(resultado);
		resultado = carpetaDia(resultado);
		return resultado;
	}
	
	private static String carpetaAno(String rutaReportes) {
		String principal = rutaPrincipal + rutaReportes;
		String anoActual = "\\" +LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy"));
		
		File carpeta = new File(principal + anoActual);
		if(!carpeta.exists()) {
			System.out.println("Creando la carpeta: " + principal + anoActual);
			if(carpeta.mkdirs()) {
				System.out.println("Carpeta Creada: " + principal + anoActual);
			}else {
				System.out.println("No se pudo crear la carpeta: " + principal + anoActual);
			}
		}else {
			System.out.println("La carpeta: " + principal + anoActual+ " ya existe.");
		}
		
		return principal + anoActual;
	}
	
	private static String carpetaMes(String rutaReportes) {		
		String mesActual = "\\" +LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMM"));
		
		File carpeta = new File(rutaReportes + mesActual);
		if(!carpeta.exists()) {
			System.out.println("Creando la carpeta: " + rutaReportes + mesActual);
			if(carpeta.mkdirs()) {
				System.out.println("Carpeta Creada: " + rutaReportes + mesActual);
			}else {
				System.out.println("No se pudo crear la carpeta: " + rutaReportes + mesActual);
			}
		}else {
			System.out.println("La carpeta: " + rutaReportes + mesActual+ " ya existe.");
		}
		return rutaReportes + mesActual;
	}
	
	private static String carpetaDia(String rutaReportes) {		
		String diaActual = "\\" +LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd"));
		
		File carpeta = new File(rutaReportes + diaActual);
		if(!carpeta.exists()) {
			System.out.println("Creando la carpeta: " + rutaReportes + diaActual);
			if(carpeta.mkdirs()) {
				System.out.println("Carpeta Creada: " + rutaReportes + diaActual);
			}else {
				System.out.println("No se pudo crear la carpeta: " + rutaReportes + diaActual);
			}
		}else {
			System.out.println("La carpeta: " + rutaReportes + diaActual+ " ya existe.");
		}
		return rutaReportes + diaActual;
	}

}
