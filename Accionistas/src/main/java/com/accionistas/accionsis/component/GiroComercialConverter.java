package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.GiroComercial;
import com.accionistas.accionsis.model.GiroComercialModel;

@Component("giroComercialConverter")
public class GiroComercialConverter {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(EmpresaConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param girocomercialModel GiroComercialModel 
	 * @return girocomercial
	 */
	public GiroComercial modelo2Entidad(GiroComercialModel girocomercialModel) {
		LOG.info("Convirtiendo apoderado de modelo a entidad");
		GiroComercial girocomercial = new GiroComercial();
		girocomercial.setIdGiroComercial(girocomercialModel.getIdGiroComercialModel());
		girocomercial.setDescripcion(girocomercialModel.getDescripcionModel());
		return girocomercial;
	}
	
	/**
	 * Entidad a model.
	 * @param girocomercial GiroComercial
	 * @return girocomercialModel
	 */
	public GiroComercialModel entidad2Model(GiroComercial girocomercial) {
		LOG.info("Convirtiendo apoderado de entidad a modelo");
		GiroComercialModel girocomercialModel = new GiroComercialModel();
		girocomercialModel.setIdGiroComercialModel(girocomercial.getIdGiroComercial());
		girocomercialModel.setDescripcionModel(girocomercial.getDescripcion());
		return girocomercialModel;
	}

}
