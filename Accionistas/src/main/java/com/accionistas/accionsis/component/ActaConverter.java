package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Acta;
import com.accionistas.accionsis.model.ActaModel;

@Component("actaConverter")
public class ActaConverter {
	
	private static final Log LOG = LogFactory.getLog(ActaConverter.class);
	
	
	public Acta modelo2Entidad(ActaModel actaModel) {
		
		if(actaModel == null) {return null;}
		
		LOG.info("Convirtiendo acta de modelo a entidad");
		Acta acta = new Acta();
		acta.setIdActa(actaModel.getIdActa());
		acta.setJuntaAccionista(actaModel.getJuntaAccionista());
		acta.setEmpresaAccionesA(actaModel.getEmpresaAccionesA());
		acta.setEmpresaAccionesB(actaModel.getEmpresaAccionesB());
		acta.setAccionistasAccionesA(actaModel.getAccionistasAccionesA());
		acta.setAccionistasAccionesB(actaModel.getAccionistasAccionesB());
		acta.setFechaApertura(actaModel.getFechaApertura());
		acta.setFechaTermino(actaModel.getFechaTermino());
		acta.setDetalleActa(actaModel.getDetalleActa());
		acta.setDocumento(actaModel.getDocumento());
		acta.setUrl(actaModel.getUrl());
		acta.setTipoActa(actaModel.getTipoActa());
		
		return acta;
		
	}
	
	public ActaModel entidad2Modelo(Acta acta) {
		
		if(acta == null) {return null;}
		
		LOG.info("Convirtiendo accionista de entidad a modelo");
		ActaModel actaModel = new ActaModel();
		actaModel.setIdActa(acta.getIdActa());
		actaModel.setJuntaAccionista(acta.getJuntaAccionista());
		actaModel.setEmpresaAccionesA(acta.getEmpresaAccionesA());
		actaModel.setEmpresaAccionesB(acta.getEmpresaAccionesB());
		actaModel.setAccionistasAccionesA(acta.getAccionistasAccionesA());
		actaModel.setAccionistasAccionesB(acta.getAccionistasAccionesB());
		actaModel.setFechaApertura(acta.getFechaApertura());
		actaModel.setFechaTermino(acta.getFechaTermino());
		actaModel.setDetalleActa(acta.getDetalleActa());
		actaModel.setDocumento(acta.getDocumento());
		actaModel.setUrl(acta.getUrl());
		actaModel.setTipoActa(acta.getTipoActa());
		
		return actaModel;
	}

}
