package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.CuentaCorriente;
import com.accionistas.accionsis.model.CuentaCorrienteModel;

@Component("cuentaCorrienteConverte")
public class CuentaCorrienteConverte {

	private static final Log LOG = LogFactory.getLog(CuentaCorrienteConverte.class);
	
	public CuentaCorriente modelo2Entidad (CuentaCorrienteModel cuentaCorrienteModel) {
		
		LOG.info("Convirtiendo cuentaCorriente de modelo a entidad");
		
		CuentaCorriente cuenta = new CuentaCorriente();
		cuenta.setIdCuentaCorriente(cuentaCorrienteModel.getIdCuentaCorrienteModel());
		cuenta.setNumeroCuenta(cuentaCorrienteModel.getNumeroCuentaModel());
		cuenta.setNombreBanco(cuentaCorrienteModel.getNombreBancoModel());
		cuenta.setAccionista(cuentaCorrienteModel.getAccionistaModel());
		return cuenta;
	}
	
	public CuentaCorrienteModel entidad2Modelo(CuentaCorriente cuentaCorriente) {
		
		LOG.info("Convirtiendo cuentaCorriente de entidad a modelo");
		CuentaCorrienteModel cuentaModel = new CuentaCorrienteModel();
		cuentaModel.setIdCuentaCorrienteModel(cuentaCorriente.getIdCuentaCorriente());
		cuentaModel.setNumeroCuentaModel(cuentaCorriente.getNumeroCuenta());
		cuentaModel.setNombreBancoModel(cuentaCorriente.getNombreBanco());
		cuentaModel.setAccionistaModel(cuentaCorriente.getAccionista());
		return cuentaModel;
	}
}
