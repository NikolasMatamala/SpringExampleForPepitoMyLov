package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.model.DividendoModel;

@Component("DividendoConverter")
public class DividendoConverter {
	
	AccionistaConverter aC = new AccionistaConverter();
	JuntaAccionistaConverter juntaC = new JuntaAccionistaConverter();
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(DividendoConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param dividendoModel DividendoModel 
	 * @return dividendo
	 */
	public Dividendo modelo2Entidad(DividendoModel dividendoModel) {
		LOG.info("Convierte dividendo de modelo a entidad");
		
		Dividendo dividendo = new Dividendo();
		dividendo.setIdDividendo(dividendoModel.getIdDividendoModel());
		dividendo.setJuntaAccionista(juntaC.modelo2Entidad(dividendoModel.getJuntaAccionistaModel()));
		dividendo.setAccionista(aC.modelo2Entidad(dividendoModel.getAccionistaModel()));
		dividendo.setFechaDeclaracion(dividendoModel.getFechaDeclaracionModel());
		dividendo.setFechaRegistro(dividendoModel.getFechaRegistroModel());
		dividendo.setDividendoAccionA(dividendoModel.getDividendoAccionAModel());
		dividendo.setDividendoAccionB(dividendoModel.getDividendoAccionBModel());
		dividendo.setDividendoTotal(dividendoModel.getDividendoTotalModel());
		dividendo.setEstado(dividendoModel.getEstadoModel());
		return dividendo;
	}
	
	/**
	 * Entidad a modelo.
	 * @param dividendo Dividendo 
	 * @return dividendoModel
	 */
	public DividendoModel entidad2Model(Dividendo dividendo) {
		LOG.info("Convierte dividendo de entidad a modelo");
		
		DividendoModel dividendoModel = new DividendoModel();
		dividendoModel.setIdDividendoModel(dividendo.getIdDividendo());
		dividendoModel.setJuntaAccionistaModel(juntaC.entidad2Model(dividendo.getJuntaAccionista()));
		dividendoModel.setAccionistaModel(aC.entidad2Model(dividendo.getAccionista()));
		dividendoModel.setFechaDeclaracionModel(dividendo.getFechaDeclaracion());
		dividendoModel.setFechaRegistroModel(dividendo.getFechaRegistro());
		dividendoModel.setDividendoAccionAModel(dividendo.getDividendoAccionA());
		dividendoModel.setDividendoAccionBModel(dividendo.getDividendoAccionB());
		dividendoModel.setDividendoTotalModel(dividendo.getDividendoTotal());
		dividendoModel.setEstadoModel(dividendo.getEstado());
		return dividendoModel;
	}
}
