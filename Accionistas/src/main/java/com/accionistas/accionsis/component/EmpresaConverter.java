package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.EmpresaModel;

/**
 * Clase EmpresaConverter.
 */
@Component("empresaConverter")
public class EmpresaConverter {
	
	RepresentanteConverter rC = new RepresentanteConverter();
	GiroComercialConverter gC = new GiroComercialConverter();
	
	/** The Constant LOG. */
	private static final Log LOG = LogFactory.getLog(EmpresaConverter.class);
	
	/**
	 * Modelo a entidad.
	 *
cd	 * @param empresaModel EmpresaModel
	 * @return empresa
	 */
	public Empresa modelo2Entidad(EmpresaModel empresaModel) {
		LOG.info("Convirtiendo empresa de modelo a entidad");
		
		Empresa empresa = new Empresa();
		empresa.setIdEmpresa(empresaModel.getIdEmpresaModel());
		empresa.setRut(empresaModel.getRutModel());
		empresa.setRazonSocial(empresaModel.getRazonSocialModel());
		empresa.setLogo(empresaModel.getLogoModel());
		empresa.setWeb(empresaModel.getWebModel());
		empresa.setCantidadAccionesSerieA(empresaModel.getCantidadAccionesSerieAModel());
		empresa.setCantidadAccionesSerieB(empresaModel.getCantidadAccionesSerieBModel());
		empresa.setCantidadAccionistas(empresaModel.getCantidadAccionistasModel());
		empresa.setEmail(empresaModel.getEmailModel());
		empresa.setDireccion(empresaModel.getDireccionModel());
		empresa.setTelefono(empresaModel.getTelefonoModel());
		if(empresaModel.getRepresentanteModel() != null) {
			empresa.setRepresentante(rC.modelo2Entidad(empresaModel.getRepresentanteModel()));
		}
		if(empresaModel.getGiroComercialModel() != null) {
			empresa.setGiroComercial(gC.modelo2Entidad(empresaModel.getGiroComercialModel()));
		}
		
		return empresa;
	}

	
	/**
	 * Entidad a model.
	 *
	 * @param empresa Empresa 
	 * @return empresaModel
	 */
	public EmpresaModel entidad2Model(Empresa empresa) {
		LOG.info("Convirtiendo empresa de entidad a modelo");
				
		
		EmpresaModel empresaModel = new EmpresaModel();
		
		if(empresa == null) return null;
		empresaModel.setIdEmpresaModel(empresa.getIdEmpresa());
		empresaModel.setRutModel(empresa.getRut());
		empresaModel.setRazonSocialModel(empresa.getRazonSocial());
		empresaModel.setLogoModel(empresa.getLogo());
		empresaModel.setWebModel(empresa.getWeb());
		empresaModel.setCantidadAccionesSerieAModel(empresa.getCantidadAccionesSerieA());
		empresaModel.setCantidadAccionesSerieBModel(empresa.getCantidadAccionesSerieB());
		empresaModel.setCantidadAccionistasModel(empresa.getCantidadAccionistas());
		empresaModel.setEmailModel(empresa.getEmail());
		empresaModel.setDireccionModel(empresa.getDireccion());
		empresaModel.setTelefonoModel(empresa.getTelefono());
		empresaModel.setRepresentanteModel(rC.entidad2Model(empresa.getRepresentante()));
		empresaModel.setGiroComercialModel(gC.entidad2Model(empresa.getGiroComercial()));
		return empresaModel;
	}
}
