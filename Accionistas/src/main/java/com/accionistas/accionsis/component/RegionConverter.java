package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Regiones;
import com.accionistas.accionsis.model.RegionesModel;

@Component("regionConverter")
public class RegionConverter {
	
	private static final Log LOG = LogFactory.getLog(AccionistaConverter.class);
	
	public Regiones modelo2Entidad(RegionesModel regionesModel) {
		LOG.info("Convirtiendo Region de modelo a entidad");
		if(regionesModel == null) {return null;}
		Regiones region = new Regiones();
		region.setIdRegion(regionesModel.getIdRegionModel());
		region.setRegion(regionesModel.getRegionModel());
		
		return region;
	}
	
	public RegionesModel entidad2Modelo(Regiones regiones) {
		LOG.info("Convirtiendo Region de entidad a modelo");
		if(regiones == null) {return null;}
		RegionesModel regionesModel = new RegionesModel();
		regionesModel.setIdRegionModel(regiones.getIdRegion());
		regionesModel.setRegionModel(regiones.getRegion());
		
		return regionesModel;
	}

}
