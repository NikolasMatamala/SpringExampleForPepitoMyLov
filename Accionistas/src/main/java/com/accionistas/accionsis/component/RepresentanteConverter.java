package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.RepresentanteModel;

@Component("representanteConverter")
public class RepresentanteConverter {
 
	public static final Log LOG = LogFactory.getLog(RepresentanteConverter.class);
	
	private RegionConverter rC = new RegionConverter();
	
	public RepresentanteModel entidad2Model(Representante representante) {
		
		LOG.info("convirtiendo representante de entidad a modelo");
		if(representante == null) {return null;}
		
		RepresentanteModel representanteModel = new RepresentanteModel();
		representanteModel.setIdRepresentanteModel(representante.getIdRepresentante());
		representanteModel.setRutModel(representante.getRut());
		representanteModel.setNombreModel(representante.getNombre());
		representanteModel.setApellido_paternoModel(representante.getApellido_paterno());
		representanteModel.setApellido_maternoModel(representante.getApellido_materno());
		representanteModel.setDireccionModel(representante.getDireccion());
		representanteModel.setEmailModel(representante.getEmail());
		representanteModel.setTelefonoModel(representante.getTelefono());
		representanteModel.setEstadoModel(representante.getEstado());
		representanteModel.setRegionesModel(rC.entidad2Modelo(representante.getRegion()));
		
		return representanteModel;
	}
	
	public Representante modelo2Entidad(RepresentanteModel representanteModel) {
		LOG.info("convirtiendo representante de modelo a entidad");
		
		if(representanteModel == null) {return null;}
		
		Representante representante = new Representante();
		representante.setIdRepresentante(representanteModel.getIdRepresentanteModel());
		representante.setRut(representanteModel.getRutModel());
		representante.setNombre(representanteModel.getNombreModel());
		representante.setApellido_paterno(representanteModel.getApellido_paternoModel());
		representante.setApellido_materno(representanteModel.getApellido_maternoModel());
		representante.setDireccion(representanteModel.getDireccionModel());
		representante.setEmail(representanteModel.getEmailModel());
		representante.setTelefono(representanteModel.getTelefonoModel());
		representante.setEstado(representanteModel.getEstadoModel());
		representante.setRegion(rC.modelo2Entidad(representanteModel.getRegionesModel()));
		
		return representante;
	}
	
}
