package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.TipoAccionista;
import com.accionistas.accionsis.model.TipoAccionistaModel;

@Component("tipoAccionistaConverter")
public class TipoAccionistaConverter {
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(TipoAccionistaConverter.class);
	
	/**
	 * Modelo a entidad.
	 * @param tipoAccionistaModel
	 * @return tipoAccionista
	 */
	public TipoAccionista modelo2Entidad(TipoAccionistaModel tipoAccionistaModel) {
		LOG.info("Convirtiendo nacionalidad de modelo a entidad");
		TipoAccionista tipoAccionista= new TipoAccionista();
		tipoAccionista.setIdTipoAccionista(tipoAccionistaModel.getIdTipoAccionistaModel());
		tipoAccionista.setDescripcion_tipo_accionista(tipoAccionistaModel.getDescripcion_tipo_accionistaModel());
		return tipoAccionista;
	}
	
	/**
	 * Entidad a model.
	 * @param tipoAccionista
	 * @return tipoAccionistaModel
	 */
	public TipoAccionistaModel entidad2Model(TipoAccionista tipoAccionista) {
		LOG.info("Convirtiendo nacionalidad de entidad a modelo");
		TipoAccionistaModel tipoAccionistaModel = new TipoAccionistaModel();
		tipoAccionistaModel.setIdTipoAccionistaModel(tipoAccionista.getIdTipoAccionista());
		tipoAccionistaModel.setDescripcion_tipo_accionistaModel(tipoAccionista.getDescripcion_tipo_accionista());
		return tipoAccionistaModel;
	}

}
