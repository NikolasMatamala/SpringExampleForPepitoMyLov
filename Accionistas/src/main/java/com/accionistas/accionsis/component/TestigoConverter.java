package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Testigo;
import com.accionistas.accionsis.model.TestigoModel;

/**
 * Clase TestigoConverter.
 */
@Component("testigoConverter")
public class TestigoConverter {

/** LOG constante. */
private static final Log LOG = LogFactory.getLog(TestigoConverter.class);
	
	/**
	 * Modelo a entidad.
	 *
	 * @param testigoModel TestigoModel 
	 * @return testigo
	 */
	//Modelo a entiedad
	public Testigo modelo2Entidad(TestigoModel testigoModel) {
		LOG.info("Convirtiendo testigo de modelo a entidad");
		Testigo testigo = new Testigo();
		testigo.setIdTestigo(testigoModel.getIdTestigoModel());
		testigo.setRut(testigoModel.getRutModel());
		testigo.setNombre(testigoModel.getNombreModel());
		testigo.setApellido_paterno(testigoModel.getApellido_paternoModel());
		testigo.setApellido_materno(testigoModel.getApellido_maternoModel());
		testigo.setEmail(testigoModel.getEmailModel());
		testigo.setFono(testigoModel.getFonoModel());
		testigo.setEstado(testigoModel.getEstado());
		return testigo;
	}

	
	/**
	 * Entidad 2 model.
	 *
	 * @param testigo the testigo
	 * @return the testigo model
	 */
	//Entidad a modelo
	public TestigoModel entidad2Model(Testigo testigo) {
		LOG.info("Convirtiendo testigo de entidad a modelo");
		TestigoModel testigoModel = new TestigoModel();
		testigoModel.setIdTestigoModel(testigo.getIdTestigo());
		testigoModel.setRutModel(testigo.getRut());
		testigoModel.setNombreModel(testigo.getNombre());
		testigoModel.setApellido_paternoModel(testigo.getApellido_paterno());
		testigoModel.setApellido_maternoModel(testigo.getApellido_materno());
		testigoModel.setEmailModel(testigo.getEmail());
		testigoModel.setFonoModel(testigo.getFono());
		testigoModel.setEstado(testigo.getEstado());
		return testigoModel;
	}
	
}
