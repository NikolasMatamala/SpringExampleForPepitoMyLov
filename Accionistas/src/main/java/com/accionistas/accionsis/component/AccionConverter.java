package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.model.AccionModel;

@Component("accionConverter")
public class AccionConverter {
	
	

	private static final Log LOG = LogFactory.getLog(AccionConverter.class);

	public Accion modelo2Entidad(AccionModel accionModel) {
		LOG.info("Convirtiendo accion de modelo a entidad");
		
		Accion accion = new Accion();
		accion.setIdAccion(accionModel.getIdAccionModel());
		accion.setValorA(accionModel.getValorAModel());
		accion.setValorB(accionModel.getValorBModel());
		accion.setFechaIngreso(accionModel.getFechaIngresoModel());
		
		return accion;
	}
	
	public AccionModel entidad2Modelo(Accion accion) {
		LOG.info("Convirtiendo accion de entidad a modelo");
		
		AccionModel accionModel = new AccionModel();
		accionModel.setIdAccionModel(accion.getIdAccion());
		accionModel.setValorAModel(accion.getValorA());
		accionModel.setValorBModel(accion.getValorB());
		accionModel.setFechaIngresoModel(accion.getFechaIngreso());
		
		return accionModel;
	}
	
}
