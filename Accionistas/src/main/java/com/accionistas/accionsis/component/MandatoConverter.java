package com.accionistas.accionsis.component;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.accionistas.accionsis.entity.Mandato;
import com.accionistas.accionsis.model.MandatoModel;

@Component("mandatoConverter")
public class MandatoConverter {

	private static final Log LOG = LogFactory.getLog(MandatoConverter.class);

	public Mandato modelo2Entidad(MandatoModel mandatoModel) {
		LOG.info("Convirtiendo mandatario de modelo a entidad");
		
		Mandato mandato = new Mandato();
		mandato.setIdMandato(mandatoModel.getIdMandatoModel());
		mandato.setDescripcionMandato(mandatoModel.getDescripcionMandato());
		
		return mandato;
	}
	
	public MandatoModel entidad2Model(Mandato mandato) {
		LOG.info("Convirtiendo mandatario de entidad a modelo");
		
		MandatoModel mandatoModel = new MandatoModel();
		mandatoModel.setIdMandatoModel(mandato.getIdMandato());
		mandatoModel.setDescripcionMandato(mandato.getDescripcionMandato());
	
		return mandatoModel;
	}
	
	
}
