package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.component.HerederoConverter;
import com.accionistas.accionsis.component.NacionalidadConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Nacionalidad;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.model.HerederoModel;
import com.accionistas.accionsis.model.NacionalidadModel;
import com.accionistas.accionsis.model.RegionesModel;
import com.accionistas.accionsis.model.TipoAccionistaModel;
import com.accionistas.accionsis.service.HerederoService;
import com.accionistas.accionsis.service.RegionesService;
import com.accionistas.accionsis.service.impl.AccionistaServiceImp;
import com.accionistas.accionsis.service.impl.EmpresaServiceImp;
import com.accionistas.accionsis.service.impl.NacionalidadServiceImp;
import com.accionistas.accionsis.service.impl.TipoAccionistaServiceImp;

@Controller
@RequestMapping("/heredero")
public class HerederoController {

	/*
	 * Constante log para informacion
	 */
	private static final Log LOG = LogFactory.getLog(HerederoController.class);
	/*
	 * inyeccion de herederoServiceImp
	 */
	@Autowired
	@Qualifier("herederoServiceImp")
	private HerederoService herederoService;
	
	@Autowired
	@Qualifier("herederoConverter")
	private HerederoConverter herederoConverter;
	
	@Autowired
	@Qualifier("nacionalidadServiceImp")
	private NacionalidadServiceImp nacionalidadService;
	
	@Autowired
	@Qualifier("tipoAccionistaServiceImp")
	private TipoAccionistaServiceImp tipoAccionistaService;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaServiceImp accionistaService;
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaServiceImp empresaService;
	
	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	@Autowired
	@Qualifier("nacionalidadConverter")
	private NacionalidadConverter nacionalidadConverter;
	
	@Autowired
	@Qualifier("regionesServiceImp")
	private RegionesService regionesService;

	/*
	 * listar herederos
	 */
	@GetMapping("/listarherederos")
	public ModelAndView listAllherederos() {
		LOG.info("Call: " + "listAllherederos() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.HEREDERO_VIEW);
		mav.addObject("herederos", herederoService.listAllHeredero());
		return mav;
	}
	/*
	 * Agregar herederos
	 */
	@RequestMapping(value="/addheredero", method=RequestMethod.GET)
	public @ResponseBody String addHeredero(@RequestParam(name="id", required=false) String id, @RequestParam String rutModel, @RequestParam String empresaModel, @RequestParam String nombreModel, 
			@RequestParam String apellido_paternoModel, @RequestParam String apellido_maternoModel,	@RequestParam String cantidad_acciones_serie_aModel, 
			@RequestParam String cantidad_acciones_serie_bModel, @RequestParam String fecha_nacimientoModel, 
			@RequestParam String es_validoModel, @RequestParam String direccionModel, @RequestParam String emailModel,	@RequestParam String fonoModel, 
			@RequestParam String ciudadModel, @RequestParam String comunaModel, @RequestParam String regionesModel, @RequestParam String nacionalidadModel, 
			@RequestParam String tipoAccionistaModel, @RequestParam String accionistaModel) {
		
		String respuesta = "";
		HerederoModel herederoModel = null;
		LOG.info("Call: "+"addHeredero()");	
		
		if(!id.equalsIgnoreCase("0")) {
			int idHeredero = Integer.parseInt(id);
			herederoModel = herederoConverter.entidad2Modelo(herederoService.findHerederoByidHeredero(idHeredero));
		}else {
		}
		
		herederoModel.setRutModel(rutModel);
		
		EmpresaModel em = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		herederoModel.setEmpresaModel(em);
		
		herederoModel.setNombreModel(nombreModel);
		herederoModel.setApellido_paternoModel(apellido_paternoModel);
		herederoModel.setApellido_maternoModel(apellido_maternoModel);
		herederoModel.setCantidad_acciones_serie_aModel(Integer.parseInt(cantidad_acciones_serie_aModel));
		herederoModel.setCantidad_acciones_serie_bModel(Integer.parseInt(cantidad_acciones_serie_bModel));		
		herederoModel.setFecha_nacimientoModel(fecha_nacimientoModel);
		herederoModel.setEs_validoModel(es_validoModel);
		herederoModel.setDireccionModel(direccionModel);
		herederoModel.setEmailModel(emailModel);
		herederoModel.setFonoModel(fonoModel);
		herederoModel.setCiudadModel(ciudadModel);
		herederoModel.setComunaModel(comunaModel);
		
		RegionesModel region = regionesService.findByidRegionModel(Integer.parseInt(regionesModel));
		herederoModel.setRegionesModel(region);
		
		NacionalidadModel nac = nacionalidadService.findNacionalidadModelByIdModel(Integer.parseInt(nacionalidadModel));
		herederoModel.setNacionalidadModel(nac);
		
		TipoAccionistaModel tipoA = tipoAccionistaService.findTipoAccionistaModelByIdModel(Integer.parseInt(tipoAccionistaModel));
		tipoA.setIdTipoAccionistaModel(Integer.parseInt(tipoAccionistaModel));
		herederoModel.setTipoAccionistaModel(tipoA);
		
		//Obtiene la cantidad de acciones serie A y serie B de un Accionista
		AccionistaModel acc = accionistaService.findAccionistaModelById(Integer.parseInt(accionistaModel));
		herederoModel.setAccionistaModel(acc);
		
		int cantidadAccionesA = acc.getCantidad_acciones_serie_aModel();
		int cantidadAccionesB = acc.getCantidad_acciones_serie_bModel();
		
		//Calcula las acciones totales para sacar porcentaje de participacion
		int accionesTotales = cantidadAccionesA + cantidadAccionesB;
		
		
		//Calcula las acciones del nuevo accionista
		int accionesHeredero = herederoModel.getCantidad_acciones_serie_aModel() + herederoModel.getCantidad_acciones_serie_bModel();
		
		//Obtiene porcentaje de participacion
		double porcentajeParticipacion = (accionesHeredero * 100) / accionesTotales;
		String.format("%.2f", porcentajeParticipacion);
		herederoModel.setPorcentaje_participacionModel(porcentajeParticipacion);
		
		if(rutModel != "" && empresaModel != "" && nombreModel != "" && apellido_paternoModel != "" && apellido_maternoModel != "" && cantidad_acciones_serie_aModel != "" 
				&& cantidad_acciones_serie_bModel != "" && fecha_nacimientoModel != "" && es_validoModel != "" && direccionModel != "" 
				&& emailModel != "" && fonoModel != "" && ciudadModel != "" && comunaModel != "" && regionesModel != "" && nacionalidadModel != "" && tipoAccionistaModel != "" 
				&& accionistaModel != "") {
			if(null != herederoService.addHeredero(herederoModel)){			
	//			model.addAttribute("result", 1);
				respuesta = "1";
			}else {
				respuesta = "2";
			}
		} else {
//			model.addAttribute("result", 0);
			respuesta = "3";
		}
		return respuesta;
	}


	/*
	 * direccionando al formulario con hidden Rut
	 */
	@GetMapping("/addherederosform")
	private ModelAndView redirectToAddHerederoForm(@RequestParam(name="rut", required=false) String rut,
			Model model) {
		ModelAndView mav = new ModelAndView(ViewConstant.HEREDERO_FORM);
		HerederoModel herederoModel = new HerederoModel();
		if(!rut.equals("agregarHeredero")) {
			herederoModel = herederoService.findHerederoModelByRutModel(rut);
		}
		mav.addObject("empresas", empresaService.listAllEmpresa());
		mav.addObject("nacionalidades", nacionalidadService.listAllNacionalidades());
		mav.addObject("tipoAccionistas", tipoAccionistaService.listAllTiposAccionistas());
		mav.addObject("accionistas", accionistaService.listAllAccionista_active());
		mav.addObject("herederos", herederoService.listAllHeredero());	
		mav.addObject("regiones", regionesService.listAllRegiones());
		mav.addObject(herederoModel);
		return mav;		
	}
	/*
	 * remover heredero
	 */
	@GetMapping("/removeheredero")
	public ModelAndView removeHeredero(@RequestParam(name="rut", required=false)String rut, Model model) {
		herederoService.removeHeredero(rut);
		return listAllherederos();
	}
	/*
	 * cancelar
	 */
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllHerederos() -- Al cancelar");
		ModelAndView mav = new ModelAndView(ViewConstant.HEREDERO_VIEW);
		mav.addObject("herederos", herederoService.listAllHeredero());
		return mav;
	}
	
	@RequestMapping(value="del", method=RequestMethod.GET)
	public @ResponseBody String removeHeredero(@RequestParam String rut) {
		LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
			
			String message = "Heredero con el rut: " +rut+" eliminado";
		
			HerederoModel heredero = new HerederoModel();		
			heredero.setRutModel(rut);
			heredero = herederoService.findHerederoModelByRutModel(rut);
			heredero.setEstadoModel(0);			
			heredero = herederoService.addHeredero(heredero);
			
		return message;
	}
	
	@RequestMapping(value="act", method=RequestMethod.GET)
	public @ResponseBody String updateTestigo(@RequestParam String rut) {
		LOG.info("Call: "+ "removeTestigo()" + "-- Param: "+ rut.toString());
			
		String message = "Heredero con el rut: " +rut+" activado";
		
		HerederoModel heredero = new HerederoModel();		
		heredero.setRutModel(rut);
		heredero = herederoService.findHerederoModelByRutModel(rut);
		heredero.setEstadoModel(1);			
		heredero = herederoService.addHeredero(heredero);
			
		return message;
	}
	
	
}
