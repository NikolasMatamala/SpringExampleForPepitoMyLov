package com.accionistas.accionsis.controller;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.component.ApoderadoConverter;
import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.component.NacionalidadConverter;
import com.accionistas.accionsis.component.TipoAccionistaConverter;
import com.accionistas.accionsis.component.TraspasoaccionesConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.ApoderadoModel;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.model.NacionalidadModel;
import com.accionistas.accionsis.model.TestigoModel;
import com.accionistas.accionsis.model.TipoAccionistaModel;
import com.accionistas.accionsis.model.TraspasoaccionesModel;
import com.accionistas.accionsis.repository.QueryDslRepository;
import com.accionistas.accionsis.service.AccionService;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ApoderadoService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.NacionalidadService;
import com.accionistas.accionsis.service.RegionesService;
import com.accionistas.accionsis.service.TestigoService;
import com.accionistas.accionsis.service.TipoAccionistaService;
import com.accionistas.accionsis.service.TraspasoaccionesService;
import com.accionistas.accionsis.utility.JSONResponse;

/**
 * Clase AccionistaController.
 */
@Controller
@RequestMapping("accionistas")
public class AccionistaController {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(AccionistaController.class);
	
	@Autowired
	@Qualifier("queryDslRepository")
	private QueryDslRepository queryDslRepository;
	
	/** The accionista service. */	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("accionServiceImp")
	private AccionService accionService;
	
	@Autowired
	@Qualifier("testigoServiceImp")
	private TestigoService testigoService;
	
	
	@Autowired
	@Qualifier("tipoAccionistaServiceImp")
	private TipoAccionistaService tipoAccionistaService;
	
	@Autowired
	@Qualifier("apoderadoServiceImp")
	private ApoderadoService apoderadoService;
	
	@Autowired
	@Qualifier("apoderadoConverter")
	private ApoderadoConverter apoderadoConverter;
	
	
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("traspasoaccionesServiceImp")
	private TraspasoaccionesService traspasoaccionesService;
	
	@Autowired
	@Qualifier("nacionalidadServiceImp")
	private NacionalidadService nacionalidadService;
	
	@Autowired
	@Qualifier("regionesServiceImp")
	private RegionesService regionesService;
	
	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	@Autowired
	@Qualifier("traspasoaccionesConverter")
	private TraspasoaccionesConverter traspasoAccionesConverter;
	
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;
	
	@Autowired
	@Qualifier("nacionalidadConverter")
	private NacionalidadConverter nacionalidadConverter;
	
	@Autowired
	@Qualifier("tipoAccionistaConverter")
	private TipoAccionistaConverter tipoAccionistaConverter;
	
	/**
	 * lista todos los accionistas.
	 * @return the model and view
	 */
	@GetMapping("listaccionistas")
	public ModelAndView listAllAccionistas() {
		LOG.info("Call: " + "listAllAccionistas() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_LIST);
		mav.addObject("accionistas", accionistaService.listAllAccionista_active());
		return mav;
	}
	
	/**
	 * lista todos los accionistas en su mantenedor.
	 * @return the model and view
	 */
	@GetMapping("adminaccionistas")
	public ModelAndView listAllAccionistasMantenedor() {
		LOG.info("Call: " + "listAllAccionistas() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		List<AccionistaModel> ac = accionistaService.listAllAccionista();			
		mav.addObject("accionistas", ac );
		return mav;
	}
	
	
	@RequestMapping(value="/listarAccionistas2", method=RequestMethod.GET)
	public @ResponseBody String listarAccionistas2_grafico() {
		LOG.info("Call: "+ "listarAccionistas_grafico2() -- desde controller");
		List<Accionista> lista = new ArrayList<Accionista>();
		lista = queryDslRepository.findTop10AccionistasSerieA();
		ObjectMapper mapper = new ObjectMapper();
		String result;
		try {
			result = mapper.writeValueAsString(lista);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			result = "error";
			e.printStackTrace();
		}			
		return result;
	}
	
	
	@RequestMapping(value="/listarAccionistas", method=RequestMethod.GET)
	public @ResponseBody String listarAccionistas_grafico() {
		LOG.info("Call: "+ "listarAccionistas_grafico() -- desde controller");
		List<JSONResponse> response = new ArrayList<>();
		
		List<AccionistaModel> lista = new ArrayList<AccionistaModel>();
		List<AccionistaModel> lista2 = new ArrayList<AccionistaModel>();
		List<AccionistaModel> lista3 = new ArrayList<AccionistaModel>();
		//lista = accionistaService.listAllAccionista_active();
		lista = accionistaService.listAccionistasForGraficoTop10AccionTypeA();
		lista2 = accionistaService.listAccionistasForGraficoTop10AccionTypeB();
		lista3 = accionistaService.listAllAccionista_active();
		ObjectMapper mapper = new ObjectMapper();
		String result ="",result2 ="",result3="";
		try {
			result = mapper.writeValueAsString(lista);
			result2 = mapper.writeValueAsString(lista2);
			result3 = mapper.writeValueAsString(lista3);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			result = "error";
			e.printStackTrace();
		}			
		response.add(new JSONResponse(JSONResponse.graficoTipoA,result ));
		response.add(new JSONResponse(JSONResponse.graficoTipoB,result2 ));
		response.add(new JSONResponse(JSONResponse.graficoTodos,result3 ));
		String resultadoFinal = "";
		try {
			resultadoFinal = mapper.writeValueAsString(response);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultadoFinal;
	}
	
	
	/**
	 * Traspaso acciones propuesta.
	 * @param accionistaVendedor 
	 * @param accionistaComprador
	 * @param cantidadAccionesA 
	 * @param cantidadAccionesB 
	 * @return the model and view
	 */
	@GetMapping("traspasoacciones")
	public ModelAndView traspasoAcciones(@ModelAttribute(name="traspasoaccionesModel")TraspasoaccionesModel traspasoaccionesModel,
			Model model) {
		LOG.info("Call: " + "traspasoacciones() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONES_TRASPASO);
		mav.addObject("accionistas", accionistaService.listAllAccionista());
		mav.addObject("testigos", testigoService.listAllTestigo());		
		mav.addObject(traspasoaccionesModel);		
		
		return mav;
	}
	
	@RequestMapping(value="traspasoacciones2", method=RequestMethod.GET)
	public @ResponseBody int traspasoacciones2(@RequestParam String rut_accionista_vendedorModel, @RequestParam String rut_accionista_compradorModel,@RequestParam String fecha_ventaModel
			,@RequestParam String cantidad_acciones_vendidas_aModel,@RequestParam String cantidad_acciones_vendidas_bModel,@RequestParam String es_sucesionModel
			,@RequestParam String testigoModel,@RequestParam String observacionModel) {
		int error = 0;
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ rut_accionista_vendedorModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ rut_accionista_compradorModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ fecha_ventaModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ cantidad_acciones_vendidas_aModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ cantidad_acciones_vendidas_bModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ es_sucesionModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ testigoModel);
		LOG.info("Call: "+ "addtraspasoacciones2()" + "-- Param: "+ observacionModel);
		TraspasoaccionesModel traspasoaccionesModel = new TraspasoaccionesModel();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date fechaAux = null;
		try {
			fechaAux = formatter.parse(fecha_ventaModel);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("");
			System.out.println("");
			System.out.println("Error al convertir la fecha, ingresa una fecha correcta -> "+ fecha_ventaModel);
			System.out.println("");
			System.out.println("");
			error = 4;
			e.printStackTrace();
			return error;
		}
		Date fecha = new Date(fechaAux.getTime());
		TestigoModel testigo = testigoService.findTestigoByRutModel(testigoModel);
		AccionistaModel accionistaVendedorModel = accionistaService.findAccionistaModelByRut(rut_accionista_vendedorModel);
		AccionistaModel accionistaCompradorModel = accionistaService.findAccionistaModelByRut(rut_accionista_compradorModel);
		traspasoaccionesModel.setRut_accionista_compradorModel(accionistaCompradorModel);
		traspasoaccionesModel.setRut_accionista_vendedorModel(accionistaVendedorModel);
		traspasoaccionesModel.setFecha_ventaModel(fecha);
		int cantidadaccionesA = Integer.parseInt(cantidad_acciones_vendidas_aModel);
		int cantidadaccionesB = Integer.parseInt(cantidad_acciones_vendidas_bModel);
		traspasoaccionesModel.setCantidad_acciones_vendidas_aModel(cantidadaccionesA);
		traspasoaccionesModel.setCantidad_acciones_vendidas_bModel(cantidadaccionesB);
		traspasoaccionesModel.setEs_sucesionModel(es_sucesionModel);
		traspasoaccionesModel.setTestigoModel(testigo);
		traspasoaccionesModel.setObservacionModel(observacionModel);
		
		System.out.println("Error al convertir la fecha, ingresa una fecha correcta -> "+ fecha_ventaModel);
		
		
		//Captura accionistas para realizar acciones
		
		Accionista accionistaVendedor = accionistaService.findAccionistaByAccionistaModel(accionistaConverter.modelo2Entidad(traspasoaccionesModel.getRut_accionista_vendedorModel()));
		System.out.println("caca1");
		Accionista accionistaComprador = accionistaService.findAccionistaByAccionistaModel(accionistaConverter.modelo2Entidad(traspasoaccionesModel.getRut_accionista_compradorModel()));
		System.out.println("caca2");
		
		if(accionistaVendedor.getRut().equals(accionistaComprador.getRut())) {
			System.out.println("");
			System.out.println("");
			System.out.println("No se pudo realizar venta de acciones por que el accionista comprador y vendedor son la misma persona");
			System.out.println("");
			System.out.println("");
			error = 1;
			return error;
			//error = "No se pudo realizar venta de acciones por que el accionista comprador y vendedor son la misma persona";
		} else {
			
			System.out.println();
			if(traspasoaccionesModel.getCantidad_acciones_vendidas_aModel() == 0 && traspasoaccionesModel.getCantidad_acciones_vendidas_bModel() == 0) {
				error = 5;
				return error;
			}
			if(traspasoaccionesModel.getCantidad_acciones_vendidas_aModel() > accionistaVendedor.getCantidad_acciones_serie_a()
					|| traspasoaccionesModel.getCantidad_acciones_vendidas_bModel() > accionistaVendedor.getCantidad_acciones_serie_b()) {
				System.out.println("");
				System.out.println("");
				System.out.println("No se pudo realizar venta de acciones debido a que accionista vendedor no posee la cantidad de acciones serie A o serie B ingresadas por sistema");
				System.out.println("");
				System.out.println("");
				error = 2;
				return error;
				//error = "No se pudo realizar venta de acciones debido a que accionista vendedor no posee la cantidad de acciones serie A o serie B ingresadas por sistema";
			} else {
				
				Empresa empresa = (empresaService.findEmpresaActiva());
				Accion accion = empresa.getAccion();
				double valorAccionA = accion.getValorA();
				double valorAccionB = accion.getValorB();
				double montoTotal= (valorAccionA*traspasoaccionesModel.getCantidad_acciones_vendidas_aModel())+(valorAccionB*traspasoaccionesModel.getCantidad_acciones_vendidas_bModel());
				String.format("%.2f", montoTotal);
				traspasoaccionesModel.setMontoModel(montoTotal);
				//actualiza acciones de accionista vendedor
				accionistaVendedor.setCantidad_acciones_serie_a(accionistaVendedor.getCantidad_acciones_serie_a() - traspasoaccionesModel.getCantidad_acciones_vendidas_aModel());				
				accionistaVendedor.setCantidad_acciones_serie_b(accionistaVendedor.getCantidad_acciones_serie_b() - traspasoaccionesModel.getCantidad_acciones_vendidas_bModel());
				//actualiza acciones de accionista comprador
				accionistaComprador.setCantidad_acciones_serie_a(accionistaComprador.getCantidad_acciones_serie_a() + traspasoaccionesModel.getCantidad_acciones_vendidas_aModel());
				accionistaComprador.setCantidad_acciones_serie_b(accionistaComprador.getCantidad_acciones_serie_b() + traspasoaccionesModel.getCantidad_acciones_vendidas_bModel());
				
				accionistaService.addAccionista(accionistaVendedor);
				accionistaService.addAccionista(accionistaComprador);
				traspasoaccionesService.addTraspasoacciones(traspasoaccionesModel);
				error = 3;
				return error;
				//error = "Venta exitosa";
			}
		}
		
	}
	
	
	
	/**
	 * Agrega los traspasos de acciones al historial
	 * @param traspasoaccionesModel
	 * @return string
	 */
	/**
	@PostMapping("/addtraspasoacciones")
	public String addtraspasoacciones(@ModelAttribute(name="traspasoaccionesModel")TraspasoaccionesModel traspasoaccionesModel,
			Model model) {
		LOG.info("Call: "+ "addtraspasoacciones()" + "-- Param: "+ traspasoaccionesModel.toString());
		//Captura accionistas para realizar acciones
		AccionistaModel accionistaVendedor = accionistaService.findAccionistaByRutModel(traspasoaccionesModel.getRut_accionista_vendedor());
		AccionistaModel accionistaComprador = accionistaService.findAccionistaByRutModel(traspasoaccionesModel.getRut_accionista_comprador());
				
		if(accionistaVendedor.getRut().equals(accionistaComprador.getRut())) {
			System.out.println("");
			System.out.println("");
			System.out.println("No se pudo realizar venta de acciones por que el accionista comprador y vendedor son la misma persona");
			System.out.println("");
			System.out.println("");
			
		} else {
			if(traspasoaccionesModel.getCantidad_acciones_vendidas_a() > accionistaVendedor.getCantidad_acciones_serie_a()
					|| traspasoaccionesModel.getCantidad_acciones_vendidas_b() > accionistaVendedor.getCantidad_acciones_serie_b()) {
				System.out.println("");
				System.out.println("");
				System.out.println("No se pudo realizar venta de acciones debido a que accionista vendedor no posee la cantidad de acciones serie A o serie B ingresadas por sistema");
				System.out.println("");
				System.out.println("");
				
			} else {
				//actualiza acciones de accionista vendedor
				accionistaVendedor.setCantidad_acciones_serie_a(accionistaVendedor.getCantidad_acciones_serie_a() - traspasoaccionesModel.getCantidad_acciones_vendidas_a());				
				accionistaVendedor.setCantidad_acciones_serie_b(accionistaVendedor.getCantidad_acciones_serie_b() - traspasoaccionesModel.getCantidad_acciones_vendidas_b());
				//actualiza acciones de accionista comprador
				accionistaComprador.setCantidad_acciones_serie_a(accionistaComprador.getCantidad_acciones_serie_a() + traspasoaccionesModel.getCantidad_acciones_vendidas_a());
				accionistaComprador.setCantidad_acciones_serie_b(accionistaComprador.getCantidad_acciones_serie_b() + traspasoaccionesModel.getCantidad_acciones_vendidas_b());
				
				accionistaService.addAccionista(accionistaVendedor);
				accionistaService.addAccionista(accionistaComprador);
				traspasoaccionesService.addTraspasoacciones(traspasoaccionesModel);
				
			}
		}
		return "redirect:/accionistas/traspasoacciones";
	}
	**/
	/**
	 * Redirecciona a graficos listando accionistas
	 * @return mav
	 */
	@GetMapping("/graficas")
	public ModelAndView redirectToGraficasView() {
		ModelAndView mav = new ModelAndView(ViewConstant.GRAFICOS_VIEW);
		return mav;
	}
	
	/**
	 * Elimina accionista.
	 * @param rut
	 * @return model and view
	 */	
	@RequestMapping(value="del", method=RequestMethod.GET)
	public @ResponseBody String removeaccionista(@RequestParam String rut) {
		LOG.info("Call: "+ "removeaccionista()" + "-- Param: "+ rut.toString());
			
			String message = "Accionista con el rut: " +rut+"eliminado";
		
			Accionista accionista = new Accionista();	
			
			accionista.setRut(rut);
			accionista = accionistaService.findAccionistaByAccionista(accionista);
			accionista.setEstado(0);
			accionista = accionistaService.addAccionista(accionista);
			
		return message;
	}
	
	@GetMapping(value="eliminar_acc")
	public ModelAndView eliminaaccionista(@RequestParam(name="rut", required=false) String rut) {
		LOG.info("Call: "+ "removeaccionista()" + "-- Param: "+ rut.toString());
						
			Accionista accionistaModel = new Accionista();	
			accionistaModel.setRut(rut);
			accionistaModel = accionistaService.findAccionistaByAccionista(accionistaModel);
			accionistaModel.setEstado(0);			
			accionistaModel = accionistaService.addAccionista(accionistaModel);
			
			ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
			List<AccionistaModel> ac = accionistaService.listAllAccionista_active();
				for(int i = 0;i<ac.size();i++) {
					if(ac.get(i).getEstadoModel() == 0) {
						ac.remove(i);
					}				
				}		
			mav.addObject("accionistas", ac );
			return mav;		
	}
	
	
	/**
	 * redirecciona a addaccionistasform.
	 * @param rut 
	 * @param model 
	 * @return string
	 **/
	
	@GetMapping("/addaccionistasform")
	private ModelAndView redirectToAddAccionistaForm(@RequestParam(name="rut", required=false) String rut,
			Model model) {
		LOG.info("Call: "+ "addaccionistasForm()" + "-- Param: "+ rut.toString());
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_FORM);
		AccionistaModel accionistaModel = new AccionistaModel();
		
		Accionista acc = new Accionista();
		acc.setRut(rut);
		
		accionistaModel.setRutModel(rut);
		if(!rut.equals("agregarAccionista")) {
			acc = accionistaService.findAccionistaByAccionistaModel(acc);
			mav.addObject("empresas", empresaService.listAllEmpresa());		
			mav.addObject("tiposAccionistas", tipoAccionistaService.listAllTiposAccionistas());
			mav.addObject("nacionalidades", nacionalidadService.listAllNacionalidades());
			mav.addObject("accionistaModel", accionistaConverter.entidad2Model(acc));
			mav.addObject("apoderadoModel", apoderadoService.listAllApoderados());
			mav.addObject("regiones", regionesService.listAllRegiones());
			
		}else {
			mav.addObject("empresas", empresaService.listAllEmpresa());		
			mav.addObject("tiposAccionistas", tipoAccionistaService.listAllTiposAccionistas());
			mav.addObject("nacionalidades", nacionalidadService.listAllNacionalidades());
			mav.addObject("accionistaModel", new AccionistaModel());
			mav.addObject("apoderadoModel", apoderadoService.listAllApoderados());
			mav.addObject("regiones", regionesService.listAllRegiones());
		}
		
		
		return mav;		
	}
	
	/**
	 * Agrega nuevos accionistas.
	 * @param accionistaModel 
	 * @param model 
	 * @return string
	 */	
	@RequestMapping(value="/addaccionista", method=RequestMethod.GET)
	public @ResponseBody String addUsuario(@RequestParam(name="id", required=false) String id, @RequestParam String empresaModel , @RequestParam String nombreModel, @RequestParam String rutModel, @RequestParam String apellido_paternoModel, @RequestParam String apellido_maternoModel, 
			@RequestParam String cantidad_acciones_serie_aModel, @RequestParam String cantidad_acciones_serie_bModel, @RequestParam String fecha_nacimientoModel, @RequestParam String fecha_ingresoModel, @RequestParam String apoderadoModel, 
			@RequestParam String esValidoModel, @RequestParam String EmailModel, @RequestParam String direccionModel, @RequestParam String nacionalidadModel, @RequestParam String tipoAccionistaModel, 
			@RequestParam String fonoModel, @RequestParam String ciudadModel, @RequestParam String comunaModel, @RequestParam String regionModel){
		LOG.info("Call: "+ "addUsuario()" + "-- Param: ");
		String salida = "";
	
		AccionistaModel accionistaModel = null;
		
		if(!id.equalsIgnoreCase("0")) {
			int idAccionista = Integer.parseInt(id);
			accionistaModel = accionistaService.findAccionistaModelById(idAccionista);
		}else {
			accionistaModel = new AccionistaModel();
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date fechaAux = null;
		java.util.Date fechaIAux = null;
		try {
			fechaAux = formatter.parse(fecha_nacimientoModel);
			fechaIAux = formatter.parse(fecha_ingresoModel);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("");
			System.out.println("");
			System.out.println("Error al convertir la fecha, ingresa una fecha correcta -> "+ e.getMessage());
			System.out.println("");
			System.out.println("");
			salida = "4";
			e.printStackTrace();
			return salida;
		}
		Date fecha = new Date(fechaAux.getTime());
		Date fechaI = new Date(fechaIAux.getTime());
		
		EmpresaModel em = new EmpresaModel();
		em.setIdEmpresaModel(Integer.parseInt(empresaModel));
		accionistaModel.setEmpresaModel(em);
		accionistaModel.setNombreModel(nombreModel);
		accionistaModel.setRutModel(rutModel);
		accionistaModel.setApellido_paternoModel(apellido_paternoModel);
		accionistaModel.setApellido_maternoModel(apellido_maternoModel);
		
		accionistaModel.setCantidad_acciones_serie_aModel(Integer.parseInt(cantidad_acciones_serie_aModel));
		accionistaModel.setCantidad_acciones_serie_bModel(Integer.parseInt(cantidad_acciones_serie_bModel));
		
		
		accionistaModel.setFecha_nacimientoModel(fecha);
		accionistaModel.setFecha_ingresoModel(fechaI);
		
		ApoderadoModel apoModel = new ApoderadoModel();
		apoModel.setIdApoderadoModel(Integer.parseInt(apoderadoModel));
		accionistaModel.setApoderadoModel(apoModel);
		
		accionistaModel.setEsValidoModel(esValidoModel);
		accionistaModel.setEmailModel(EmailModel);
		accionistaModel.setDireccionModel(direccionModel);
		
		NacionalidadModel nModel = new NacionalidadModel();
		nModel.setIdNacionalidadModel(Integer.parseInt(nacionalidadModel));
		accionistaModel.setNacionalidadModel(nModel);
		
		TipoAccionistaModel tModel = new TipoAccionistaModel();
		tModel.setIdTipoAccionistaModel(Integer.parseInt(tipoAccionistaModel));
		accionistaModel.setTipoAccionistaModel(tModel);
		
		accionistaModel.setFonoModel(fonoModel);
		accionistaModel.setComunaModel(comunaModel);
//		accionistaModel.setRegionModel(Integer.parseInt(regionModel));
		
		
		LOG.info("Call: "+ "addUsuario()" + "-- Param: "+ accionistaModel.toString());
		
		
		//Busca empresa segun ID activa
			
		EmpresaModel empresa = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		NacionalidadModel nM = nacionalidadConverter.entidad2Model(nacionalidadService.findNacionalidadById(accionistaModel.getNacionalidadModel().getIdNacionalidadModel()));
		TipoAccionistaModel tM = tipoAccionistaConverter.entidad2Model(tipoAccionistaService.findTipoAccionistaById(accionistaModel.getTipoAccionistaModel().getIdTipoAccionistaModel()));
		ApoderadoModel aM = apoderadoConverter.entidad2Model(apoderadoService.findApoderadoById(accionistaModel.getApoderadoModel().getIdApoderadoModel()));
	
		//Obtiene la cantidad de acciones serie A y serie B de la empresa
		int cantidadAccionesA = empresa.getCantidadAccionesSerieAModel();
		int cantidadAccionesB = empresa.getCantidadAccionesSerieBModel();
		
		//Calcula las acciones totales para sacar porcentaje de participacion
		int accionesTotales = cantidadAccionesA + cantidadAccionesB;
		
		
		//Calcula las acciones del nuevo accionista
		int accionesAccionista = accionistaModel.getCantidad_acciones_serie_aModel() + accionistaModel.getCantidad_acciones_serie_bModel();
		
		//Obtiene porcentaje de participacion
		double porcentajeParticipacion = (accionesAccionista * 100) / accionesTotales;
		String.format("%.2f", porcentajeParticipacion);
		
		//Serie el porcentaje de participacion del accionista
		accionistaModel.setPorcentaje_participacionModel(porcentajeParticipacion);		
		
		//Se crea lista se accionistasModel para realizar calculo de acciones totales de la empresa
		List<AccionistaModel> accionistasModel = new ArrayList<AccionistaModel>();
		accionistasModel = accionistaService.listAllAccionista();
		
		//se crean variables para tener la cantidad parcial de acciones A y B de accionistas
		int accionesParcialesA = 0;
		int accionesParcialesB = 0;
		
		//Se recorre lista para calcular acciones parciales
		for (AccionistaModel accionista : accionistasModel) {
			accionesParcialesA += accionista.getCantidad_acciones_serie_aModel();
			accionesParcialesB += accionista.getCantidad_acciones_serie_bModel();
		}		
		//se crean variables para tener la cantidad de acciones faltantes en empresa
		int accionesFaltantesA = empresa.getCantidadAccionesSerieAModel() - accionesParcialesA;
		int accionesFaltantesB = empresa.getCantidadAccionesSerieBModel() - accionesParcialesB;
		
		//busca datos de la base de datos del accionista a editar.
		Accionista accionistaModelOfBBDD = new Accionista();
				
		int accionesFaltantesAValidations = 0;
		int accionesFaltantesBValidations = 0;
		if(accionistaModel.getIdAccionistaModel() != 0 ) {
			
			accionistaModelOfBBDD.setRut(accionistaModel.getRutModel());			
			accionistaModelOfBBDD = accionistaService.findAccionistaByRut(accionistaModelOfBBDD);
			accionesFaltantesAValidations = accionesFaltantesA + accionistaModelOfBBDD.getCantidad_acciones_serie_a();
			accionesFaltantesBValidations = accionesFaltantesB + accionistaModelOfBBDD.getCantidad_acciones_serie_b();
			
		}else {
			accionesFaltantesAValidations = accionesFaltantesA;
			accionesFaltantesBValidations = accionesFaltantesB;
		}
		
		if(accionesFaltantesAValidations < accionistaModel.getCantidad_acciones_serie_aModel() ) {
			System.out.println();
			System.out.println();
			System.out.println("No se puede agregar accionista por que excede el numero total de acciones A");
			System.out.println();
			System.out.println();
			salida = "1";
		}else if(accionesFaltantesBValidations < accionistaModel.getCantidad_acciones_serie_bModel() ) {
			System.out.println();
			System.out.println();
			System.out.println("No se puede agregar accionista por que excede el numero total de acciones B");
			System.out.println();
			System.out.println();
			salida = "2";
		}else {
			accionistaModel.setEmpresaModel(empresa);
			accionistaModel.setNacionalidadModel(nM);
			accionistaModel.setTipoAccionistaModel(tM);
			accionistaModel.setApoderadoModel(aM);
			
			if(null != accionistaService.addAccionista(accionistaConverter.modelo2Entidad(accionistaModel))) {
//				model.addAttribute("result", 1);
				salida = "redirect:/accionistas/adminaccionistas";  
				salida = "3";
			} else {
//				model.addAttribute("result", 0);
				salida = "5";
			}
		}
		
		return salida; 
	}


	
	/**
	 * Cancel.
	 * @return the model and view
	 */
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllUsuarios() -- Al cancelar");
		ModelAndView mav = new ModelAndView(ViewConstant.ACCIONISTAS_VIEW);
		mav.addObject("accionista", new Accionista());
		mav.addObject("accionistas", accionistaService.listAllAccionista());
		return mav;
	}	

	
	/**
	 * lista historial de venta de acciones.
	 * @return the model and view
	 */
	@GetMapping("listtraspasosacciones")
	public ModelAndView listAlltraspasoacciones() {
		LOG.info("Call: " + "listAllTraspasoacciones() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.HISTORIAL_TRASPASO);
		mav.addObject("traspasosacciones", traspasoaccionesService.listAllTraspasosacciones());
		return mav;
	}
	
	@RequestMapping(value="act", method=RequestMethod.GET)
	public @ResponseBody String updateAccionista(@RequestParam String rut) {
		
		String message = "Accionista con el rut: " +rut+" activo";
		
		Accionista acc = new Accionista();
		acc.setRut(rut);
		acc = accionistaService.findAccionistaByAccionista(acc);
		acc.setEstado(1);
		acc = accionistaService.updateAccionista(acc);
		
		return message;
		
	}
}
