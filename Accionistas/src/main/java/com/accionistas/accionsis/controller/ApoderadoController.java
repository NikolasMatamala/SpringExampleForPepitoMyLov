package com.accionistas.accionsis.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.component.ApoderadoConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Apoderado;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.ApoderadoModel;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.ApoderadoService;
import com.accionistas.accionsis.service.RegionesService;;

// TODO: Auto-generated Javadoc
/**
 * Clase ApoderadoController.
 */
@Controller
@RequestMapping("apoderados")
public class ApoderadoController {
	
/** LOG constante. */
private static final Log LOG = LogFactory.getLog(ApoderadoController.class);
	
	/** The apoderado service. */
	@Autowired
	@Qualifier("apoderadoServiceImp")
	private ApoderadoService apoderadoService;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;
	
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;
	
	@Autowired
	@Qualifier("apoderadoConverter")
	private ApoderadoConverter apoderadoConverter;
	
	@Autowired
	@Qualifier("regionesServiceImp")
	private RegionesService regionesService;
	
	/**
	 * Lista todos los apoderados.
	 *
	 * @return the model and view
	 */
	@GetMapping("/listapoderados")
	public ModelAndView listAllApoderados() {
		LOG.info("Call: "+ "listAllApoderados() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.APODERADOS_VIEW);
		mav.addObject("apoderados", apoderadoService.listAllApoderados());
		return mav;
	}	
	
	@GetMapping("/listapoderadosAdmin")
	public ModelAndView listAllApoderadosAdmin() {
		LOG.info("Call: "+ "listAllApoderados() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.APODERADOS_LIST);
		mav.addObject("apoderados", apoderadoService.listAllApoderados());
		return mav;
	}
	
	@RequestMapping(value="/addapoderado", method=RequestMethod.GET)
	public @ResponseBody String addApoderado(@RequestParam(name="id", required=false) String id,@RequestParam String nombreModel, @RequestParam String rutModel, 
			@RequestParam String apellido_paternoModel, @RequestParam String apellido_maternoModel,
			@RequestParam String emailModel, @RequestParam String fonoModel) {
		ApoderadoModel apoderadoModel = null;
		if(!id.equalsIgnoreCase("0")) {
			int idApoderado = Integer.parseInt(id);
			apoderadoModel = apoderadoConverter.entidad2Model(apoderadoService.findApoderadoById(idApoderado));
		}else {
			apoderadoModel = new ApoderadoModel();
		}
		String respuesta = "";
		
		LOG.info("Call: "+"addApoderado()"+"--Param: "+ apoderadoModel.toString());
		
		apoderadoModel.setRutModel(rutModel);
		apoderadoModel.setNombreModel(nombreModel);
		apoderadoModel.setApellido_paternoModel(apellido_paternoModel);
		apoderadoModel.setApellido_maternoModel(apellido_maternoModel);
		apoderadoModel.setEmailModel(emailModel);
		apoderadoModel.setFonoModel(fonoModel);
		
		
		if(rutModel != "" && nombreModel != "" && apellido_paternoModel != "" &&
				apellido_maternoModel != "" && emailModel != "" && fonoModel != "") {
			if(null != apoderadoService.addApoderado(apoderadoModel)){
//			model.addAttribute("result", 1);
					respuesta = "1";
			}
		} else {
//			model.addAttribute("result", 0);
			respuesta = "2";
		}
		return respuesta;
	}
	
	@GetMapping("/addapoderadoform")
	private ModelAndView redirectToAddApoderadoForm(@RequestParam(name="id", required=false) int id, 
			Model model) {
		
		ModelAndView mav = new ModelAndView(ViewConstant.APODERADO_FORM);
		ApoderadoModel apoderadoModel = new ApoderadoModel();
		if(id != 0) {
			apoderadoModel = apoderadoService.findApoderadoByIdModel(id);
			mav.addObject("regiones", regionesService.listAllRegiones());
		}						
		mav.addObject(apoderadoModel);
		mav.addObject("regiones", regionesService.listAllRegiones());
		return mav;		
	}
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		LOG.info("Call: "+ "listAllApoderados() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.APODERADOS_LIST);
		mav.addObject("apoderados", apoderadoService.listAllApoderados());
		return mav;
	}
	
	@RequestMapping(value="del", method=RequestMethod.GET)
	public ModelAndView removeApoderado(@RequestParam(name="rut", required=true)String rut, Model model) {
		apoderadoService.removeApoderado(rut);
		return listAllApoderados();
	}
	
	
	@RequestMapping(value="delete", method=RequestMethod.GET)
	public @ResponseBody String updateApoderado_delete(@RequestParam String rut) {
		
		String message = "Apoderado con el rut: " +rut+" eliminado";
		
		Apoderado ap = new Apoderado();
		ap.setRut(rut);
		ap = apoderadoConverter.modelo2Entidad(apoderadoService.findApoderadoByRut(ap.getRut()));
		ap.setEstado(0);
		ap = apoderadoService.updateApoderado(ap);
		
		return message;
		
	}
	
	@RequestMapping(value="act", method=RequestMethod.GET)
	public @ResponseBody String updateApoderado(@RequestParam String rut) {
		
		String message = "Apoderado con el rut: " +rut+" activado";
		
		Apoderado ap = new Apoderado();
		ap.setRut(rut);
		ap = apoderadoConverter.modelo2Entidad(apoderadoService.findApoderadoByRut(ap.getRut()));
		ap.setEstado(1);
		ap = apoderadoService.updateApoderado(ap);
		
		return message;
		
	}

}
