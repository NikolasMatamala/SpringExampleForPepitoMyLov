package com.accionistas.accionsis.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accionistas.accionsis.component.EmpresaConverter;
import com.accionistas.accionsis.constant.ViewConstant;
import com.accionistas.accionsis.entity.Empresa;
import com.accionistas.accionsis.model.AccionModel;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.EmpresaModel;
import com.accionistas.accionsis.model.GiroComercialModel;
import com.accionistas.accionsis.service.AccionService;
import com.accionistas.accionsis.service.AccionistaService;
import com.accionistas.accionsis.service.EmpresaService;
import com.accionistas.accionsis.service.GiroComercialService;


/**
 * Clase EmpresaController.
 */
@Controller
@RequestMapping("/empresas")
public class EmpresaController {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(EmpresaController.class);
	
	/** The empresa service. */
	@Autowired
	@Qualifier("empresaServiceImp")
	private EmpresaService empresaService;
	
	@Autowired
	@Qualifier("giroComercialServiceImp")
	private GiroComercialService giroComercialService;
	
	@Autowired
	@Qualifier("accionServiceImp")
	private AccionService accionService;
	
	@Autowired
	@Qualifier("accionistaServiceImp")
	private AccionistaService accionistaService;

	@Autowired
	@Qualifier("empresaConverter")
	private EmpresaConverter empresaConverter;
	
	/**
	 * Lista todas las empresas.
	 * @return the model and view
	 */
	@GetMapping("/listempresas")
	public ModelAndView listAllEmpresas() {
		LOG.info("Call: "+ "listEmpresa() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.PRINCIPAL_VIEW);		
		EmpresaModel empresaModel = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		GiroComercialModel giroComercialModel = giroComercialService.findGiroComercialModelByModel(empresaModel.getGiroComercialModel());
		
		
		mav.addObject(empresaModel);
		mav.addObject(giroComercialModel);
		return mav;
	}
	
	/**
	 * Lista empresa.
	 *
	 * @return the model and view
	 */
	@GetMapping("listempresa")
	public ModelAndView listEmpresa() {
		LOG.info("Call: "+ "listEmpresa() -- desde controller");
		ModelAndView mav = new ModelAndView(ViewConstant.PRINCIPAL_VIEW);
		
		EmpresaModel empresaModel = empresaConverter.entidad2Model(empresaService.findEmpresaActiva());
		AccionModel accionModel = accionService.findAccionById_accionModel(6);		
		GiroComercialModel giroComercialModel = giroComercialService.findGiroComercialModelByModel(empresaModel.getGiroComercialModel());
		
		List<AccionistaModel> accionistasModel = new ArrayList<AccionistaModel>();
		accionistasModel = accionistaService.listAllAccionista_active();
		
		empresaModel.setCantidadAccionistasModel(accionistasModel.size());
		
		mav.addObject(empresaModel);
		mav.addObject(giroComercialModel);
		mav.addObject(accionModel);
		return mav;
	}
	@PostMapping("/addempresa")
	public String addEmpresa(@ModelAttribute(name="empresaModel") EmpresaModel empresaModel, 
			Model model) {
		LOG.info("Call: "+"addAccion()"+"--Param: "+ empresaModel.toString());
		
		empresaModel.setCantidadAccionistasModel(0);
		
		if(null != empresaService.addEmpresa(empresaModel)){
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		return "redirect:/empresas/listempresa";
	}
	
	@GetMapping("/addempresaform")
	private String redirectToAddEmpresaForm(@RequestParam(name="id", required=false) int id, 
			Model model) {
		EmpresaModel empresaModel = new EmpresaModel();
		Empresa emp = new Empresa();
		emp.setIdEmpresa(id);
		if(id != 0) {
			empresaModel = (empresaService.findEmpresaByid(emp));	
		}
		model.addAttribute("empresaModel", empresaModel);
		return ViewConstant.EMPRESA_FORM;
	}
	
	@GetMapping("/cancel")
	public ModelAndView cancel() {
		ModelAndView mav = new ModelAndView(ViewConstant.PRINCIPAL_VIEW);
		Empresa emp = new Empresa();
		emp.setIdEmpresa(2);
		EmpresaModel empresaModel = (empresaService.findEmpresaByid(emp));
		AccionModel accionModel = accionService.findAccionById_accionModel(4);
		GiroComercialModel giroComercialModel = giroComercialService.findGiroComercialModelByModel(empresaModel.getGiroComercialModel());
		mav.addObject(empresaModel);
		mav.addObject(giroComercialModel);
		mav.addObject(accionModel);
		return mav;
	}

	 
}
