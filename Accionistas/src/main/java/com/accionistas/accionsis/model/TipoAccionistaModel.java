package com.accionistas.accionsis.model;

public class TipoAccionistaModel {
	
	private int idTipoAccionistaModel;
	private String descripcion_tipo_accionistaModel;
	
	public TipoAccionistaModel() {
		super();
	}

	public TipoAccionistaModel(int idTipoAccionistaModel, String descripcion_tipo_accionistaModel) {
		super();
		this.idTipoAccionistaModel = idTipoAccionistaModel;
		this.descripcion_tipo_accionistaModel = descripcion_tipo_accionistaModel;
	}

	@Override
	public String toString() {
		return "TipoAccionistaModel [idTipoAccionistaModel=" + idTipoAccionistaModel
				+ ", descripcion_tipo_accionistaModel=" + descripcion_tipo_accionistaModel + "]";
	}

	public int getIdTipoAccionistaModel() {
		return idTipoAccionistaModel;
	}

	public void setIdTipoAccionistaModel(int idTipoAccionistaModel) {
		this.idTipoAccionistaModel = idTipoAccionistaModel;
	}

	public String getDescripcion_tipo_accionistaModel() {
		return descripcion_tipo_accionistaModel;
	}

	public void setDescripcion_tipo_accionistaModel(String descripcion_tipo_accionistaModel) {
		this.descripcion_tipo_accionistaModel = descripcion_tipo_accionistaModel;
	}
	
	
	

}
