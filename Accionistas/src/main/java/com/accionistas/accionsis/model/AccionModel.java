package com.accionistas.accionsis.model;

import java.sql.Date;

public class AccionModel {

	private int idAccionModel;
	private double valorAModel;
	private double valorBModel;
	private EmpresaModel empresaModel;
	private Date fechaIngresoModel;
	
	public AccionModel() {
		super();
	}

	public AccionModel(int idAccionModel, double valorAModel, double valorBModel, EmpresaModel empresaModel,
			Date fechaIngresoModel) {
		super();
		this.idAccionModel = idAccionModel;
		this.valorAModel = valorAModel;
		this.valorBModel = valorBModel;
		this.empresaModel = empresaModel;
		this.fechaIngresoModel = fechaIngresoModel;
	}

	@Override
	public String toString() {
		return "AccionModel [idAccionModel=" + idAccionModel + ", valorAModel=" + valorAModel + ", valorBModel="
				+ valorBModel + ", empresaModel=" + empresaModel + ", fechaIngresoModel=" + fechaIngresoModel + "]";
	}

	public int getIdAccionModel() {
		return idAccionModel;
	}

	public void setIdAccionModel(int idAccionModel) {
		this.idAccionModel = idAccionModel;
	}

	public double getValorAModel() {
		return valorAModel;
	}

	public void setValorAModel(double valorAModel) {
		this.valorAModel = valorAModel;
	}

	public double getValorBModel() {
		return valorBModel;
	}

	public void setValorBModel(double valorBModel) {
		this.valorBModel = valorBModel;
	}

	public EmpresaModel getEmpresaModel() {
		return empresaModel;
	}

	public void setEmpresaModel(EmpresaModel empresaModel) {
		this.empresaModel = empresaModel;
	}

	public Date getFechaIngresoModel() {
		return fechaIngresoModel;
	}

	public void setFechaIngresoModel(Date fechaIngresoModel) {
		this.fechaIngresoModel = fechaIngresoModel;
	}
	
	
}
