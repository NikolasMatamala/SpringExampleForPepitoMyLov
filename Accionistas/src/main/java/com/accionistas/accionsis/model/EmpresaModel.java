package com.accionistas.accionsis.model;

/**
 * Clase EmpresaModel.
 */
public class EmpresaModel {

	/** id. */
	private int idEmpresaModel;
	
	/** rut. */
	private String rutModel;
	
	/** razon social. */
	private String razonSocialModel;
	
	/** logo. */
	private String logoModel;
	
	/** web. */
	private String webModel;
	
	/** cantidad acciones. */
	private int cantidadAccionesSerieAModel;
	
	private int cantidadAccionesSerieBModel;
	
	/** cantidad accionistas. */
	private int cantidadAccionistasModel;
	
	/** mail. */
	private String EmailModel;
	
	/** direccion. */
	private String direccionModel;
	
	/** telefono. */
	private String telefonoModel;
	
	/** representante legalrut. */
	private RepresentanteModel representanteModel;
	
	/** tipo giro comercialid. */
	private GiroComercialModel giroComercialModel;

	public EmpresaModel() {
		super();
	}

	public EmpresaModel(int idEmpresaModel, String rutModel, String razonSocialModel, String logoModel, String webModel,
			int cantidadAccionesSerieAModel, int cantidadAccionesSerieBModel, int cantidadAccionistasModel,
			String emailModel, String direccionModel, String telefonoModel, RepresentanteModel representanteModel,
			GiroComercialModel giroComercialModel) {
		super();
		this.idEmpresaModel = idEmpresaModel;
		this.rutModel = rutModel;
		this.razonSocialModel = razonSocialModel;
		this.logoModel = logoModel;
		this.webModel = webModel;
		this.cantidadAccionesSerieAModel = cantidadAccionesSerieAModel;
		this.cantidadAccionesSerieBModel = cantidadAccionesSerieBModel;
		this.cantidadAccionistasModel = cantidadAccionistasModel;
		EmailModel = emailModel;
		this.direccionModel = direccionModel;
		this.telefonoModel = telefonoModel;
		this.representanteModel = representanteModel;
		this.giroComercialModel = giroComercialModel;
	}

	@Override
	public String toString() {
		return "EmpresaModel [idEmpresaModel=" + idEmpresaModel + ", rutModel=" + rutModel + ", razonSocialModel="
				+ razonSocialModel + ", logoModel=" + logoModel + ", webModel=" + webModel
				+ ", cantidadAccionesSerieAModel=" + cantidadAccionesSerieAModel + ", cantidadAccionesSerieBModel="
				+ cantidadAccionesSerieBModel + ", cantidadAccionistasModel=" + cantidadAccionistasModel
				+ ", EmailModel=" + EmailModel + ", direccionModel=" + direccionModel + ", telefonoModel="
				+ telefonoModel + ", representanteModel=" + representanteModel + ", giroComercialModel="
				+ giroComercialModel + "]";
	}

	public int getIdEmpresaModel() {
		return idEmpresaModel;
	}

	public void setIdEmpresaModel(int idEmpresaModel) {
		this.idEmpresaModel = idEmpresaModel;
	}

	public String getRutModel() {
		return rutModel;
	}

	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}

	public String getRazonSocialModel() {
		return razonSocialModel;
	}

	public void setRazonSocialModel(String razonSocialModel) {
		this.razonSocialModel = razonSocialModel;
	}

	public String getLogoModel() {
		return logoModel;
	}

	public void setLogoModel(String logoModel) {
		this.logoModel = logoModel;
	}

	public String getWebModel() {
		return webModel;
	}

	public void setWebModel(String webModel) {
		this.webModel = webModel;
	}

	public int getCantidadAccionesSerieAModel() {
		return cantidadAccionesSerieAModel;
	}

	public void setCantidadAccionesSerieAModel(int cantidadAccionesSerieAModel) {
		this.cantidadAccionesSerieAModel = cantidadAccionesSerieAModel;
	}

	public int getCantidadAccionesSerieBModel() {
		return cantidadAccionesSerieBModel;
	}

	public void setCantidadAccionesSerieBModel(int cantidadAccionesSerieBModel) {
		this.cantidadAccionesSerieBModel = cantidadAccionesSerieBModel;
	}

	public int getCantidadAccionistasModel() {
		return cantidadAccionistasModel;
	}

	public void setCantidadAccionistasModel(int cantidadAccionistasModel) {
		this.cantidadAccionistasModel = cantidadAccionistasModel;
	}

	public String getEmailModel() {
		return EmailModel;
	}

	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}

	public String getDireccionModel() {
		return direccionModel;
	}

	public void setDireccionModel(String direccionModel) {
		this.direccionModel = direccionModel;
	}

	public String getTelefonoModel() {
		return telefonoModel;
	}

	public void setTelefonoModel(String telefonoModel) {
		this.telefonoModel = telefonoModel;
	}

	public RepresentanteModel getRepresentanteModel() {
		return representanteModel;
	}

	public void setRepresentanteModel(RepresentanteModel representanteModel) {
		this.representanteModel = representanteModel;
	}

	public GiroComercialModel getGiroComercialModel() {
		return giroComercialModel;
	}

	public void setGiroComercialModel(GiroComercialModel giroComercialModel) {
		this.giroComercialModel = giroComercialModel;
	}

	

	
}