package com.accionistas.accionsis.model;

public class RepresentanteModel {

	/*id*/
	private int idRepresentanteModel;
	
	/*
	 * rut
	 */
	private String rutModel;
	/*
	 * nombre
	 */
	private String nombreModel;
	/*
	 * apellido_paterno
	 */
	private String apellido_paternoModel;
	/*
	 * apellido_materno
	 */
	private String apellido_maternoModel;
	/*
	 * direccion
	 */
	private String direccionModel;
	/*
	 * mail
	 */
	private String EmailModel;
	/*
	 * telefono
	 */
	private String telefonoModel;
	
	private RegionesModel regionesModel;
	
	private int estadoModel;

	public RepresentanteModel() {
		super();
	}

	public RepresentanteModel(int idRepresentanteModel, String rutModel, String nombreModel,
			String apellido_paternoModel, String apellido_maternoModel, String direccionModel, String emailModel,
			String telefonoModel, RegionesModel regionesModel, int estadoModel) {
		super();
		this.idRepresentanteModel = idRepresentanteModel;
		this.rutModel = rutModel;
		this.nombreModel = nombreModel;
		this.apellido_paternoModel = apellido_paternoModel;
		this.apellido_maternoModel = apellido_maternoModel;
		this.direccionModel = direccionModel;
		EmailModel = emailModel;
		this.telefonoModel = telefonoModel;
		this.regionesModel = regionesModel;
		this.estadoModel = estadoModel;
	}

	@Override
	public String toString() {
		return "RepresentanteModel [idRepresentanteModel=" + idRepresentanteModel + ", rutModel=" + rutModel
				+ ", nombreModel=" + nombreModel + ", apellido_paternoModel=" + apellido_paternoModel
				+ ", apellido_maternoModel=" + apellido_maternoModel + ", direccionModel=" + direccionModel
				+ ", EmailModel=" + EmailModel + ", telefonoModel=" + telefonoModel + ", regionesModel=" + regionesModel
				+ ", estadoModel=" + estadoModel + "]";
	}

	public int getIdRepresentanteModel() {
		return idRepresentanteModel;
	}

	public void setIdRepresentanteModel(int idRepresentanteModel) {
		this.idRepresentanteModel = idRepresentanteModel;
	}

	public String getRutModel() {
		return rutModel;
	}

	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}

	public String getNombreModel() {
		return nombreModel;
	}

	public void setNombreModel(String nombreModel) {
		this.nombreModel = nombreModel;
	}

	public String getApellido_paternoModel() {
		return apellido_paternoModel;
	}

	public void setApellido_paternoModel(String apellido_paternoModel) {
		this.apellido_paternoModel = apellido_paternoModel;
	}

	public String getApellido_maternoModel() {
		return apellido_maternoModel;
	}

	public void setApellido_maternoModel(String apellido_maternoModel) {
		this.apellido_maternoModel = apellido_maternoModel;
	}

	public String getDireccionModel() {
		return direccionModel;
	}

	public void setDireccionModel(String direccionModel) {
		this.direccionModel = direccionModel;
	}

	public String getEmailModel() {
		return EmailModel;
	}

	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}

	public String getTelefonoModel() {
		return telefonoModel;
	}

	public void setTelefonoModel(String telefonoModel) {
		this.telefonoModel = telefonoModel;
	}

	public RegionesModel getRegionesModel() {
		return regionesModel;
	}

	public void setRegionesModel(RegionesModel regionesModel) {
		this.regionesModel = regionesModel;
	}

	public int getEstadoModel() {
		return estadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		this.estadoModel = estadoModel;
	}

	
}
