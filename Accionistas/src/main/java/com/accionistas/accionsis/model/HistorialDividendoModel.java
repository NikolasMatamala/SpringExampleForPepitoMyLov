package com.accionistas.accionsis.model;

import java.sql.Date;

public class HistorialDividendoModel {
	
	private int idHistorialDividendoModel;
	
	private DividendoModel dividendoModel;
	
	private Date fechaDeclaracionModel;
	
	private Date fechaRegristroModel;
	
	private double dividendoPorAccionModel;
	
	private double dividendoTotalModel;

	public HistorialDividendoModel() {
		super();
	}

	public HistorialDividendoModel(int idHistorialDividendoModel, DividendoModel dividendoModel,
			Date fechaDeclaracionModel, Date fechaRegristroModel, double dividendoPorAccionModel,
			double dividendoTotalModel) {
		super();
		this.idHistorialDividendoModel = idHistorialDividendoModel;
		this.dividendoModel = dividendoModel;
		this.fechaDeclaracionModel = fechaDeclaracionModel;
		this.fechaRegristroModel = fechaRegristroModel;
		this.dividendoPorAccionModel = dividendoPorAccionModel;
		this.dividendoTotalModel = dividendoTotalModel;
	}

	@Override
	public String toString() {
		return "HistorialDividendoModel [idHistorialDividendoModel=" + idHistorialDividendoModel + ", dividendoModel="
				+ dividendoModel + ", fechaDeclaracionModel=" + fechaDeclaracionModel + ", fechaRegristroModel="
				+ fechaRegristroModel + ", dividendoPorAccionModel=" + dividendoPorAccionModel
				+ ", dividendoTotalModel=" + dividendoTotalModel + "]";
	}

	public int getIdHistorialDividendoModel() {
		return idHistorialDividendoModel;
	}

	public void setIdHistorialDividendoModel(int idHistorialDividendoModel) {
		this.idHistorialDividendoModel = idHistorialDividendoModel;
	}

	public DividendoModel getDividendoModel() {
		return dividendoModel;
	}

	public void setDividendoModel(DividendoModel dividendoModel) {
		this.dividendoModel = dividendoModel;
	}

	public Date getFechaDeclaracionModel() {
		return fechaDeclaracionModel;
	}

	public void setFechaDeclaracionModel(Date fechaDeclaracionModel) {
		this.fechaDeclaracionModel = fechaDeclaracionModel;
	}

	public Date getFechaRegristroModel() {
		return fechaRegristroModel;
	}

	public void setFechaRegristroModel(Date fechaRegristroModel) {
		this.fechaRegristroModel = fechaRegristroModel;
	}

	public double getDividendoPorAccionModel() {
		return dividendoPorAccionModel;
	}

	public void setDividendoPorAccionModel(double dividendoPorAccionModel) {
		this.dividendoPorAccionModel = dividendoPorAccionModel;
	}

	public double getDividendoTotalModel() {
		return dividendoTotalModel;
	}

	public void setDividendoTotalModel(double dividendoTotalModel) {
		this.dividendoTotalModel = dividendoTotalModel;
	}

	

}
