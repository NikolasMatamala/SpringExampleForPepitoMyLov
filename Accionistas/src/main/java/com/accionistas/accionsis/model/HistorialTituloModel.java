package com.accionistas.accionsis.model;

public class HistorialTituloModel {

	private int idHistorialTituloModel;
	
	private AccionistaModel accionistaModel;
	
	private String detalleSolicitudModel;

	public HistorialTituloModel() {
		super();
	}

	public HistorialTituloModel(int idHistorialTituloModel, AccionistaModel accionistaModel,
			String detalleSolicitudModel) {
		super();
		this.idHistorialTituloModel = idHistorialTituloModel;
		this.accionistaModel = accionistaModel;
		this.detalleSolicitudModel = detalleSolicitudModel;
	}

	@Override
	public String toString() {
		return "HistorialTituloModel [idHistorialTituloModel=" + idHistorialTituloModel + ", accionistaModel="
				+ accionistaModel + ", detalleSolicitudModel=" + detalleSolicitudModel + "]";
	}

	public int getIdHistorialTituloModel() {
		return idHistorialTituloModel;
	}

	public void setIdHistorialTituloModel(int idHistorialTituloModel) {
		this.idHistorialTituloModel = idHistorialTituloModel;
	}

	public AccionistaModel getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(AccionistaModel accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	public String getDetalleSolicitudModel() {
		return detalleSolicitudModel;
	}

	public void setDetalleSolicitudModel(String detalleSolicitudModel) {
		this.detalleSolicitudModel = detalleSolicitudModel;
	}

	
	
	
}
