package com.accionistas.accionsis.model;

import java.sql.Date;


public class DividendoModel {
	
	private int idDividendoModel;
	
	private JuntaAccionistaModel juntaAccionistaModel;
	
	/*AccionistasRut*/
	private AccionistaModel accionistaModel;
	
	/*FechaDdelaracion*/
	private Date fechaDeclaracionModel;
	
	/*FechaRegistro*/
	private Date fechaRegistroModel;
	
	/*DividendoPorAccionA*/
	private double dividendoAccionAModel;
	
	/*DividendoPorAccionB*/
	private double dividendoAccionBModel;
	
	/*DividendoTotalA*/
	private double dividendoTotalModel;
	
	/*Estado*/
	private int EstadoModel;

	public DividendoModel() {
		super();
	}

	public DividendoModel(int idDividendoModel, JuntaAccionistaModel juntaAccionistaModel,
			AccionistaModel accionistaModel, Date fechaDeclaracionModel, Date fechaRegistroModel,
			float dividendoAccionAModel, double dividendoAccionBModel, double dividendoTotalModel, int estadoModel) {
		super();
		this.idDividendoModel = idDividendoModel;
		this.juntaAccionistaModel = juntaAccionistaModel;
		this.accionistaModel = accionistaModel;
		this.fechaDeclaracionModel = fechaDeclaracionModel;
		this.fechaRegistroModel = fechaRegistroModel;
		this.dividendoAccionAModel = dividendoAccionAModel;
		this.dividendoAccionBModel = dividendoAccionBModel;
		this.dividendoTotalModel = dividendoTotalModel;
		EstadoModel = estadoModel;
	}

	@Override
	public String toString() {
		return "DividendoModel [idDividendoModel=" + idDividendoModel + ", juntaAccionistaModel=" + juntaAccionistaModel
				+ ", accionistaModel=" + accionistaModel + ", fechaDeclaracionModel=" + fechaDeclaracionModel
				+ ", fechaRegistroModel=" + fechaRegistroModel + ", dividendoAccionAModel=" + dividendoAccionAModel
				+ ", dividendoAccionBModel=" + dividendoAccionBModel + ", dividendoTotalModel=" + dividendoTotalModel
				+ ", EstadoModel=" + EstadoModel + "]";
	}

	public int getIdDividendoModel() {
		return idDividendoModel;
	}

	public void setIdDividendoModel(int idDividendoModel) {
		this.idDividendoModel = idDividendoModel;
	}

	public JuntaAccionistaModel getJuntaAccionistaModel() {
		return juntaAccionistaModel;
	}

	public void setJuntaAccionistaModel(JuntaAccionistaModel juntaAccionistaModel) {
		this.juntaAccionistaModel = juntaAccionistaModel;
	}

	public AccionistaModel getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(AccionistaModel accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	public Date getFechaDeclaracionModel() {
		return fechaDeclaracionModel;
	}

	public void setFechaDeclaracionModel(Date fechaDeclaracionModel) {
		this.fechaDeclaracionModel = fechaDeclaracionModel;
	}

	public Date getFechaRegistroModel() {
		return fechaRegistroModel;
	}

	public void setFechaRegistroModel(Date fechaRegistroModel) {
		this.fechaRegistroModel = fechaRegistroModel;
	}

	public double getDividendoAccionAModel() {
		return dividendoAccionAModel;
	}

	public void setDividendoAccionAModel(double dividendoAccionAModel) {
		this.dividendoAccionAModel = dividendoAccionAModel;
	}

	public double getDividendoAccionBModel() {
		return dividendoAccionBModel;
	}

	public void setDividendoAccionBModel(double dividendoAccionBModel) {
		this.dividendoAccionBModel = dividendoAccionBModel;
	}

	public double getDividendoTotalModel() {
		return dividendoTotalModel;
	}

	public void setDividendoTotalModel(double dividendoTotalModel) {
		this.dividendoTotalModel = dividendoTotalModel;
	}

	public int getEstadoModel() {
		return EstadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		EstadoModel = estadoModel;
	}

	
}
