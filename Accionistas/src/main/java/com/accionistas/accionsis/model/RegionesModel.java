package com.accionistas.accionsis.model;

public class RegionesModel {
	
	private int idRegionModel;
	
	private String regionModel;

	public RegionesModel() {
		super();
	}

	public RegionesModel(int idRegionModel, String regionModel) {
		super();
		this.idRegionModel = idRegionModel;
		this.regionModel = regionModel;
	}

	@Override
	public String toString() {
		return "RegionesModel [idRegionModel=" + idRegionModel + ", regionModel=" + regionModel + "]";
	}

	public int getIdRegionModel() {
		return idRegionModel;
	}

	public void setIdRegionModel(int idRegionModel) {
		this.idRegionModel = idRegionModel;
	}

	public String getRegionModel() {
		return regionModel;
	}

	public void setRegionModel(String regionModel) {
		this.regionModel = regionModel;
	}

	
}
