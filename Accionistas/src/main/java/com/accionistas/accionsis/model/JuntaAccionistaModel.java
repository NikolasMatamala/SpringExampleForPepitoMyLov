package com.accionistas.accionsis.model;

import java.sql.Date;
import java.sql.Time;

public class JuntaAccionistaModel {

	private int idJuntaAccionistaModel;
	private String ubicacionModel;
	private Date fecha_realizacionModel;
	private Time hora_inicioModel;
	private Time hora_terminoModel;
	private String direccionModel;
	private Date fecha_difusionModel;
	private double asistenciaTotalModel;
	private int estadoModel;
	
	public JuntaAccionistaModel() {
		super();
	}

	public JuntaAccionistaModel(int idJuntaAccionistaModel, String ubicacionModel, Date fecha_realizacionModel,
			Time hora_inicioModel, Time hora_terminoModel, String direccionModel, Date fecha_difusionModel,
			double asistenciaTotalModel, int estadoModel) {
		super();
		this.idJuntaAccionistaModel = idJuntaAccionistaModel;
		this.ubicacionModel = ubicacionModel;
		this.fecha_realizacionModel = fecha_realizacionModel;
		this.hora_inicioModel = hora_inicioModel;
		this.hora_terminoModel = hora_terminoModel;
		this.direccionModel = direccionModel;
		this.fecha_difusionModel = fecha_difusionModel;
		this.asistenciaTotalModel = asistenciaTotalModel;
		this.estadoModel = estadoModel;
	}

	@Override
	public String toString() {
		return "JuntaAccionistaModel [idJuntaAccionistaModel=" + idJuntaAccionistaModel + ", ubicacionModel="
				+ ubicacionModel + ", fecha_realizacionModel=" + fecha_realizacionModel + ", hora_inicioModel="
				+ hora_inicioModel + ", hora_terminoModel=" + hora_terminoModel + ", direccionModel=" + direccionModel
				+ ", fecha_difusionModel=" + fecha_difusionModel + ", asistenciaTotalModel=" + asistenciaTotalModel
				+ ", estadoModel=" + estadoModel + "]";
	}

	public int getIdJuntaAccionistaModel() {
		return idJuntaAccionistaModel;
	}

	public void setIdJuntaAccionistaModel(int idJuntaAccionistaModel) {
		this.idJuntaAccionistaModel = idJuntaAccionistaModel;
	}

	public String getUbicacionModel() {
		return ubicacionModel;
	}

	public void setUbicacionModel(String ubicacionModel) {
		this.ubicacionModel = ubicacionModel;
	}

	public Date getFecha_realizacionModel() {
		return fecha_realizacionModel;
	}

	public void setFecha_realizacionModel(Date fecha_realizacionModel) {
		this.fecha_realizacionModel = fecha_realizacionModel;
	}

	public Time getHora_inicioModel() {
		return hora_inicioModel;
	}

	public void setHora_inicioModel(Time hora_inicioModel) {
		this.hora_inicioModel = hora_inicioModel;
	}

	public Time getHora_terminoModel() {
		return hora_terminoModel;
	}

	public void setHora_terminoModel(Time hora_terminoModel) {
		this.hora_terminoModel = hora_terminoModel;
	}

	public String getDireccionModel() {
		return direccionModel;
	}

	public void setDireccionModel(String direccionModel) {
		this.direccionModel = direccionModel;
	}

	public Date getFecha_difusionModel() {
		return fecha_difusionModel;
	}

	public void setFecha_difusionModel(Date fecha_difusionModel) {
		this.fecha_difusionModel = fecha_difusionModel;
	}

	public double getAsistenciaTotalModel() {
		return asistenciaTotalModel;
	}

	public void setAsistenciaTotalModel(double asistenciaTotalModel) {
		this.asistenciaTotalModel = asistenciaTotalModel;
	}

	public int getEstadoModel() {
		return estadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		this.estadoModel = estadoModel;
	}
	
	
	
	
	
	
}
