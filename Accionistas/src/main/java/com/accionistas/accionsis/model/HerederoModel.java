package com.accionistas.accionsis.model;

/*
 *HerederoModel
 */
public class HerederoModel {
	
	/*id*/
	private int idHerederoModel;
	
	/*
	 * Rut
	 */
	private String rutModel;
	/*
	 * Empresaid_empresa
	 */
	private EmpresaModel empresaModel;
	/*
	 * Nombre
	 */
	private String nombreModel;
	/*
	 * Apellido_paterno
	 */
	private String apellido_paternoModel;
	/*
	 * Apellido_materno
	 */
	private String apellido_maternoModel;
	/*
	 * cantidad_acciones_serie_a
	 */
	private int cantidad_acciones_serie_aModel;
	/*
	 * cantidad_acciones_serie_b
	 */
	private int cantidad_acciones_serie_bModel;
	/*
	 * porcentaje_participacion
	 */
	private double porcentaje_participacionModel;
	/*
	 * Fecha_nacimiento
	 */
	private String fecha_nacimientoModel;
	/*
	 *es_valido
	 */
	private String es_validoModel;
	/*
	 * mail
	 */
	private String EmailModel;
	/*
	 * direccion
	 */
	private String direccionModel;
	/*
	 * fono
	 */
	private  String fonoModel;
	/*
	 * ciudad
	 */
	private String ciudadModel;
	/*
	 * comuna
	 */
	private String comunaModel;
	/*
	 * nacionalidad
	 */
	private NacionalidadModel nacionalidadModel;
	/*
	 * tipoAccionista
	 */
	private TipoAccionistaModel tipoAccionistaModel;
	/*
	 *accionistarut
	 */
	private AccionistaModel accionistaModel;
	
	private RegionesModel regionesModel;
	
	/*
	 * estado
	 */
	private int estadoModel;

	public HerederoModel() {
		super();
	}

	public HerederoModel(int idHerederoModel, String rutModel, EmpresaModel empresaModel, String nombreModel,
			String apellido_paternoModel, String apellido_maternoModel, int cantidad_acciones_serie_aModel,
			int cantidad_acciones_serie_bModel, double porcentaje_participacionModel, String fecha_nacimientoModel,
			String es_validoModel, String emailModel, String direccionModel, String fonoModel, String ciudadModel,
			String comunaModel, NacionalidadModel nacionalidadModel, TipoAccionistaModel tipoAccionistaModel,
			AccionistaModel accionistaModel, RegionesModel regionesModel, int estadoModel) {
		super();
		this.idHerederoModel = idHerederoModel;
		this.rutModel = rutModel;
		this.empresaModel = empresaModel;
		this.nombreModel = nombreModel;
		this.apellido_paternoModel = apellido_paternoModel;
		this.apellido_maternoModel = apellido_maternoModel;
		this.cantidad_acciones_serie_aModel = cantidad_acciones_serie_aModel;
		this.cantidad_acciones_serie_bModel = cantidad_acciones_serie_bModel;
		this.porcentaje_participacionModel = porcentaje_participacionModel;
		this.fecha_nacimientoModel = fecha_nacimientoModel;
		this.es_validoModel = es_validoModel;
		EmailModel = emailModel;
		this.direccionModel = direccionModel;
		this.fonoModel = fonoModel;
		this.ciudadModel = ciudadModel;
		this.comunaModel = comunaModel;
		this.nacionalidadModel = nacionalidadModel;
		this.tipoAccionistaModel = tipoAccionistaModel;
		this.accionistaModel = accionistaModel;
		this.regionesModel = regionesModel;
		this.estadoModel = estadoModel;
	}

	@Override
	public String toString() {
		return "HerederoModel [idHerederoModel=" + idHerederoModel + ", rutModel=" + rutModel + ", empresaModel="
				+ empresaModel + ", nombreModel=" + nombreModel + ", apellido_paternoModel=" + apellido_paternoModel
				+ ", apellido_maternoModel=" + apellido_maternoModel + ", cantidad_acciones_serie_aModel="
				+ cantidad_acciones_serie_aModel + ", cantidad_acciones_serie_bModel=" + cantidad_acciones_serie_bModel
				+ ", porcentaje_participacionModel=" + porcentaje_participacionModel + ", fecha_nacimientoModel="
				+ fecha_nacimientoModel + ", es_validoModel=" + es_validoModel + ", EmailModel=" + EmailModel
				+ ", direccionModel=" + direccionModel + ", fonoModel=" + fonoModel + ", ciudadModel=" + ciudadModel
				+ ", comunaModel=" + comunaModel + ", nacionalidadModel=" + nacionalidadModel + ", tipoAccionistaModel="
				+ tipoAccionistaModel + ", accionistaModel=" + accionistaModel + ", regionesModel=" + regionesModel
				+ ", estadoModel=" + estadoModel + "]";
	}

	public int getIdHerederoModel() {
		return idHerederoModel;
	}

	public void setIdHerederoModel(int idHerederoModel) {
		this.idHerederoModel = idHerederoModel;
	}

	public String getRutModel() {
		return rutModel;
	}

	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}

	public EmpresaModel getEmpresaModel() {
		return empresaModel;
	}

	public void setEmpresaModel(EmpresaModel empresaModel) {
		this.empresaModel = empresaModel;
	}

	public String getNombreModel() {
		return nombreModel;
	}

	public void setNombreModel(String nombreModel) {
		this.nombreModel = nombreModel;
	}

	public String getApellido_paternoModel() {
		return apellido_paternoModel;
	}

	public void setApellido_paternoModel(String apellido_paternoModel) {
		this.apellido_paternoModel = apellido_paternoModel;
	}

	public String getApellido_maternoModel() {
		return apellido_maternoModel;
	}

	public void setApellido_maternoModel(String apellido_maternoModel) {
		this.apellido_maternoModel = apellido_maternoModel;
	}

	public int getCantidad_acciones_serie_aModel() {
		return cantidad_acciones_serie_aModel;
	}

	public void setCantidad_acciones_serie_aModel(int cantidad_acciones_serie_aModel) {
		this.cantidad_acciones_serie_aModel = cantidad_acciones_serie_aModel;
	}

	public int getCantidad_acciones_serie_bModel() {
		return cantidad_acciones_serie_bModel;
	}

	public void setCantidad_acciones_serie_bModel(int cantidad_acciones_serie_bModel) {
		this.cantidad_acciones_serie_bModel = cantidad_acciones_serie_bModel;
	}

	public double getPorcentaje_participacionModel() {
		return porcentaje_participacionModel;
	}

	public void setPorcentaje_participacionModel(double porcentaje_participacionModel) {
		this.porcentaje_participacionModel = porcentaje_participacionModel;
	}

	public String getFecha_nacimientoModel() {
		return fecha_nacimientoModel;
	}

	public void setFecha_nacimientoModel(String fecha_nacimientoModel) {
		this.fecha_nacimientoModel = fecha_nacimientoModel;
	}

	public String getEs_validoModel() {
		return es_validoModel;
	}

	public void setEs_validoModel(String es_validoModel) {
		this.es_validoModel = es_validoModel;
	}

	public String getEmailModel() {
		return EmailModel;
	}

	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}

	public String getDireccionModel() {
		return direccionModel;
	}

	public void setDireccionModel(String direccionModel) {
		this.direccionModel = direccionModel;
	}

	public String getFonoModel() {
		return fonoModel;
	}

	public void setFonoModel(String fonoModel) {
		this.fonoModel = fonoModel;
	}

	public String getCiudadModel() {
		return ciudadModel;
	}

	public void setCiudadModel(String ciudadModel) {
		this.ciudadModel = ciudadModel;
	}

	public String getComunaModel() {
		return comunaModel;
	}

	public void setComunaModel(String comunaModel) {
		this.comunaModel = comunaModel;
	}

	public NacionalidadModel getNacionalidadModel() {
		return nacionalidadModel;
	}

	public void setNacionalidadModel(NacionalidadModel nacionalidadModel) {
		this.nacionalidadModel = nacionalidadModel;
	}

	public TipoAccionistaModel getTipoAccionistaModel() {
		return tipoAccionistaModel;
	}

	public void setTipoAccionistaModel(TipoAccionistaModel tipoAccionistaModel) {
		this.tipoAccionistaModel = tipoAccionistaModel;
	}

	public AccionistaModel getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(AccionistaModel accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	public RegionesModel getRegionesModel() {
		return regionesModel;
	}

	public void setRegionesModel(RegionesModel regionesModel) {
		this.regionesModel = regionesModel;
	}

	public int getEstadoModel() {
		return estadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		this.estadoModel = estadoModel;
	}

	
}

