package com.accionistas.accionsis.model;

import java.sql.Date;

import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.entity.TipoActa;

public class ActaModel {
	
	private int idActa;
	
	private JuntaAccionista juntaAccionista;
	
	private String detalleActa;
	
	private String empresaAccionesA;
	
	private String empresaAccionesB;
	
	private String accionistasAccionesA;
	
	private String accionistasAccionesB;
	
	private Date fechaApertura;
	
	private Date fechaTermino;
	
	private String documento;
	
	private String url;
	
	private TipoActa tipoActa;

	public ActaModel() {
		super();
	}

	public ActaModel(int idActa, JuntaAccionista juntaAccionista, String detalleActa, String empresaAccionesA,
			String empresaAccionesB, String accionistasAccionesA, String accionistasAccionesB, Date fechaApertura,
			Date fechaTermino, String documento, String url, TipoActa tipoActa) {
		super();
		this.idActa = idActa;
		this.juntaAccionista = juntaAccionista;
		this.detalleActa = detalleActa;
		this.empresaAccionesA = empresaAccionesA;
		this.empresaAccionesB = empresaAccionesB;
		this.accionistasAccionesA = accionistasAccionesA;
		this.accionistasAccionesB = accionistasAccionesB;
		this.fechaApertura = fechaApertura;
		this.fechaTermino = fechaTermino;
		this.documento = documento;
		this.url = url;
		this.tipoActa = tipoActa;
	}

	@Override
	public String toString() {
		return "ActaModel [idActa=" + idActa + ", juntaAccionista=" + juntaAccionista + ", detalleActa=" + detalleActa
				+ ", empresaAccionesA=" + empresaAccionesA + ", empresaAccionesB=" + empresaAccionesB
				+ ", accionistasAccionesA=" + accionistasAccionesA + ", accionistasAccionesB=" + accionistasAccionesB
				+ ", fechaApertura=" + fechaApertura + ", fechaTermino=" + fechaTermino + ", documento=" + documento
				+ ", url=" + url + ", tipoActa=" + tipoActa + "]";
	}

	public int getIdActa() {
		return idActa;
	}

	public void setIdActa(int idActa) {
		this.idActa = idActa;
	}

	public JuntaAccionista getJuntaAccionista() {
		return juntaAccionista;
	}

	public void setJuntaAccionista(JuntaAccionista juntaAccionista) {
		this.juntaAccionista = juntaAccionista;
	}

	public String getDetalleActa() {
		return detalleActa;
	}

	public void setDetalleActa(String detalleActa) {
		this.detalleActa = detalleActa;
	}

	public String getEmpresaAccionesA() {
		return empresaAccionesA;
	}

	public void setEmpresaAccionesA(String empresaAccionesA) {
		this.empresaAccionesA = empresaAccionesA;
	}

	public String getEmpresaAccionesB() {
		return empresaAccionesB;
	}

	public void setEmpresaAccionesB(String empresaAccionesB) {
		this.empresaAccionesB = empresaAccionesB;
	}

	public String getAccionistasAccionesA() {
		return accionistasAccionesA;
	}

	public void setAccionistasAccionesA(String accionistasAccionesA) {
		this.accionistasAccionesA = accionistasAccionesA;
	}

	public String getAccionistasAccionesB() {
		return accionistasAccionesB;
	}

	public void setAccionistasAccionesB(String accionistasAccionesB) {
		this.accionistasAccionesB = accionistasAccionesB;
	}

	public Date getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public Date getFechaTermino() {
		return fechaTermino;
	}

	public void setFechaTermino(Date fechaTermino) {
		this.fechaTermino = fechaTermino;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public TipoActa getTipoActa() {
		return tipoActa;
	}

	public void setTipoActa(TipoActa tipoActa) {
		this.tipoActa = tipoActa;
	}

	
}
