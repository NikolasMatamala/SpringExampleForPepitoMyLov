package com.accionistas.accionsis.model;

import java.sql.Date;

/**
 * Class AccionistaModel.
 */
public class AccionistaModel {
	
	/** id. */
	private int idAccionistaModel;
	
	/** rut. */
	private String rutModel;
	
	/** empresaid empresa. */
	private EmpresaModel empresaModel;
	
	/** nombre. */
	private String nombreModel;
	
	/** apellido paterno. */
	private String apellido_paternoModel;
	
	/** apellido materno. */
	private String apellido_maternoModel;
	
	/** cantidad acciones serie a. */
	private int cantidad_acciones_serie_aModel;
	
	/** cantidad acciones serie b. */
	private int cantidad_acciones_serie_bModel;
	
	/** porcentaje participacion. */
	private double porcentaje_participacionModel;
	
	/** fecha nacimiento. */
	private Date fecha_nacimientoModel;
	
	/** fecha ingreso. */
	private Date fecha_ingresoModel;
	
	/** es valido. */
	private String esValidoModel;
	
	/** mail. */
	private String EmailModel;
	
	/** direccion. */
	private String direccionModel;
	
	/** fono. */
	private String fonoModel;
	
	/** nacionalidadid. */
	private NacionalidadModel nacionalidadModel;
	
	/** tipo accionistaid. */
	private TipoAccionistaModel tipoAccionistaModel;
	
	/** ciudad. */
	private String ciudadModel;
	
	/** comuna. */
	private String comunaModel;
	
	private RegionesModel regionModel;
	
	private ApoderadoModel apoderadoModel;
	
	/** estado */
	private int estadoModel;

	public AccionistaModel() {
		super();
	}

	public AccionistaModel(int idAccionistaModel, String rutModel, EmpresaModel empresaModel, String nombreModel,
			String apellido_paternoModel, String apellido_maternoModel, int cantidad_acciones_serie_aModel,
			int cantidad_acciones_serie_bModel, double porcentaje_participacionModel, Date fecha_nacimientoModel,
			Date fecha_ingresoModel, String esValidoModel, String emailModel, String direccionModel, String fonoModel,
			NacionalidadModel nacionalidadModel, TipoAccionistaModel tipoAccionistaModel, String ciudadModel,
			String comunaModel, RegionesModel regionModel, ApoderadoModel apoderadoModel, int estadoModel) {
		super();
		this.idAccionistaModel = idAccionistaModel;
		this.rutModel = rutModel;
		this.empresaModel = empresaModel;
		this.nombreModel = nombreModel;
		this.apellido_paternoModel = apellido_paternoModel;
		this.apellido_maternoModel = apellido_maternoModel;
		this.cantidad_acciones_serie_aModel = cantidad_acciones_serie_aModel;
		this.cantidad_acciones_serie_bModel = cantidad_acciones_serie_bModel;
		this.porcentaje_participacionModel = porcentaje_participacionModel;
		this.fecha_nacimientoModel = fecha_nacimientoModel;
		this.fecha_ingresoModel = fecha_ingresoModel;
		this.esValidoModel = esValidoModel;
		EmailModel = emailModel;
		this.direccionModel = direccionModel;
		this.fonoModel = fonoModel;
		this.nacionalidadModel = nacionalidadModel;
		this.tipoAccionistaModel = tipoAccionistaModel;
		this.ciudadModel = ciudadModel;
		this.comunaModel = comunaModel;
		this.regionModel = regionModel;
		this.apoderadoModel = apoderadoModel;
		this.estadoModel = estadoModel;
	}

	@Override
	public String toString() {
		return "AccionistaModel [idAccionistaModel=" + idAccionistaModel + ", rutModel=" + rutModel + ", empresaModel="
				+ empresaModel + ", nombreModel=" + nombreModel + ", apellido_paternoModel=" + apellido_paternoModel
				+ ", apellido_maternoModel=" + apellido_maternoModel + ", cantidad_acciones_serie_aModel="
				+ cantidad_acciones_serie_aModel + ", cantidad_acciones_serie_bModel=" + cantidad_acciones_serie_bModel
				+ ", porcentaje_participacionModel=" + porcentaje_participacionModel + ", fecha_nacimientoModel="
				+ fecha_nacimientoModel + ", fecha_ingresoModel=" + fecha_ingresoModel + ", esValidoModel="
				+ esValidoModel + ", EmailModel=" + EmailModel + ", direccionModel=" + direccionModel + ", fonoModel="
				+ fonoModel + ", nacionalidadModel=" + nacionalidadModel + ", tipoAccionistaModel="
				+ tipoAccionistaModel + ", ciudadModel=" + ciudadModel + ", comunaModel=" + comunaModel
				+ ", regionModel=" + regionModel + ", apoderadoModel=" + apoderadoModel + ", estadoModel=" + estadoModel
				+ "]";
	}

	public int getIdAccionistaModel() {
		return idAccionistaModel;
	}

	public void setIdAccionistaModel(int idAccionistaModel) {
		this.idAccionistaModel = idAccionistaModel;
	}

	public String getRutModel() {
		return rutModel;
	}

	public void setRutModel(String rutModel) {
		this.rutModel = rutModel;
	}

	public EmpresaModel getEmpresaModel() {
		return empresaModel;
	}

	public void setEmpresaModel(EmpresaModel empresaModel) {
		this.empresaModel = empresaModel;
	}

	public String getNombreModel() {
		return nombreModel;
	}

	public void setNombreModel(String nombreModel) {
		this.nombreModel = nombreModel;
	}

	public String getApellido_paternoModel() {
		return apellido_paternoModel;
	}

	public void setApellido_paternoModel(String apellido_paternoModel) {
		this.apellido_paternoModel = apellido_paternoModel;
	}

	public String getApellido_maternoModel() {
		return apellido_maternoModel;
	}

	public void setApellido_maternoModel(String apellido_maternoModel) {
		this.apellido_maternoModel = apellido_maternoModel;
	}

	public int getCantidad_acciones_serie_aModel() {
		return cantidad_acciones_serie_aModel;
	}

	public void setCantidad_acciones_serie_aModel(int cantidad_acciones_serie_aModel) {
		this.cantidad_acciones_serie_aModel = cantidad_acciones_serie_aModel;
	}

	public int getCantidad_acciones_serie_bModel() {
		return cantidad_acciones_serie_bModel;
	}

	public void setCantidad_acciones_serie_bModel(int cantidad_acciones_serie_bModel) {
		this.cantidad_acciones_serie_bModel = cantidad_acciones_serie_bModel;
	}

	public double getPorcentaje_participacionModel() {
		return porcentaje_participacionModel;
	}

	public void setPorcentaje_participacionModel(double porcentaje_participacionModel) {
		this.porcentaje_participacionModel = porcentaje_participacionModel;
	}

	public Date getFecha_nacimientoModel() {
		return fecha_nacimientoModel;
	}

	public void setFecha_nacimientoModel(Date fecha_nacimientoModel) {
		this.fecha_nacimientoModel = fecha_nacimientoModel;
	}

	public Date getFecha_ingresoModel() {
		return fecha_ingresoModel;
	}

	public void setFecha_ingresoModel(Date fecha_ingresoModel) {
		this.fecha_ingresoModel = fecha_ingresoModel;
	}

	public String getEsValidoModel() {
		return esValidoModel;
	}

	public void setEsValidoModel(String esValidoModel) {
		this.esValidoModel = esValidoModel;
	}

	public String getEmailModel() {
		return EmailModel;
	}

	public void setEmailModel(String emailModel) {
		EmailModel = emailModel;
	}

	public String getDireccionModel() {
		return direccionModel;
	}

	public void setDireccionModel(String direccionModel) {
		this.direccionModel = direccionModel;
	}

	public String getFonoModel() {
		return fonoModel;
	}

	public void setFonoModel(String fonoModel) {
		this.fonoModel = fonoModel;
	}

	public NacionalidadModel getNacionalidadModel() {
		return nacionalidadModel;
	}

	public void setNacionalidadModel(NacionalidadModel nacionalidadModel) {
		this.nacionalidadModel = nacionalidadModel;
	}

	public TipoAccionistaModel getTipoAccionistaModel() {
		return tipoAccionistaModel;
	}

	public void setTipoAccionistaModel(TipoAccionistaModel tipoAccionistaModel) {
		this.tipoAccionistaModel = tipoAccionistaModel;
	}

	public String getCiudadModel() {
		return ciudadModel;
	}

	public void setCiudadModel(String ciudadModel) {
		this.ciudadModel = ciudadModel;
	}

	public String getComunaModel() {
		return comunaModel;
	}

	public void setComunaModel(String comunaModel) {
		this.comunaModel = comunaModel;
	}

	public RegionesModel getRegionModel() {
		return regionModel;
	}

	public void setRegionModel(RegionesModel regionModel) {
		this.regionModel = regionModel;
	}

	public ApoderadoModel getApoderadoModel() {
		return apoderadoModel;
	}

	public void setApoderadoModel(ApoderadoModel apoderadoModel) {
		this.apoderadoModel = apoderadoModel;
	}

	public int getEstadoModel() {
		return estadoModel;
	}

	public void setEstadoModel(int estadoModel) {
		this.estadoModel = estadoModel;
	}

	
}
