package com.accionistas.accionsis.model;


public class AsistenciaJuntaAccionistaModel {
	
	/*idAsistencia*/
	private int idAsistenciaModel;
	
	/*idJunta*/
	private JuntaAccionistaModel juntaAccionistaModel;
	
	/*rutAccionista*/
	private AccionistaModel accionistaModel;
	
	/*idApoderado*/
	private ApoderadoModel apoderadoModel;
	
	/*idMandato*/
	private MandatoModel mandatoModel;
	
	/*estadoAsistenciaJunta*/
	private int estadoAsistenciaJuntaModel;

	public AsistenciaJuntaAccionistaModel() {
		super();
	}

	public AsistenciaJuntaAccionistaModel(int idAsistenciaModel, JuntaAccionistaModel juntaAccionistaModel,
			AccionistaModel accionistaModel, ApoderadoModel apoderadoModel, MandatoModel mandatoModel,
			int estadoAsistenciaJuntaModel) {
		super();
		this.idAsistenciaModel = idAsistenciaModel;
		this.juntaAccionistaModel = juntaAccionistaModel;
		this.accionistaModel = accionistaModel;
		this.apoderadoModel = apoderadoModel;
		this.mandatoModel = mandatoModel;
		this.estadoAsistenciaJuntaModel = estadoAsistenciaJuntaModel;
	}

	@Override
	public String toString() {
		return "AsistenciaJuntaAccionistaModel [idAsistenciaModel=" + idAsistenciaModel + ", juntaAccionistaModel="
				+ juntaAccionistaModel + ", accionistaModel=" + accionistaModel + ", apoderadoModel=" + apoderadoModel
				+ ", mandatoModel=" + mandatoModel + ", estadoAsistenciaJuntaModel=" + estadoAsistenciaJuntaModel + "]";
	}

	public int getIdAsistenciaModel() {
		return idAsistenciaModel;
	}

	public void setIdAsistenciaModel(int idAsistenciaModel) {
		this.idAsistenciaModel = idAsistenciaModel;
	}

	public JuntaAccionistaModel getJuntaAccionistaModel() {
		return juntaAccionistaModel;
	}

	public void setJuntaAccionistaModel(JuntaAccionistaModel juntaAccionistaModel) {
		this.juntaAccionistaModel = juntaAccionistaModel;
	}

	public AccionistaModel getAccionistaModel() {
		return accionistaModel;
	}

	public void setAccionistaModel(AccionistaModel accionistaModel) {
		this.accionistaModel = accionistaModel;
	}

	public ApoderadoModel getApoderadoModel() {
		return apoderadoModel;
	}

	public void setApoderadoModel(ApoderadoModel apoderadoModel) {
		this.apoderadoModel = apoderadoModel;
	}

	public MandatoModel getMandatoModel() {
		return mandatoModel;
	}

	public void setMandatoModel(MandatoModel mandatoModel) {
		this.mandatoModel = mandatoModel;
	}

	public int getEstadoAsistenciaJuntaModel() {
		return estadoAsistenciaJuntaModel;
	}

	public void setEstadoAsistenciaJuntaModel(int estadoAsistenciaJuntaModel) {
		this.estadoAsistenciaJuntaModel = estadoAsistenciaJuntaModel;
	}

	
}
