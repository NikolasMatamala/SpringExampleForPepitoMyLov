package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Heredero;

@Repository("herederoJpaRepository")
public interface HerederoJpaRepository extends JpaRepository<Heredero, Serializable>{

	/*
	 * buscar heredero por rut
	 */
	public abstract Heredero findByRut(String Rut);
	public abstract Heredero findByidHeredero(int id);
}
