package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Regiones;

@Repository("regionesJpaRepository")
public interface RegionesJpaRepository extends JpaRepository<Regiones, Serializable> {
	
	public abstract List<Regiones> findAll();
	public abstract Regiones findByidRegion(int id);

}
