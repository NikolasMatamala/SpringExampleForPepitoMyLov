package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.User;

/**
 * Interface UserJpaRepository.
 */
@Repository("userJpaRepository")
public interface UserJpaRepository extends JpaRepository<User, Serializable>{
	
	/**
	 * Encontrar usuario a travez de username.
	 * @param username 
	 * @return user
	 */
	public abstract User findByUsername(String username);

}
