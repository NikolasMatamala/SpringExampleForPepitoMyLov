package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Testigo;

/**
 * Interface TestigoJpaRepository.
 */
@Repository("testigoJpaRepository")
public interface TestigoJpaRepository extends JpaRepository<Testigo, Serializable> {

	/**
	 * Encontrar testigo por rut.
	 * @param rut
	 * @return testigo
	 */
	public abstract Testigo findByRut(String rut);
	
	public abstract Testigo findByidTestigo(int id);
	
}
