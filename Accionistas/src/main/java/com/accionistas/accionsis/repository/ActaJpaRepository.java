package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Acta;

@Repository("actaJpaRepository")
public interface ActaJpaRepository  extends JpaRepository<Acta, Serializable>{
	
	public abstract Acta findByidActa(int id);

}
