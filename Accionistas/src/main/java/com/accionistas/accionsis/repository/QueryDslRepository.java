package com.accionistas.accionsis.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.QAccionista;
import com.querydsl.jpa.impl.JPAQuery;

@Repository("queryDslRepository")
public class QueryDslRepository {
	
	private QAccionista qAccionista = QAccionista.accionista;
	
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;
	
	//Gestiona la persistencia de la aplicacion
	@PersistenceContext	
	private EntityManager em;
	
	public List<Accionista> findTop10AccionistasSerieA() {
		
		JPAQuery<Accionista> queryAccionistasTop = new JPAQuery<Accionista>(em);
		
		List<Accionista> accionistasTop10 = 
							queryAccionistasTop.select(qAccionista)
							.from(qAccionista)						
							.orderBy(qAccionista.cantidad_acciones_serie_a.desc())
							.limit(2)
							.fetch();

		return accionistasTop10;
	}
//	
//	
//	
//	private QAsistenciaJuntaAccionista qAsistencia = QAsistenciaJuntaAccionista.asistenciaJuntaq;
//	private QApoderado qApoderado = QApoderado.apoderado;
//	
//	@Autowired
//	@Qualifier("apoderadoConverter")
//	private ApoderadoConverter apoderadoConverter;
//	
//	@Autowired
//	@Qualifier("AsistenciaJuntaAccionistaConverter")
//	private AsistenciaJuntaAccionistaConverter asistenciaJuntaAccionistaConverter;
//	
//	public List<Object> findAllAsistenciaJunta(){
//		
//		JPAQuery<Object> q = new JPAQuery<Object>(em);
//		
//		
//		List<Tuple> lista = q.select(qAsistencia.estadoAsistenciaJunta, qAccionista.nombre, qApoderado.nombre)
//			.from(qAsistencia)
//			.leftJoin(qAccionista).on(qAsistencia.rutAccionista.eq(qAccionista.rut))		
//			.leftJoin(qApoderado).on(qAsistencia.idApoderado.eq(qApoderado.id)).fetch();
//		
//		
//
//		
//		
//		LOG.info(lista.toString());
//		
//		
//		return null;
//	}
	
	
	
	
}

