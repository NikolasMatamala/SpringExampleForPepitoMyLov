package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accionistas.accionsis.entity.Representante;

public interface RepresentanteJpaRepository extends JpaRepository<Representante, Serializable> {

	public abstract Representante findByRut(String rut);
	
	public abstract Representante findByIdRepresentante(int id);
	
	public abstract Representante findByidRepresentante(int id);
}
