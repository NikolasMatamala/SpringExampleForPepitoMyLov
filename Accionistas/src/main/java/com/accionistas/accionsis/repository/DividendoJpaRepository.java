package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Dividendo;

/*Interface DividendoJpaRepository*/
@Repository("DividendoJpaRepository")
public interface DividendoJpaRepository extends JpaRepository<Dividendo, Serializable>{
	
	/*
	 * Buscar dividendo por rut.
	 * @param rut
	 * @return dividendo
	 */
	
	public abstract Dividendo findByAccionista (Set<Accionista> accionista);
}
