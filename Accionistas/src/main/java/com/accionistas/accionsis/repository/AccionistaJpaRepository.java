package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Accionista;

/**
 * Interface AccionistaJpaRepository.
 */
@Repository("accionistaJpaRepository")
public interface AccionistaJpaRepository extends JpaRepository<Accionista, Serializable> {
	
	/**
	 * Encontrar accionista por rut.
	 * @param rut
	 * @return accionista
	 */
	public abstract Accionista findByRut(String rut);
	
	public abstract Accionista findByidAccionista(int id);
	
	@Query(nativeQuery=true)
	List<Accionista> findTodos();
	
	@Query(nativeQuery=true)
	List<Accionista> findTodosB();
	
	@Query(nativeQuery=true)
	List<Accionista> findTodosAB();

	
}
