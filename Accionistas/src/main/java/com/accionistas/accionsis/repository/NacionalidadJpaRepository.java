package com.accionistas.accionsis.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Nacionalidad;

@Repository("nacionalidadJpaRepository")
public interface NacionalidadJpaRepository extends JpaRepository<Nacionalidad, Serializable>{
	
	/**
	 * Encontrar nacionalidad por id.
	 * @param id
	 * @return nacionalidad
	 */
	public abstract Nacionalidad findByIdNacionalidad(int id);
}
