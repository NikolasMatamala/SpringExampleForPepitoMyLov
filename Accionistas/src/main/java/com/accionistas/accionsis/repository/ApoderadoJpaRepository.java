package com.accionistas.accionsis.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Apoderado;

/**
 * Interface ApoderadoJpaRepository.
 */
@Repository("apoderadoJpaRepository")
public interface ApoderadoJpaRepository extends JpaRepository<Apoderado, Serializable> {

	/**
	 * Encontrar apoderado por id
	 * @param id 
	 * @return the apoderado
	 */
	public abstract Apoderado findByIdApoderado(int idApoderado);
	
	public abstract Apoderado findByRut(String rut);
	
	Apoderado findByAccionista(Accionista accionista);
	
	Apoderado findByTipoApoderado(int tipoApoderado);
	
	List<Apoderado> findAllByTipoApoderado(int tipo);
}
