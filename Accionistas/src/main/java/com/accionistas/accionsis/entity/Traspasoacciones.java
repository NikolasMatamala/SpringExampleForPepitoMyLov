package com.accionistas.accionsis.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Traspasoacciones")
public class Traspasoacciones {
	
	@Id
	@GeneratedValue
	@Column(name = "idTraspasoAcciones")
	private int ididTraspasoAcciones;
	
	@ManyToOne
	private Accionista accionistaVendedor;
	
	@ManyToOne
	private Accionista accionistaComprador;
	
	@Column(name = "fecha_venta")
	private Date fecha_venta;
	
	@Column(name = "cantidad_acciones_vendidas_a")
	private int cantidad_acciones_vendidas_a;
	
	@Column(name = "cantidad_acciones_vendidas_b")
	private int cantidad_acciones_vendidas_b;
	
	@Column (name = "monto")
	private double monto;
	
	@Column (name = "es_sucesion")
	private String es_sucesion;
	
	@ManyToOne
	private Testigo testigo;
	
	@Column (name = "observacion")
	private String observacion;

	public Traspasoacciones() {
		super();
	}

	public Traspasoacciones(int ididTraspasoAcciones, Accionista accionistaVendedor, Accionista accionistaComprador,
			Date fecha_venta, int cantidad_acciones_vendidas_a, int cantidad_acciones_vendidas_b, double monto,
			String es_sucesion, Testigo testigo, String observacion) {
		super();
		this.ididTraspasoAcciones = ididTraspasoAcciones;
		this.accionistaVendedor = accionistaVendedor;
		this.accionistaComprador = accionistaComprador;
		this.fecha_venta = fecha_venta;
		this.cantidad_acciones_vendidas_a = cantidad_acciones_vendidas_a;
		this.cantidad_acciones_vendidas_b = cantidad_acciones_vendidas_b;
		this.monto = monto;
		this.es_sucesion = es_sucesion;
		this.testigo = testigo;
		this.observacion = observacion;
	}

	@Override
	public String toString() {
		return "Traspasoacciones [ididTraspasoAcciones=" + ididTraspasoAcciones + ", accionistaVendedor="
				+ accionistaVendedor + ", accionistaComprador=" + accionistaComprador + ", fecha_venta=" + fecha_venta
				+ ", cantidad_acciones_vendidas_a=" + cantidad_acciones_vendidas_a + ", cantidad_acciones_vendidas_b="
				+ cantidad_acciones_vendidas_b + ", monto=" + monto + ", es_sucesion=" + es_sucesion + ", testigo="
				+ testigo + ", observacion=" + observacion + "]";
	}

	public int getIdidTraspasoAcciones() {
		return ididTraspasoAcciones;
	}

	public void setIdidTraspasoAcciones(int ididTraspasoAcciones) {
		this.ididTraspasoAcciones = ididTraspasoAcciones;
	}

	public Accionista getAccionistaVendedor() {
		return accionistaVendedor;
	}

	public void setAccionistaVendedor(Accionista accionistaVendedor) {
		this.accionistaVendedor = accionistaVendedor;
	}

	public Accionista getAccionistaComprador() {
		return accionistaComprador;
	}

	public void setAccionistaComprador(Accionista accionistaComprador) {
		this.accionistaComprador = accionistaComprador;
	}

	public Date getFecha_venta() {
		return fecha_venta;
	}

	public void setFecha_venta(Date fecha_venta) {
		this.fecha_venta = fecha_venta;
	}

	public int getCantidad_acciones_vendidas_a() {
		return cantidad_acciones_vendidas_a;
	}

	public void setCantidad_acciones_vendidas_a(int cantidad_acciones_vendidas_a) {
		this.cantidad_acciones_vendidas_a = cantidad_acciones_vendidas_a;
	}

	public int getCantidad_acciones_vendidas_b() {
		return cantidad_acciones_vendidas_b;
	}

	public void setCantidad_acciones_vendidas_b(int cantidad_acciones_vendidas_b) {
		this.cantidad_acciones_vendidas_b = cantidad_acciones_vendidas_b;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getEs_sucesion() {
		return es_sucesion;
	}

	public void setEs_sucesion(String es_sucesion) {
		this.es_sucesion = es_sucesion;
	}

	public Testigo getTestigo() {
		return testigo;
	}

	public void setTestigo(Testigo testigo) {
		this.testigo = testigo;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	
}
