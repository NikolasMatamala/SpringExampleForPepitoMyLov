package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="AsistenciaJuntaAccionista")
public class AsistenciaJuntaAccionista {
	
	/*
	 * idAsistencia
	 */
	@Id
	@GeneratedValue
	@Column(name="idAsistencia")
	private int idAsistencia;
	
	/*
	 * idJunta
	 */
	@ManyToOne
	private JuntaAccionista juntaAccionista;
	
	/*
	 * rutAccionista
	 */
	@ManyToOne
	private Accionista accionista;
	
	/*
	 * idApoderado
	 */
	@ManyToOne
	private Apoderado apoderado;
	
	/*
	 * idMandato
	 */
	@ManyToOne
	private Mandato mandato;
	
	/*
	 * asistenciaJunta
	 */
	@Column(name="estadoAsistenciaJunta")
	private int estadoAsistenciaJunta;

	public AsistenciaJuntaAccionista() {
		super();
	}

	public AsistenciaJuntaAccionista(int idAsistencia, JuntaAccionista juntaAccionista, Accionista accionista,
			Apoderado apoderado, int estadoAsistenciaJunta) {
		super();
		this.idAsistencia = idAsistencia;
		this.juntaAccionista = juntaAccionista;
		this.accionista = accionista;
		this.apoderado = apoderado;
		this.estadoAsistenciaJunta = estadoAsistenciaJunta;
	}

	@Override
	public String toString() {
		return "AsistenciaJuntaAccionista [idAsistencia=" + idAsistencia + ", juntaAccionista=" + juntaAccionista
				+ ", accionista=" + accionista + ", apoderado=" + apoderado + ", estadoAsistenciaJunta="
				+ estadoAsistenciaJunta + "]";
	}

	public int getIdAsistencia() {
		return idAsistencia;
	}

	public void setIdAsistencia(int idAsistencia) {
		this.idAsistencia = idAsistencia;
	}

	public JuntaAccionista getJuntaAccionista() {
		return juntaAccionista;
	}

	public void setJuntaAccionista(JuntaAccionista juntaAccionista) {
		this.juntaAccionista = juntaAccionista;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	public Apoderado getApoderado() {
		return apoderado;
	}

	public void setApoderado(Apoderado apoderado) {
		this.apoderado = apoderado;
	}

	public int getEstadoAsistenciaJunta() {
		return estadoAsistenciaJunta;
	}

	public void setEstadoAsistenciaJunta(int estadoAsistenciaJunta) {
		this.estadoAsistenciaJunta = estadoAsistenciaJunta;
	}

	
}
