package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="RepresentanteLegal")
public class Representante {	
	
	@OneToMany
	(mappedBy = "representante")
	private Set<Empresa> empresa = new HashSet<>();

	/*
	 * id
	 */
	@Id
	@GeneratedValue
	@Column(name="idRepresentante")
	private int idRepresentante;
	
	/*rut*/
	@Column(name="rut")
	private String rut;
	/*
	 * nombre
	 */
	@Column(name="nombre")
	private String nombre;
	/*
	 * apellido_paterno
	 */
	@Column(name="apellido_paterno")
	private String apellido_paterno;
	/*
	 * apellido_materno
	 */
	@Column(name="apellido_materno")
	private String apellido_materno;
	/*
	 * direccion
	 */
	@Column(name="direccion")
	private String direccion;
	/*
	 * mail
	 */
	@Column(name="Email")
	private String Email;
	/*
	 * telefono
	 */
	@Column(name="telefono")
	private String telefono;
	
	@Column(name = "estado")
	private int estado;

	@ManyToOne
	private Regiones region;

	public Representante() {
		super();
	}

	public Representante(Set<Empresa> empresa, int idRepresentante, String rut, String nombre, String apellido_paterno,
			String apellido_materno, String direccion, String email, String telefono, int estado, Regiones region) {
		super();
		this.empresa = empresa;
		this.idRepresentante = idRepresentante;
		this.rut = rut;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		this.direccion = direccion;
		Email = email;
		this.telefono = telefono;
		this.estado = estado;
		this.region = region;
	}

	@Override
	public String toString() {
		return "Representante [empresa=" + empresa + ", idRepresentante=" + idRepresentante + ", rut=" + rut
				+ ", nombre=" + nombre + ", apellido_paterno=" + apellido_paterno + ", apellido_materno="
				+ apellido_materno + ", direccion=" + direccion + ", Email=" + Email + ", telefono=" + telefono
				+ ", estado=" + estado + ", region=" + region + "]";
	}

	public Set<Empresa> getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Set<Empresa> empresa) {
		this.empresa = empresa;
	}

	public int getIdRepresentante() {
		return idRepresentante;
	}

	public void setIdRepresentante(int idRepresentante) {
		this.idRepresentante = idRepresentante;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Regiones getRegion() {
		return region;
	}

	public void setRegion(Regiones region) {
		this.region = region;
	}
	
	
		
}
