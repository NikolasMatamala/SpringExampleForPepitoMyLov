package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CuentaCorriente")
public class CuentaCorriente {
	
	
	@Id	
	@GeneratedValue
	@Column(name="idCuentaCorriente")
	private int idCuentaCorriente;
	
	@Column(name = "numeroCuenta")
	private int numeroCuenta;
	
	@Column(name = "nombreBanco")
	private String nombreBanco;
	
	@ManyToOne
	private Accionista accionista;

	public CuentaCorriente() {
		super();
	}

	public CuentaCorriente(int idCuentaCorriente, int numeroCuenta, String nombreBanco, Accionista accionista) {
		super();
		this.idCuentaCorriente = idCuentaCorriente;
		this.numeroCuenta = numeroCuenta;
		this.nombreBanco = nombreBanco;
		this.accionista = accionista;
	}

	@Override
	public String toString() {
		return "CuentaCorriente [idCuentaCorriente=" + idCuentaCorriente + ", numeroCuenta=" + numeroCuenta
				+ ", nombreBanco=" + nombreBanco + ", accionista=" + accionista + "]";
	}

	public int getIdCuentaCorriente() {
		return idCuentaCorriente;
	}

	public void setIdCuentaCorriente(int idCuentaCorriente) {
		this.idCuentaCorriente = idCuentaCorriente;
	}

	public int getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(int numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getNombreBanco() {
		return nombreBanco;
	}

	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	
}
