package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TipoActa")
public class TipoActa {
	
	@OneToMany
	(mappedBy = "tipoActa")
	private Set<Acta> acta = new HashSet<>();

	@Id
	@GeneratedValue
	@Column(name = "idTipoActa")
	private int idTipoActa;
	
	@Column(name = "descripcion")
	private String descripcion;

	public TipoActa() {
		super();
	}

	public TipoActa(Set<Acta> acta, int idTipoActa, String descripcion) {
		super();
		this.acta = acta;
		this.idTipoActa = idTipoActa;
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "TipoActa [acta=" + acta + ", idTipoActa=" + idTipoActa + ", descripcion=" + descripcion + "]";
	}

	public Set<Acta> getActa() {
		return acta;
	}

	public void setActa(Set<Acta> acta) {
		this.acta = acta;
	}

	public int getIdTipoActa() {
		return idTipoActa;
	}

	public void setIdTipoActa(int idTipoActa) {
		this.idTipoActa = idTipoActa;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
