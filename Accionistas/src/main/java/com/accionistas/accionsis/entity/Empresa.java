package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Clase Empresa.
 */
@Entity
@Table(name = "Empresa")
public class Empresa {
	
	@OneToMany
	(mappedBy = "empresa")
	private Set<Accionista> accionista = new HashSet<>();
	
	@OneToMany
	(mappedBy = "empresa")
	private Set<Heredero> heredero = new HashSet<>();
	
	/** id. */
	@Id
	@GeneratedValue
	@Column(name="idEmpresa")
	private int idEmpresa;
	
	/** rut. */
	@Column(name="rut")
	private String rut;
	
	/** razon social. */
	@Column(name="razonSocial")
	private String razonSocial;
	
	/** logo. */
	@Column(name="logo")
	private String logo;
	
	/** web. */
	@Column(name="web")
	private String web;
	
	/** cantidad acciones. */
	@Column(name="cantidadAccionesSerieA")
	private int cantidadAccionesSerieA;
	
	@Column(name="cantidadAccionesSerieB")
	private int cantidadAccionesSerieB;
	
	/** cantidad accionistas. */
	@Column(name="cantidadAccionistas")
	private int cantidadAccionistas;
	
	/** mail. */
	@Column(name="Email")
	private String Email;
	
	/** direccion. */
	@Column(name="direccion")
	private String direccion;
	
	/** telefono. */
	@Column(name="telefono")
	private String telefono;
	
	/** representante legalrut. */
	@ManyToOne
	private Representante representante;
	
	/** tipo giro comercialid. */
	@ManyToOne
	private GiroComercial giroComercial;
	
	@ManyToOne
	private Accion accion;

	public Empresa() {
		super();
	}

	public Empresa(Set<Accionista> accionista, Set<Heredero> heredero, int idEmpresa, String rut, String razonSocial,
			String logo, String web, int cantidadAccionesSerieA, int cantidadAccionesSerieB, int cantidadAccionistas,
			String email, String direccion, String telefono, Representante representante, GiroComercial giroComercial,
			Accion accion) {
		super();
		this.accionista = accionista;
		this.heredero = heredero;
		this.idEmpresa = idEmpresa;
		this.rut = rut;
		this.razonSocial = razonSocial;
		this.logo = logo;
		this.web = web;
		this.cantidadAccionesSerieA = cantidadAccionesSerieA;
		this.cantidadAccionesSerieB = cantidadAccionesSerieB;
		this.cantidadAccionistas = cantidadAccionistas;
		Email = email;
		this.direccion = direccion;
		this.telefono = telefono;
		this.representante = representante;
		this.giroComercial = giroComercial;
		this.accion = accion;
	}

	@Override
	public String toString() {
		return "Empresa [accionista=" + accionista + ", heredero=" + heredero + ", idEmpresa=" + idEmpresa + ", rut="
				+ rut + ", razonSocial=" + razonSocial + ", logo=" + logo + ", web=" + web + ", cantidadAccionesSerieA="
				+ cantidadAccionesSerieA + ", cantidadAccionesSerieB=" + cantidadAccionesSerieB
				+ ", cantidadAccionistas=" + cantidadAccionistas + ", Email=" + Email + ", direccion=" + direccion
				+ ", telefono=" + telefono + ", representante=" + representante + ", giroComercial=" + giroComercial
				+ ", accion=" + accion + "]";
	}

	public Set<Accionista> getAccionista() {
		return accionista;
	}

	public void setAccionista(Set<Accionista> accionista) {
		this.accionista = accionista;
	}

	public Set<Heredero> getHeredero() {
		return heredero;
	}

	public void setHeredero(Set<Heredero> heredero) {
		this.heredero = heredero;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public int getCantidadAccionesSerieA() {
		return cantidadAccionesSerieA;
	}

	public void setCantidadAccionesSerieA(int cantidadAccionesSerieA) {
		this.cantidadAccionesSerieA = cantidadAccionesSerieA;
	}

	public int getCantidadAccionesSerieB() {
		return cantidadAccionesSerieB;
	}

	public void setCantidadAccionesSerieB(int cantidadAccionesSerieB) {
		this.cantidadAccionesSerieB = cantidadAccionesSerieB;
	}

	public int getCantidadAccionistas() {
		return cantidadAccionistas;
	}

	public void setCantidadAccionistas(int cantidadAccionistas) {
		this.cantidadAccionistas = cantidadAccionistas;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Representante getRepresentante() {
		return representante;
	}

	public void setRepresentante(Representante representante) {
		this.representante = representante;
	}

	public GiroComercial getGiroComercial() {
		return giroComercial;
	}

	public void setGiroComercial(GiroComercial giroComercial) {
		this.giroComercial = giroComercial;
	}

	public Accion getAccion() {
		return accion;
	}

	public void setAccion(Accion accion) {
		this.accion = accion;
	}
		
}