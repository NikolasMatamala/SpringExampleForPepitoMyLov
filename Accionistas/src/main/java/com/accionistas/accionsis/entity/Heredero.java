package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Heredero")
public class Heredero {
	
	/*
	 * id
	 */
	@Id
	@GeneratedValue
	@Column(name = "idHeredero")
	private int idHeredero;
	
	/*rut*/
	@Column(name="rut")
	private String rut;
	
	/** Empresa. */
	@ManyToOne
	private Empresa empresa;
	
	/*
	 * Nombre
	 */
	@Column(name="nombre")
	private String nombre;
	/*
	 * Apellido_paterno
	 */
	@Column(name="apellido_paterno")
	private String apellido_paterno;
	/*
	 * Apellido_materno
	 */
	@Column(name="apellido_materno")
	private String apellido_materno;
	/*
	 *Cantidad_acciones_serie_a
	 */
	@Column(name="cantidad_acciones_serie_a")
	private int cantidad_acciones_serie_a;
	/*
	 * Cantidad_acciones_serie_b
	 */
	@Column(name="cantidad_acciones_serie_b")
	private int cantidad_acciones_serie_b;
	/*
	 *Porcentaje_participacion
	 */
	@Column(name="porcentaje_participacion")
	private double porcentaje_participacion;
	/*
	 *Fecha Nacimiento
	 */
	@Column(name="fecha_nacimiento")
	private String fecha_nacimiento;
	/*
	 * es_valido
	 */
	@Column(name="es_valido")
	private String es_valido;
	/*
	 * Mail
	 */
	@Column(name="Email")
	private String Email;
	/*
	 *Direccion
	 */
	@Column(name="direccion")
	private String direccion;
	/*
	 *Fono
	 */
	@Column(name="fono")
	private String fono;
	/*
	 *Ciudad
	 */
	@Column(name="ciudad")
	private String ciudad;
	/*
	 *Comuna
	 */
	@Column(name="comuna")
	private String comuna;

	/** nacionalidadid. */
	@ManyToOne
	private Nacionalidad nacionalidad;
	
	/** tipo accionistaid. */
	@ManyToOne
	private TipoAccionista tipoAccionista;
	
	/*
	 *Accionistarut
	 */
	@ManyToOne
	private Accionista accionista;
	
	/*
	 * Estado
	 */
	@Column(name="estado")
	private int estado;

	@ManyToOne
	private Regiones region;

	public Heredero() {
		super();
	}

	public Heredero(int idHeredero, String rut, Empresa empresa, String nombre, String apellido_paterno,
			String apellido_materno, int cantidad_acciones_serie_a, int cantidad_acciones_serie_b,
			double porcentaje_participacion, String fecha_nacimiento, String es_valido, String email, String direccion,
			String fono, String ciudad, String comuna, Nacionalidad nacionalidad, TipoAccionista tipoAccionista,
			Accionista accionista, int estado, Regiones region) {
		super();
		this.idHeredero = idHeredero;
		this.rut = rut;
		this.empresa = empresa;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		this.cantidad_acciones_serie_a = cantidad_acciones_serie_a;
		this.cantidad_acciones_serie_b = cantidad_acciones_serie_b;
		this.porcentaje_participacion = porcentaje_participacion;
		this.fecha_nacimiento = fecha_nacimiento;
		this.es_valido = es_valido;
		Email = email;
		this.direccion = direccion;
		this.fono = fono;
		this.ciudad = ciudad;
		this.comuna = comuna;
		this.nacionalidad = nacionalidad;
		this.tipoAccionista = tipoAccionista;
		this.accionista = accionista;
		this.estado = estado;
		this.region = region;
	}

	@Override
	public String toString() {
		return "Heredero [idHeredero=" + idHeredero + ", rut=" + rut + ", empresa=" + empresa + ", nombre=" + nombre
				+ ", apellido_paterno=" + apellido_paterno + ", apellido_materno=" + apellido_materno
				+ ", cantidad_acciones_serie_a=" + cantidad_acciones_serie_a + ", cantidad_acciones_serie_b="
				+ cantidad_acciones_serie_b + ", porcentaje_participacion=" + porcentaje_participacion
				+ ", fecha_nacimiento=" + fecha_nacimiento + ", es_valido=" + es_valido + ", Email=" + Email
				+ ", direccion=" + direccion + ", fono=" + fono + ", ciudad=" + ciudad + ", comuna=" + comuna
				+ ", nacionalidad=" + nacionalidad + ", tipoAccionista=" + tipoAccionista + ", accionista=" + accionista
				+ ", estado=" + estado + ", region=" + region + "]";
	}

	public int getIdHeredero() {
		return idHeredero;
	}

	public void setIdHeredero(int idHeredero) {
		this.idHeredero = idHeredero;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public int getCantidad_acciones_serie_a() {
		return cantidad_acciones_serie_a;
	}

	public void setCantidad_acciones_serie_a(int cantidad_acciones_serie_a) {
		this.cantidad_acciones_serie_a = cantidad_acciones_serie_a;
	}

	public int getCantidad_acciones_serie_b() {
		return cantidad_acciones_serie_b;
	}

	public void setCantidad_acciones_serie_b(int cantidad_acciones_serie_b) {
		this.cantidad_acciones_serie_b = cantidad_acciones_serie_b;
	}

	public double getPorcentaje_participacion() {
		return porcentaje_participacion;
	}

	public void setPorcentaje_participacion(double porcentaje_participacion) {
		this.porcentaje_participacion = porcentaje_participacion;
	}

	public String getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(String fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getEs_valido() {
		return es_valido;
	}

	public void setEs_valido(String es_valido) {
		this.es_valido = es_valido;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public Nacionalidad getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Nacionalidad nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public TipoAccionista getTipoAccionista() {
		return tipoAccionista;
	}

	public void setTipoAccionista(TipoAccionista tipoAccionista) {
		this.tipoAccionista = tipoAccionista;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Regiones getRegion() {
		return region;
	}

	public void setRegion(Regiones region) {
		this.region = region;
	}

	
}