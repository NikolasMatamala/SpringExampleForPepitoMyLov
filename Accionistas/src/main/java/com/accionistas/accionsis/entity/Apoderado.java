package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Clase Apoderado.
 */
@Entity
@Table(name="Apoderado")
public class Apoderado {
	
	@OneToMany
	(mappedBy = "apoderado")
	private Set<AsistenciaJuntaAccionista> asistenciaJunta = new HashSet<>();
	
	/** id. */
	@Id
	@GeneratedValue
	@Column(name="idApoderado")
	private int idApoderado;
	
	/** rut. */
	@Column(name="rut")
	private String rut;
	
	/** nombre. */
	@Column(name="nombre")
	private String nombre;
	
	/** apellido paterno. */
	@Column(name="apellido_paterno")
	private String apellido_paterno;
	
	/** apellido materno. */
	@Column(name="apellido_materno")
	private String apellido_materno;
	
	/** mail. */
	@Column(name="Email")
	private String Email;
	
	/** fono. */
	@Column(name="fono")
	private String fono;
	
	/** accionistarut. */
//	@ManyToOne
//	private Accionista accionista;
	@OneToMany
	(mappedBy = "apoderado")
	private Set<Accionista> accionista = new HashSet<>();
	
	@Column(name="estado")
	private int estado;
	
	//Se utiliza cuando un accionista no contiene apoderado
	@Column(name="tipoApoderado")
	private int tipoApoderado;

	public Apoderado() {
		super();
	}

	public Apoderado(Set<AsistenciaJuntaAccionista> asistenciaJunta, int idApoderado, String rut, String nombre,
			String apellido_paterno, String apellido_materno, String email, String fono, Set<Accionista> accionista,
			int estado, int tipoApoderado) {
		super();
		this.asistenciaJunta = asistenciaJunta;
		this.idApoderado = idApoderado;
		this.rut = rut;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		Email = email;
		this.fono = fono;
		this.accionista = accionista;
		this.estado = estado;
		this.tipoApoderado = tipoApoderado;
	}

	@Override
	public String toString() {
		return "Apoderado [asistenciaJunta=" + asistenciaJunta + ", idApoderado=" + idApoderado + ", rut=" + rut
				+ ", nombre=" + nombre + ", apellido_paterno=" + apellido_paterno + ", apellido_materno="
				+ apellido_materno + ", Email=" + Email + ", fono=" + fono + ", accionista=" + accionista + ", estado="
				+ estado + ", tipoApoderado=" + tipoApoderado + "]";
	}

	public Set<AsistenciaJuntaAccionista> getAsistenciaJunta() {
		return asistenciaJunta;
	}

	public void setAsistenciaJunta(Set<AsistenciaJuntaAccionista> asistenciaJunta) {
		this.asistenciaJunta = asistenciaJunta;
	}

	public int getIdApoderado() {
		return idApoderado;
	}

	public void setIdApoderado(int idApoderado) {
		this.idApoderado = idApoderado;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public Set<Accionista> getAccionista() {
		return accionista;
	}

	public void setAccionista(Set<Accionista> accionista) {
		this.accionista = accionista;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getTipoApoderado() {
		return tipoApoderado;
	}

	public void setTipoApoderado(int tipoApoderado) {
		this.tipoApoderado = tipoApoderado;
	}

	
}
