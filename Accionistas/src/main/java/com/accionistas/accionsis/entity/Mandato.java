package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Mandato")
public class Mandato {
	
	@OneToMany
	(mappedBy = "mandato")
	private Set<AsistenciaJuntaAccionista> asistenciaJunta = new HashSet<>();

	@Id
	@GeneratedValue
	@Column(name="idMandato")
	private int idMandato;
	 
	@Column(name="descripcionMandato")
	private String descripcionMandato;

	public Mandato() {
		super();
	}

	public Mandato(Set<AsistenciaJuntaAccionista> asistenciaJunta, int idMandato, String descripcionMandato) {
		super();
		this.asistenciaJunta = asistenciaJunta;
		this.idMandato = idMandato;
		this.descripcionMandato = descripcionMandato;
	}

	@Override
	public String toString() {
		return "Mandato [asistenciaJunta=" + asistenciaJunta + ", idMandato=" + idMandato + ", descripcionMandato="
				+ descripcionMandato + "]";
	}

	public Set<AsistenciaJuntaAccionista> getAsistenciaJunta() {
		return asistenciaJunta;
	}

	public void setAsistenciaJunta(Set<AsistenciaJuntaAccionista> asistenciaJunta) {
		this.asistenciaJunta = asistenciaJunta;
	}

	public int getIdMandato() {
		return idMandato;
	}

	public void setIdMandato(int idMandato) {
		this.idMandato = idMandato;
	}

	public String getDescripcionMandato() {
		return descripcionMandato;
	}

	public void setDescripcionMandato(String descripcionMandato) {
		this.descripcionMandato = descripcionMandato;
	}

	
}
