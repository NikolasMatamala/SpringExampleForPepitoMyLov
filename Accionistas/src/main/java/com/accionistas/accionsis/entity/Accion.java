package com.accionistas.accionsis.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Accion")
public class Accion {
	
	@OneToMany
	(mappedBy = "accion")
	public Set<Empresa> empresa = new HashSet<>();
	
	@Id
	@GeneratedValue
	@Column(name="idAccion")
	private int idAccion;
	
	@Column(name="valorA")
	private double valorA;
	
	@Column(name="valorB")
	private double valorB;
	
	@Column(name = "fechaIngreso")
	private Date fechaIngreso;

	public Accion() {
		super();
	}

	public Accion(Set<Empresa> empresa, int idAccion, double valorA, double valorB, Date fechaIngreso) {
		super();
		this.empresa = empresa;
		this.idAccion = idAccion;
		this.valorA = valorA;
		this.valorB = valorB;
		this.fechaIngreso = fechaIngreso;
	}

	@Override
	public String toString() {
		return "Accion [empresa=" + empresa + ", idAccion=" + idAccion + ", valorA=" + valorA + ", valorB=" + valorB
				+ ", fechaIngreso=" + fechaIngreso + "]";
	}

	public Set<Empresa> getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Set<Empresa> empresa) {
		this.empresa = empresa;
	}

	public int getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(int idAccion) {
		this.idAccion = idAccion;
	}

	public double getValorA() {
		return valorA;
	}

	public void setValorA(double valorA) {
		this.valorA = valorA;
	}

	public double getValorB() {
		return valorB;
	}

	public void setValorB(double valorB) {
		this.valorB = valorB;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	
	
}
