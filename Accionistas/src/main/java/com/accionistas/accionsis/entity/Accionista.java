package com.accionistas.accionsis.entity;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Clase Accionista.
 */
@Entity
@NamedNativeQueries({
@NamedNativeQuery(name="Accionista.findTodos", query="Select TOP 10 a.* from Accionista a order by a.cantidad_acciones_serie_a desc", resultClass = Accionista.class),
@NamedNativeQuery(name="Accionista.findTodosB", query="Select TOP 10 a.* from Accionista a order by a.cantidad_acciones_serie_b desc", resultClass = Accionista.class),
@NamedNativeQuery(name="Accionista.findTodosAB", query="Select TOP 50 a.* from Accionista a order by a.cantidad_acciones_serie_a desc", resultClass = Accionista.class)
})
@Table(name="Accionista")
public class Accionista {
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<AsistenciaJuntaAccionista> asistenciaJunta = new HashSet<>();
	
	
//	@OneToMany
//	(mappedBy = "accionista")
//	private Set<Apoderado> apoderado = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<Heredero> heredero = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<Dividendo> dividendo = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<AccionesConProhibicion> accionesConProhibicion = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<CuentaCorriente> cuentaCorriente = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionista")
	private Set<HistorialTitulo> historialTitulo = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionistaVendedor")
	private Set<Traspasoacciones> traspasoAccionesVendedor = new HashSet<>();
	
	@OneToMany
	(mappedBy = "accionistaComprador")
	private Set<Traspasoacciones> traspasoAccionesComprador = new HashSet<>();
	
	/** id. */
	@Id
	@GeneratedValue
	@Column(name="idAccionista")
	private int idAccionista;
	
	/* rut */
	@Column(name="rut")
	private String rut;
	
	/** empresaid empresa. */
	@ManyToOne
	private Empresa empresa;
	
	/** nombre. */
	@Column(name="nombre")
	private String nombre;
	
	/** apellido paterno. */
	@Column(name="apellido_paterno")
	private String apellido_paterno;
	
	/** apellido materno. */
	@Column(name="apellido_materno")
	private String apellido_materno;
	
	/** cantidad acciones serie a. */
	@Column(name="cantidad_acciones_serie_a")
	private int cantidad_acciones_serie_a;
	
	/** cantidad acciones serie b. */
	@Column(name="cantidad_acciones_serie_b")
	private int cantidad_acciones_serie_b;
	
	/** porcentaje participacion. */
	@Column(name="porcentaje_participacion")
	private double porcentaje_participacion;
	
	/** fecha nacimiento. */
	@Column(name="fecha_nacimiento")
	private Date fecha_nacimiento;
	
	/** fecha Ingreso. */
	@Column(name="fecha_ingreso")
	private Date fecha_ingreso;
	
	/** es valido. */
	@Column(name="esValido")
	private String esValido;
	
	/** mail. */
	@Column(name="Email")
	private String Email;
	
	/** direccion. */
	@Column(name="direccion")
	private String direccion;
	
	/** fono. */
	@Column(name="fono")
	private String fono;
	
	/** nacionalidadid. */
	@ManyToOne
	private Nacionalidad nacionalidad;
	
	/** tipo accionistaid. */
	@ManyToOne
	private TipoAccionista tipoAccionista;
	
	/** ciudad. */
	@Column(name="ciudad")
	private String ciudad;
	
	/** comuna. */
	@Column(name="comuna")
	private String comuna;
	
	@ManyToOne
	private Regiones region;
	
	@ManyToOne
	private Apoderado apoderado;
	
	/** estado */
	@Column(name="estado")
	private int estado;

	public Accionista() {
		super();
	}

	public Accionista(Set<AsistenciaJuntaAccionista> asistenciaJunta, Set<Heredero> heredero, Set<Dividendo> dividendo,
			Set<AccionesConProhibicion> accionesConProhibicion, Set<CuentaCorriente> cuentaCorriente,
			Set<HistorialTitulo> historialTitulo, Set<Traspasoacciones> traspasoAccionesVendedor,
			Set<Traspasoacciones> traspasoAccionesComprador, int idAccionista, String rut, Empresa empresa,
			String nombre, String apellido_paterno, String apellido_materno, int cantidad_acciones_serie_a,
			int cantidad_acciones_serie_b, double porcentaje_participacion, Date fecha_nacimiento, Date fecha_ingreso,
			String esValido, String email, String direccion, String fono, Nacionalidad nacionalidad,
			TipoAccionista tipoAccionista, String ciudad, String comuna, Regiones region, Apoderado apoderado,
			int estado) {
		super();
		this.asistenciaJunta = asistenciaJunta;
		this.heredero = heredero;
		this.dividendo = dividendo;
		this.accionesConProhibicion = accionesConProhibicion;
		this.cuentaCorriente = cuentaCorriente;
		this.historialTitulo = historialTitulo;
		this.traspasoAccionesVendedor = traspasoAccionesVendedor;
		this.traspasoAccionesComprador = traspasoAccionesComprador;
		this.idAccionista = idAccionista;
		this.rut = rut;
		this.empresa = empresa;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		this.cantidad_acciones_serie_a = cantidad_acciones_serie_a;
		this.cantidad_acciones_serie_b = cantidad_acciones_serie_b;
		this.porcentaje_participacion = porcentaje_participacion;
		this.fecha_nacimiento = fecha_nacimiento;
		this.fecha_ingreso = fecha_ingreso;
		this.esValido = esValido;
		Email = email;
		this.direccion = direccion;
		this.fono = fono;
		this.nacionalidad = nacionalidad;
		this.tipoAccionista = tipoAccionista;
		this.ciudad = ciudad;
		this.comuna = comuna;
		this.region = region;
		this.apoderado = apoderado;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Accionista [asistenciaJunta=" + asistenciaJunta + ", heredero=" + heredero + ", dividendo=" + dividendo
				+ ", accionesConProhibicion=" + accionesConProhibicion + ", cuentaCorriente=" + cuentaCorriente
				+ ", historialTitulo=" + historialTitulo + ", traspasoAccionesVendedor=" + traspasoAccionesVendedor
				+ ", traspasoAccionesComprador=" + traspasoAccionesComprador + ", idAccionista=" + idAccionista
				+ ", rut=" + rut + ", empresa=" + empresa + ", nombre=" + nombre + ", apellido_paterno="
				+ apellido_paterno + ", apellido_materno=" + apellido_materno + ", cantidad_acciones_serie_a="
				+ cantidad_acciones_serie_a + ", cantidad_acciones_serie_b=" + cantidad_acciones_serie_b
				+ ", porcentaje_participacion=" + porcentaje_participacion + ", fecha_nacimiento=" + fecha_nacimiento
				+ ", fecha_ingreso=" + fecha_ingreso + ", esValido=" + esValido + ", Email=" + Email + ", direccion="
				+ direccion + ", fono=" + fono + ", nacionalidad=" + nacionalidad + ", tipoAccionista=" + tipoAccionista
				+ ", ciudad=" + ciudad + ", comuna=" + comuna + ", region=" + region + ", apoderado=" + apoderado
				+ ", estado=" + estado + "]";
	}

	public Set<AsistenciaJuntaAccionista> getAsistenciaJunta() {
		return asistenciaJunta;
	}

	public void setAsistenciaJunta(Set<AsistenciaJuntaAccionista> asistenciaJunta) {
		this.asistenciaJunta = asistenciaJunta;
	}

	public Set<Heredero> getHeredero() {
		return heredero;
	}

	public void setHeredero(Set<Heredero> heredero) {
		this.heredero = heredero;
	}

	public Set<Dividendo> getDividendo() {
		return dividendo;
	}

	public void setDividendo(Set<Dividendo> dividendo) {
		this.dividendo = dividendo;
	}

	public Set<AccionesConProhibicion> getAccionesConProhibicion() {
		return accionesConProhibicion;
	}

	public void setAccionesConProhibicion(Set<AccionesConProhibicion> accionesConProhibicion) {
		this.accionesConProhibicion = accionesConProhibicion;
	}

	public Set<CuentaCorriente> getCuentaCorriente() {
		return cuentaCorriente;
	}

	public void setCuentaCorriente(Set<CuentaCorriente> cuentaCorriente) {
		this.cuentaCorriente = cuentaCorriente;
	}

	public Set<HistorialTitulo> getHistorialTitulo() {
		return historialTitulo;
	}

	public void setHistorialTitulo(Set<HistorialTitulo> historialTitulo) {
		this.historialTitulo = historialTitulo;
	}

	public Set<Traspasoacciones> getTraspasoAccionesVendedor() {
		return traspasoAccionesVendedor;
	}

	public void setTraspasoAccionesVendedor(Set<Traspasoacciones> traspasoAccionesVendedor) {
		this.traspasoAccionesVendedor = traspasoAccionesVendedor;
	}

	public Set<Traspasoacciones> getTraspasoAccionesComprador() {
		return traspasoAccionesComprador;
	}

	public void setTraspasoAccionesComprador(Set<Traspasoacciones> traspasoAccionesComprador) {
		this.traspasoAccionesComprador = traspasoAccionesComprador;
	}

	public int getIdAccionista() {
		return idAccionista;
	}

	public void setIdAccionista(int idAccionista) {
		this.idAccionista = idAccionista;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public int getCantidad_acciones_serie_a() {
		return cantidad_acciones_serie_a;
	}

	public void setCantidad_acciones_serie_a(int cantidad_acciones_serie_a) {
		this.cantidad_acciones_serie_a = cantidad_acciones_serie_a;
	}

	public int getCantidad_acciones_serie_b() {
		return cantidad_acciones_serie_b;
	}

	public void setCantidad_acciones_serie_b(int cantidad_acciones_serie_b) {
		this.cantidad_acciones_serie_b = cantidad_acciones_serie_b;
	}

	public double getPorcentaje_participacion() {
		return porcentaje_participacion;
	}

	public void setPorcentaje_participacion(double porcentaje_participacion) {
		this.porcentaje_participacion = porcentaje_participacion;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public Date getFecha_ingreso() {
		return fecha_ingreso;
	}

	public void setFecha_ingreso(Date fecha_ingreso) {
		this.fecha_ingreso = fecha_ingreso;
	}

	public String getEsValido() {
		return esValido;
	}

	public void setEsValido(String esValido) {
		this.esValido = esValido;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public Nacionalidad getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Nacionalidad nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public TipoAccionista getTipoAccionista() {
		return tipoAccionista;
	}

	public void setTipoAccionista(TipoAccionista tipoAccionista) {
		this.tipoAccionista = tipoAccionista;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public Regiones getRegion() {
		return region;
	}

	public void setRegion(Regiones region) {
		this.region = region;
	}

	public Apoderado getApoderado() {
		return apoderado;
	}

	public void setApoderado(Apoderado apoderado) {
		this.apoderado = apoderado;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}
}
