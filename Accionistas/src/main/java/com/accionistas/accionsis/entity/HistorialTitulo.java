package com.accionistas.accionsis.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "HistorialTitulo")
public class HistorialTitulo {
	
	@Id
	@GeneratedValue
	@Column(name = "idHistorialTitulo")
	private int idHistorialTitulo;
	
	@ManyToOne
	private Accionista accionista;
	
	@Column(name = "detalleSolicitud")
	private String detalleSolicitud;

	public HistorialTitulo() {
		super();
	}

	public HistorialTitulo(int idHistorialTitulo, Accionista accionista, String detalleSolicitud) {
		super();
		this.idHistorialTitulo = idHistorialTitulo;
		this.accionista = accionista;
		this.detalleSolicitud = detalleSolicitud;
	}

	@Override
	public String toString() {
		return "HistorialTitulo [idHistorialTitulo=" + idHistorialTitulo + ", accionista=" + accionista
				+ ", detalleSolicitud=" + detalleSolicitud + "]";
	}

	public int getIdHistorialTitulo() {
		return idHistorialTitulo;
	}

	public void setIdHistorialTitulo(int idHistorialTitulo) {
		this.idHistorialTitulo = idHistorialTitulo;
	}

	public Accionista getAccionista() {
		return accionista;
	}

	public void setAccionista(Accionista accionista) {
		this.accionista = accionista;
	}

	public String getDetalleSolicitud() {
		return detalleSolicitud;
	}

	public void setDetalleSolicitud(String detalleSolicitud) {
		this.detalleSolicitud = detalleSolicitud;
	}

	
}
