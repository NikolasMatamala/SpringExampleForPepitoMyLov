package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Tipogirocomercial")
public class GiroComercial {
	
	@OneToMany
	(mappedBy = "giroComercial")
	private Set<Empresa> empresa = new HashSet<>();
	
	@Id
	@GeneratedValue
	@Column(name="idGiroComercial")
	private int idGiroComercial;
	
	@Column(name="descripcion")
	private String descripcion;

	public GiroComercial() {
		super();
	}

	public GiroComercial(Set<Empresa> empresa, int idGiroComercial, String descripcion) {
		super();
		this.empresa = empresa;
		this.idGiroComercial = idGiroComercial;
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "GiroComercial [empresa=" + empresa + ", idGiroComercial=" + idGiroComercial + ", descripcion="
				+ descripcion + "]";
	}

	public Set<Empresa> getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Set<Empresa> empresa) {
		this.empresa = empresa;
	}

	public int getIdGiroComercial() {
		return idGiroComercial;
	}

	public void setIdGiroComercial(int idGiroComercial) {
		this.idGiroComercial = idGiroComercial;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}
