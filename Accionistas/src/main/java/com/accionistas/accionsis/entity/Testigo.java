package com.accionistas.accionsis.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Clase Testigo.
 */
@Entity
@Table(name="Testigo")
public class Testigo {
	
	@OneToMany
	(mappedBy = "testigo")
	private Set<Traspasoacciones> traspasoAcciones = new HashSet<>();
	
	/** id. */
	@Id
	@GeneratedValue
	@Column(name="idTestigo")
	private int idTestigo;
	
	/*rut*/
	@Column(name="rut")
	private String rut;
	
	/** nombre. */
	@Column(name="nombre")
	private String nombre;
	
	/** apellido paterno. */
	@Column(name="apellido_paterno")
	private String apellido_paterno;
	
	/** apellido materno. */
	@Column(name="apellido_materno")
	private String apellido_materno;
	
	/** mail. */
	@Column(name="Email")
	private String Email;
	
	/** fono. */
	@Column(name="fono")
	private String fono;
	
	/*Estado*/
	@Column(name="estado")
	private int estado;

	public Testigo() {
		super();
	}

	public Testigo(Set<Traspasoacciones> traspasoAcciones, int idTestigo, String rut, String nombre,
			String apellido_paterno, String apellido_materno, String email, String fono, int estado) {
		super();
		this.traspasoAcciones = traspasoAcciones;
		this.idTestigo = idTestigo;
		this.rut = rut;
		this.nombre = nombre;
		this.apellido_paterno = apellido_paterno;
		this.apellido_materno = apellido_materno;
		Email = email;
		this.fono = fono;
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Testigo [traspasoAcciones=" + traspasoAcciones + ", idTestigo=" + idTestigo + ", rut=" + rut
				+ ", nombre=" + nombre + ", apellido_paterno=" + apellido_paterno + ", apellido_materno="
				+ apellido_materno + ", Email=" + Email + ", fono=" + fono + ", estado=" + estado + "]";
	}

	public Set<Traspasoacciones> getTraspasoAcciones() {
		return traspasoAcciones;
	}

	public void setTraspasoAcciones(Set<Traspasoacciones> traspasoAcciones) {
		this.traspasoAcciones = traspasoAcciones;
	}

	public int getIdTestigo() {
		return idTestigo;
	}

	public void setIdTestigo(int idTestigo) {
		this.idTestigo = idTestigo;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_paterno() {
		return apellido_paterno;
	}

	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}

	public String getApellido_materno() {
		return apellido_materno;
	}

	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	
}
