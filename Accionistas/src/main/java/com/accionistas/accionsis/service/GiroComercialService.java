package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.GiroComercial;
import com.accionistas.accionsis.model.GiroComercialModel;

public interface GiroComercialService {
	/**
	 * Listar todos los giros comerciales.
	 * @return Lista de giros comerciales
	 */
	public abstract List<GiroComercialModel> listAllGirosComerciales();
	
	/**
	 * Encontrar giro comercial por id.
	 * @param id 
	 * @return Giro comercial
	 */
	public abstract GiroComercial findGiroComercialById(int id);
	
	
	/**
	 * Encontrar GiroComercialModel a travez de  id model.
	 * @param id 
	 * @return GiroComercialModel model
	 */
	public abstract GiroComercialModel findGiroComercialModelByModel(GiroComercialModel model);

}
