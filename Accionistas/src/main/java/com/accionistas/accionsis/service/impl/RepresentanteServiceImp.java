package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.RepresentanteConverter;
import com.accionistas.accionsis.entity.Representante;
import com.accionistas.accionsis.model.RepresentanteModel;
import com.accionistas.accionsis.repository.RepresentanteJpaRepository;
import com.accionistas.accionsis.service.RepresentanteService;

@Service("representanteServiceImp")
public class RepresentanteServiceImp implements RepresentanteService {
	
	private static final Log LOG = LogFactory.getLog(RepresentanteServiceImp.class);
	
	@Autowired
	@Qualifier("representanteJpaRepository")
	private RepresentanteJpaRepository representanteJpaRepository;
	
	@Autowired
	@Qualifier("representanteConverter")
	private RepresentanteConverter representanteConverter;
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#addRepresentante(com.accionistas.accionsis.model.RepresentanteModel)
		 */
		@Override
		public RepresentanteModel addRepresentante(RepresentanteModel representanteModel) {
			LOG.info("Call: " + "addRepresentante()" +" Desde RepresentanteService");
			Representante representante= representanteJpaRepository.save(representanteConverter.modelo2Entidad(representanteModel));
			return representanteConverter.entidad2Model(representante);
		}
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#listAllRepresentante()
		 */
		@Override
		public List<RepresentanteModel> listAllRepresentante() {
			LOG.info("Call: " + "listAllRepresentantes()");
			List<Representante> representantes = representanteJpaRepository.findAll();
			List<RepresentanteModel> representantesModel = new ArrayList<RepresentanteModel>();
			for (Representante representante: representantes) {
				representantesModel.add(representanteConverter.entidad2Model(representante));
			}		 
			return representantesModel;
		}
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#findRepresentanteByRut(java.lang.String)
		 */
		@Override
		public Representante findRepresentanteByRut(String rut) {
			return representanteJpaRepository.findByRut(rut);
		}
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#findRepresentanteByRutModel(java.lang.String)
		 */
		@Override
		public RepresentanteModel findRepresentanteByRutModel(String rut) {
			return representanteConverter.entidad2Model(findRepresentanteByRut(rut));
		}
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#removeRepresentante(java.lang.String)
		 */
		@Override
		public void removeRepresentante(String rut) {			
		Representante representante = findRepresentanteByRut(rut);
		if (null != representante) {
			representanteJpaRepository.delete(representante);}
		}
		
		/*
		 * (non-Javadoc)
		 * @see com.accionistas.accionsis.service.RepresentanteService#listAllRepresentantes_active()
		 */
		@Override
		public List<RepresentanteModel> listAllRepresentantes_active() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public Representante updateRepresentante(Representante representante) {
			return representanteJpaRepository.save(representante);
		}
		@Override
		public Representante findRepresentanteByID(int id) {
			Representante rep = new Representante();
			rep.setIdRepresentante(id);
			return representanteJpaRepository.findByIdRepresentante(id);
		}
		@Override
		public Representante findByidRepresentante(int id) {
			// TODO Auto-generated method stub
			return representanteJpaRepository.findByidRepresentante(id);
		}

}
