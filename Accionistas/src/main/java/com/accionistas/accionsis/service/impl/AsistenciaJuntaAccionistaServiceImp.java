package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.AsistenciaJuntaAccionistaConverter;
import com.accionistas.accionsis.component.JuntaAccionistaConverter;
import com.accionistas.accionsis.entity.AsistenciaJuntaAccionista;
import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.AsistenciaJuntaAccionistaModel;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
import com.accionistas.accionsis.repository.AsistenciaJuntaAccionistaJpaRepository;
import com.accionistas.accionsis.service.AsistenciaJuntaAccionistaService;

@Service("AsistenciaJuntaAccionistaServiceImp")
public class AsistenciaJuntaAccionistaServiceImp implements AsistenciaJuntaAccionistaService{
	
	private static final Log LOG = LogFactory.getLog(AsistenciaJuntaAccionistaServiceImp.class);
	
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaJpaRepository")
	private AsistenciaJuntaAccionistaJpaRepository asistenciaJuntaAccionistaJpaRepository;
	
	@Autowired
	@Qualifier("AsistenciaJuntaAccionistaConverter")
	private AsistenciaJuntaAccionistaConverter asistenciaJuntaAccionistaConverter;
	
	@Autowired
	@Qualifier("juntaAccionistaConverter")
	private JuntaAccionistaConverter juntaAccionistaConverter;
	

	@Override
	public AsistenciaJuntaAccionistaModel addAsistenciaJuntaAccionista(AsistenciaJuntaAccionistaModel asistenciaJuntaAccionistaModel) {
		LOG.info("AgregarAsistencia: " + "addAsistenciaJuntaAccionista() -- desde AsistenciaJuntaAccionistaService");
		AsistenciaJuntaAccionista asistenciaJuntaAccionistaEntity = asistenciaJuntaAccionistaConverter.modelo2Entidad(asistenciaJuntaAccionistaModel);		
		asistenciaJuntaAccionistaEntity = asistenciaJuntaAccionistaJpaRepository.save(asistenciaJuntaAccionistaEntity);
		AsistenciaJuntaAccionistaModel modelo = asistenciaJuntaAccionistaConverter.entidad2Modelo(asistenciaJuntaAccionistaEntity); 
		return modelo;
	}

	@Override
	public List<AsistenciaJuntaAccionistaModel> listAllAsistenciaJuntaAccionista() {
		LOG.info("MostarAsistencia: " + "listAllAsistenciaJuntaAccionista() -- desde AsistenciaJuntaAccionistaService");
		List<AsistenciaJuntaAccionista> listaAsistenciaJuntaAccionista = asistenciaJuntaAccionistaJpaRepository.findAll();
		List<AsistenciaJuntaAccionistaModel> listaAsistenciaJuntaAccionistaModel = new ArrayList<AsistenciaJuntaAccionistaModel>();
		for(AsistenciaJuntaAccionista asistenciaJuntaAccionista : listaAsistenciaJuntaAccionista) {
			listaAsistenciaJuntaAccionistaModel.add(asistenciaJuntaAccionistaConverter.entidad2Modelo(asistenciaJuntaAccionista));
		}
		return listaAsistenciaJuntaAccionistaModel;
	}

	@Override
	public List<AsistenciaJuntaAccionistaModel> findAsistenciaJuntaAccionistaByJuntaAccionista(JuntaAccionistaModel juntaAccionista) {
		List<AsistenciaJuntaAccionista> entity = asistenciaJuntaAccionistaJpaRepository.findAsistenciaJuntaAccionistaByJuntaAccionista(juntaAccionistaConverter.modelo2Entidad(juntaAccionista));
		List<AsistenciaJuntaAccionistaModel> model = new ArrayList<>();
		for(AsistenciaJuntaAccionista entidad : entity) {
			model.add(asistenciaJuntaAccionistaConverter.entidad2Modelo(entidad));
		}
		return model;
	}

	@Override
	public void removeAsistenciaJuntaAccionista(JuntaAccionista juntaAccionista) {
		LOG.info("RemoveAsistenciaJuntaAccionista: " + "removeAsistenciaJuntaAccionista() -- desde AsistenciaJuntaAccionistaService");
		List<AsistenciaJuntaAccionista> asistenciaJuntaAccionista = asistenciaJuntaAccionistaJpaRepository.findAsistenciaJuntaAccionistaByJuntaAccionista(juntaAccionista);
		if(null != asistenciaJuntaAccionista) {
			asistenciaJuntaAccionistaJpaRepository.delete(asistenciaJuntaAccionista);
		}
	}

	@Override
	public AsistenciaJuntaAccionistaModel updateAsistencia(AsistenciaJuntaAccionista asistenciaJuntaAccionista) {
		
		return asistenciaJuntaAccionistaConverter.entidad2Modelo(asistenciaJuntaAccionistaJpaRepository.save(asistenciaJuntaAccionista));
	}

	@Override
	public AsistenciaJuntaAccionistaModel findAsistenciaJuntaAccionistaByIdAsistenciaJuntaAccionista(int id) {
		return asistenciaJuntaAccionistaConverter.entidad2Modelo(asistenciaJuntaAccionistaJpaRepository.findAsistenciaJuntaAccionistaByidAsistencia(id));
	}

	@Override
	public List<AsistenciaJuntaAccionista> findAsistenciaJuntaAccionistaEntityByJuntaAccionista(
			JuntaAccionistaModel juntaAccionista) {
		List<AsistenciaJuntaAccionista> entity = asistenciaJuntaAccionistaJpaRepository.findAsistenciaJuntaAccionistaByJuntaAccionista(juntaAccionistaConverter.modelo2Entidad(juntaAccionista));		
		return entity;
	}
	
	


}
