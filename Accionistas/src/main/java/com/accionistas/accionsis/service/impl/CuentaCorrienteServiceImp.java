package com.accionistas.accionsis.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.CuentaCorriente;
import com.accionistas.accionsis.repository.CuentaCorrienteJpaRepository;
import com.accionistas.accionsis.service.CuentaCorrienteService;

@Service("cuentaCorrienteServiceImp")
public class CuentaCorrienteServiceImp implements CuentaCorrienteService {

	private static final Log LOG = LogFactory.getLog(CuentaCorrienteServiceImp.class);
	
	@Autowired
	@Qualifier("cuentaCorrienteJpaRepository")
	public CuentaCorrienteJpaRepository cuentaCorrienteJpaRepository;

	@Override
	public CuentaCorriente listAllCuentaCorriente(Accionista accionista) {
		LOG.info("Call: " + "Lista Cuenta Corrietne ---- Desde ServiceIMP");
		return cuentaCorrienteJpaRepository.findCuentaCorrienteByAccionista(accionista);
	}

	@Override
	public List<CuentaCorriente> listAllCuentaCorrienteEntity() {
		List<CuentaCorriente> cuenta = cuentaCorrienteJpaRepository.findAll();
		return cuenta;
	}
}
