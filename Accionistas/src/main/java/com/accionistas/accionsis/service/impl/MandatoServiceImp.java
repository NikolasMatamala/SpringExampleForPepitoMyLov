package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.MandatoConverter;
import com.accionistas.accionsis.entity.Mandato;
import com.accionistas.accionsis.model.MandatoModel;
import com.accionistas.accionsis.repository.MandatoJpaRepository;
import com.accionistas.accionsis.service.MandatoService;

@Service("mandatarioServiceImp")
public class MandatoServiceImp implements MandatoService {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(MandatoServiceImp.class);
		
		/** The mandatario jpa repository. */
		@Autowired
		@Qualifier("mandatoJpaRepository")
		private MandatoJpaRepository mandatoJpaRepository;
		
		/** The mandatario converter. */
		@Autowired
		@Qualifier("mandatoConverter")
		private MandatoConverter mandatoConverter;
		
		/**
		 * Metodo para agregar mandatario
		 * @Param mandatarioModel
		 */
		@Override
		public MandatoModel addMandato(MandatoModel mandatoModel) {
			LOG.info("Call: " + "addApoderado()" +" Desde MandatarioService");
			Mandato mandato= mandatoJpaRepository.save(mandatoConverter.modelo2Entidad(mandatoModel));
			return mandatoConverter.entidad2Model(mandato);
		}

		/** 
		 * Metodo para listar apoderados
		 */
		@Override
		public List<MandatoModel> listAllMandato() {
			LOG.info("Call: " + "listAllApoderados()");
			List<Mandato> mandato = mandatoJpaRepository.findAll();
			List<MandatoModel> mandatoModel = new ArrayList<MandatoModel>();
			for (Mandato mandatoI: mandato) {
				mandatoModel.add(mandatoConverter.entidad2Model(mandatoI));
			}		 
			return mandatoModel;
		}

		/**
		 * Metodo para encontrar mandatarios por id
		 * @param id
		 */
		@Override
		public Mandato findMandatoById(int id) {
			return mandatoJpaRepository.findByidMandato(id);
		}

		/**
		 * Metodo para remover mandatario
		 * @param id
		 */
		@Override
		public boolean removeMandato(int id) {			
			Mandato mandato= findMandatoById(id);
			if (null != mandato) {
				mandatoJpaRepository.delete(mandato);
				return true;
			}
			return false;
		}

		/**
		 * Metodo para encontrar apoderado por Id de apoderadoModel
		 * @param id
		 */
		@Override
		public MandatoModel findMandatoByIdModel(int id) {
			return mandatoConverter.entidad2Model(findMandatoById(id));
		}

		@Override
		public List<MandatoModel> listAllMandato_active() {
			// TODO Auto-generated method stub
			return null;
		}

}
