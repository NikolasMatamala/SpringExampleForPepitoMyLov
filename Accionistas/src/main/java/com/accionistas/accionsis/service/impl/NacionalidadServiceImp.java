package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.NacionalidadConverter;
import com.accionistas.accionsis.entity.Nacionalidad;
import com.accionistas.accionsis.model.NacionalidadModel;
import com.accionistas.accionsis.repository.NacionalidadJpaRepository;
import com.accionistas.accionsis.service.NacionalidadService;

@Service("nacionalidadServiceImp")
public class NacionalidadServiceImp implements NacionalidadService {

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(NacionalidadServiceImp.class);
	
	/** The nacionalidad jpa repository. */
	@Autowired
	@Qualifier("nacionalidadJpaRepository")
	private NacionalidadJpaRepository nacionalidadJpaRepository;
	
	/** The nacionalidad converter. */
	@Autowired
	@Qualifier("nacionalidadConverter")
	private NacionalidadConverter nacionalidadConverter;
	
	
	
	@Override
	public List<NacionalidadModel> listAllNacionalidades() {
		LOG.info("Call: " + "listAllNacionalidades() -- desde service");
		List<Nacionalidad> nacionalidades = nacionalidadJpaRepository.findAll();
		List<NacionalidadModel> nacionalidadesModel = new ArrayList<NacionalidadModel>();
		for (Nacionalidad nacionalidad: nacionalidades) {
			nacionalidadesModel.add(nacionalidadConverter.entidad2Model(nacionalidad));
		}		 
		return nacionalidadesModel;
	}

	@Override
	public Nacionalidad findNacionalidadById(int id) {
		Nacionalidad nacionalidad = nacionalidadJpaRepository.findByIdNacionalidad(id);
		return nacionalidad;
	}

	@Override
	public NacionalidadModel findNacionalidadModelByIdModel(int id) {
		return nacionalidadConverter.entidad2Model(findNacionalidadById(id));
	}

	@Override
	public NacionalidadModel findByIdModel(Nacionalidad nacionalidad) {
		return findByIdModel(nacionalidad);
	}

}
