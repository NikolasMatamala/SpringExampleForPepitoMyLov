package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.AccionConverter;
import com.accionistas.accionsis.entity.Accion;
import com.accionistas.accionsis.model.AccionModel;
import com.accionistas.accionsis.repository.AccionJpaRepository;
import com.accionistas.accionsis.service.AccionService;



@Service("accionServiceImp")
public class AccionServiceImp implements AccionService {

	private static final Log LOG = LogFactory.getLog(AccionServiceImp.class);
	
	@Autowired
	@Qualifier("accionJpaRepository")
	private AccionJpaRepository accionJpaRepository;
	
	@Autowired
	@Qualifier("accionConverter")
	private AccionConverter accionConverter;
	
	@Override
	public AccionModel addAccion(AccionModel accionModel) {
		LOG.info("Call: " + "addAccion()" +" Desde AccionService");
		Accion accion = accionJpaRepository.save(accionConverter.modelo2Entidad(accionModel));
		return accionConverter.entidad2Modelo(accion);
	}

	@Override
	public List<AccionModel> listAllAccion() {
		LOG.info("Call: " + "listAllAccion() -- desde AccionService");
		List<Accion> acciones = accionJpaRepository.findAll();
		List<AccionModel> accionesModel = new ArrayList<AccionModel>();
		for(Accion accion  : acciones) {
			accionesModel.add(accionConverter.entidad2Modelo(accion));
		}
		return accionesModel;
	}
	
	@Override
	public void removeAccion(int id) {
		LOG.info("Call: " + "removeAccion() -- desde AccionService");
		Accion accion = findAccionById_accion(id);
		if(null!= accion) {
			accionJpaRepository.delete(accion);
		}		
	}

	@Override
	public Accion findAccionById_accion(int id) {
		return accionJpaRepository.findByidAccion(id);
	}

	@Override
	public AccionModel findAccionById_accionModel(int id) {
		return accionConverter.entidad2Modelo(findAccionById_accion(id));
	}


}
