package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Nacionalidad;
import com.accionistas.accionsis.model.NacionalidadModel;

public interface NacionalidadService {
	
	/**
	 * Lista todas las nacionalidades.
	 * @return Lista de nacionalidades
	 */
	public abstract List<NacionalidadModel> listAllNacionalidades();
	
	/**
	 * Encontrar nacionalidad por id
	 * @param id 
	 * @return nacionalidad
	 */
	public abstract Nacionalidad findNacionalidadById(int id);
	
	
	/**
	 * Encontrar NacionalidadModel a travez de  id model.
	 * @param id 
	 * @return NacionalidadModel model
	 */
	public abstract NacionalidadModel findNacionalidadModelByIdModel(int id);
	
	public abstract NacionalidadModel findByIdModel(Nacionalidad nacionalidad);
	
}
