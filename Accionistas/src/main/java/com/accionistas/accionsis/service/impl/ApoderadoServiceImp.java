package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.AccionistaConverter;
import com.accionistas.accionsis.component.ApoderadoConverter;
import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Apoderado;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.ApoderadoModel;
import com.accionistas.accionsis.repository.ApoderadoJpaRepository;
import com.accionistas.accionsis.service.ApoderadoService;

/**
 * Clase ApoderadoServiceImp.
 */
@Service("apoderadoServiceImp")
public class ApoderadoServiceImp implements ApoderadoService{

/** LOG constante. */
private static final Log LOG = LogFactory.getLog(ApoderadoServiceImp.class);
	
	/** The apoderado jpa repository. */
	@Autowired
	@Qualifier("apoderadoJpaRepository")
	private ApoderadoJpaRepository apoderadoJpaRepository;
	
	/** The apoderado converter. */
	@Autowired
	@Qualifier("apoderadoConverter")
	private ApoderadoConverter apoderadoConverter;
	
	@Autowired
	@Qualifier("accionistaConverter")
	private AccionistaConverter accionistaConverter;
	
	/**
	 * Metodo para agregar apoderados
	 * @Param apoderadoModel
	 */
	@Override
	public ApoderadoModel addApoderado(ApoderadoModel apoderadoModel) {
		LOG.info("Call: " + "addApoderado()" +" Desde ApoderadoService");
		Apoderado apoderado= apoderadoJpaRepository.save(apoderadoConverter.modelo2Entidad(apoderadoModel));
		return apoderadoConverter.entidad2Model(apoderado);
	}

	/** 
	 * Metodo para listar apoderados
	 */
	@Override
	public List<ApoderadoModel> listAllApoderados() {
		LOG.info("Call: " + "listAllApoderados()");
		List<Apoderado> apoderados = apoderadoJpaRepository.findAllByTipoApoderado(0);
		List<ApoderadoModel> apoderadosModel = new ArrayList<ApoderadoModel>();
		for (Apoderado apoderado: apoderados) {
			apoderadosModel.add(apoderadoConverter.entidad2Model(apoderado));			
		}		 
		return apoderadosModel;
	}
	
	@Override
	public List<Apoderado> listAllApoderadosEntity() {
		LOG.info("Call: " + "listAllApoderadosEntity()");
		List<Apoderado> apoderados = apoderadoJpaRepository.findAll();
		return apoderados;
	}

	/**
	 * Metodo para encontrar apoderados por id
	 * @param id
	 */
	@Override
	public Apoderado findApoderadoById(int id) {
		return apoderadoJpaRepository.findByIdApoderado(id);
	}

	/**
	 * Metodo para remover apoderado
	 * @param id
	 */
	@Override
	public void removeApoderado(String rut) {
		Apoderado apoderado = apoderadoConverter.modelo2Entidad(findApoderadoByRut(rut));
		if (null != apoderado) {
			apoderadoJpaRepository.delete(apoderado);			
		}	
	}

	/**
	 * Metodo para encontrar apoderado por Id de apoderadoModel
	 * @param id
	 */
	@Override
	public ApoderadoModel findApoderadoByIdModel(int id) {
		return apoderadoConverter.entidad2Model(findApoderadoById(id));
	}

	@Override
	public ApoderadoModel findApoderadoByRut(String rut) {
		
		return apoderadoConverter.entidad2Model(apoderadoJpaRepository.findByRut(rut));
	}

	@Override
	public Apoderado updateApoderado(Apoderado apoderado) {
		
		return apoderadoJpaRepository.save(apoderado);
	}

	@Override
	public ApoderadoModel findApoderadoByAccionista(AccionistaModel accionista) {
		Accionista accionistaEntity = accionistaConverter.modelo2Entidad(accionista);
		Apoderado apoderadoEntity = apoderadoJpaRepository.findByAccionista(accionistaEntity);
		if(apoderadoEntity.getIdApoderado() == 0) {
			apoderadoEntity = apoderadoJpaRepository.findByTipoApoderado(1);
		}
		return apoderadoConverter.entidad2Model(apoderadoEntity);
	}
	
	
}
