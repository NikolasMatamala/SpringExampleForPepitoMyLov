package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.HerederoConverter;
import com.accionistas.accionsis.entity.Heredero;
import com.accionistas.accionsis.model.HerederoModel;
import com.accionistas.accionsis.repository.HerederoJpaRepository;
import com.accionistas.accionsis.service.HerederoService;

@Service("herederoServiceImp")
public class HerederoServiceImp implements HerederoService {

	/*
	 * Log constante
	 */
	private static final Log LOG = LogFactory.getLog(HerederoServiceImp.class);
	/*
	 * inyeccion de herederoJpaRepository
	 */
	@Autowired
	@Qualifier("herederoJpaRepository")
	private HerederoJpaRepository herederoJpaRepository;
	/*
	 * Inyeccion de herederoConverter
	 */
	@Autowired
	@Qualifier("herederoConverter")
	private HerederoConverter herederoConverter;

	/*
	 * Metodo para agregar Heredero
	 */
	@Override
	public HerederoModel addHeredero(HerederoModel herederoModel) {
		LOG.info("Call: " + "addHeredero()" + " Desde HerederoService");
		Heredero heredero = herederoJpaRepository.save(herederoConverter.modelo2Entidad(herederoModel));
		return herederoConverter.entidad2Modelo(heredero);
	}

	@Override
	public List<HerederoModel> listAllHeredero() {
		LOG.info("Call: " + "listAllHeredero() -- desde HerederoService");
		List<Heredero> herederos = herederoJpaRepository.findAll();
		List<HerederoModel> herederoModel = new ArrayList<HerederoModel>();
		for (Heredero heredero : herederos) {
			herederoModel.add(herederoConverter.entidad2Modelo(heredero));
		}
		return herederoModel;
	}

	@Override
	public Heredero findHerederoByRut(String rut) {
		return herederoJpaRepository.findByRut(rut);
	}

	@Override
	public HerederoModel findHerederoModelByRutModel(String rut) {
		return herederoConverter.entidad2Modelo(findHerederoByRut(rut));
	}

	@Override
	public void removeHeredero(String rut) {
		LOG.info("Call: " + "removeHeredero() -- desde HerederoService");
		Heredero heredero = findHerederoByRut(rut);
		if (null != heredero) {
			herederoJpaRepository.delete(heredero);
		}	

	}

	@Override
	public List<HerederoModel> listAllHerederos_active() {
		LOG.info("Call: " + "listAllHeredero() -- desde AccionistaService");
		List<Heredero> herederos = herederoJpaRepository.findAll();
		List<HerederoModel> herederoModel = new ArrayList<HerederoModel>();
		for (Heredero heredero : herederos) {
			if (heredero.getEstado() == 1) {
				herederoModel.add(herederoConverter.entidad2Modelo(heredero));
			}
		}
		return herederoModel;
	}

	@Override
	public Heredero updateHeredero(Heredero heredero) {
		return herederoJpaRepository.save(heredero);
	}

	@Override
	public Heredero findHerederoByidHeredero(int id) {
		// TODO Auto-generated method stub
		return herederoJpaRepository.findByidHeredero(id);
	}
}
