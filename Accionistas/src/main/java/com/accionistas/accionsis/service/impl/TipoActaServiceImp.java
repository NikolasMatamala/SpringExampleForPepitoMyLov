package com.accionistas.accionsis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.TipoActaConverte;
import com.accionistas.accionsis.entity.TipoActa;
import com.accionistas.accionsis.repository.TipoActaJpaRepository;
import com.accionistas.accionsis.service.TipoActaService;

@Service("tipoActaServiceImp")
public class TipoActaServiceImp implements TipoActaService{
	
	@Autowired
	@Qualifier("tipoActaJpaRepository")
	private TipoActaJpaRepository tipoActaJpaRepository;
	
	@Autowired
	@Qualifier("tipoActaConverter")
	private TipoActaConverte tipoActaConverter;

	@Override
	public TipoActa findTipoActaByidActa(int idTipoActa) {
		return tipoActaJpaRepository.findByidTipoActa(idTipoActa);
	}

	@Override
	public List<TipoActa> listAllTipoActa() {
		return tipoActaJpaRepository.findAll();
	}

}
