package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.JuntaAccionistaConverter;
import com.accionistas.accionsis.entity.JuntaAccionista;
import com.accionistas.accionsis.model.JuntaAccionistaModel;
import com.accionistas.accionsis.repository.JuntaAccionistaJpaRepository;
import com.accionistas.accionsis.service.JuntaAccionistaService;

@Service("juntaAccionistaServiceImp")
public class JuntaAccionistaServiceImp implements JuntaAccionistaService{

	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(AccionistaServiceImp.class);
	
	/** The junta accionista jpa repository. */
	@Autowired
	@Qualifier("juntaAccionistaJpaRepository")
	private JuntaAccionistaJpaRepository juntaAccionistaJpaRepository;
	
	/** The junta accionista converter. */
	@Autowired
	@Qualifier("juntaAccionistaConverter")
	private JuntaAccionistaConverter juntaAccionistaConverter;
		
	
	@Override
	public JuntaAccionistaModel addJuntaAcconista(JuntaAccionistaModel juntaAccionistaModel) {
		LOG.info("Call: " + "addJuntaAccionista()" +" Desde JuntaAccionistaService");
		JuntaAccionista juntaAccionista = juntaAccionistaJpaRepository.save(juntaAccionistaConverter.modelo2Entidad(juntaAccionistaModel));
		return juntaAccionistaConverter.entidad2Model(juntaAccionista);	
	}

	@Override
	public List<JuntaAccionistaModel> listAllJuntasAccionistas() {
		LOG.info("Call: " + "listAllJuntasAccionistas() -- desde juntaAccionistaService");
		List<JuntaAccionista> juntaAccionistas = juntaAccionistaJpaRepository.findAll();
		List<JuntaAccionistaModel> juntaAccionistaModel = new ArrayList<JuntaAccionistaModel>();
		for (JuntaAccionista juntaAccionista : juntaAccionistas) {
			juntaAccionistaModel.add(juntaAccionistaConverter.entidad2Model(juntaAccionista));
		}		 
		return juntaAccionistaModel;
	}

	@Override
	public JuntaAccionista findJuntaAccionistaById(int id) {
		return juntaAccionistaJpaRepository.findByidJuntaAccionista(id);
	}

	@Override
	public JuntaAccionistaModel findJuntaAccionistaByIdModel(int id) {
		return juntaAccionistaConverter.entidad2Model(findJuntaAccionistaById(id));
	}

	@Override
	public String updateEstadoJunta(JuntaAccionistaModel juntaAccionistaModel) {
		String mensaje = "Error";
		
		if(null != juntaAccionistaJpaRepository.save(juntaAccionistaConverter.modelo2Entidad(juntaAccionistaModel))) {
			mensaje = "Junta n° "+juntaAccionistaModel.getIdJuntaAccionistaModel() + " actualizada correctamente";
		}else {
			mensaje = "Un error ocurrio al actualizar la junta n° "+juntaAccionistaModel.getIdJuntaAccionistaModel();
		}
		return mensaje;
	}

	@Override
	public JuntaAccionistaModel updateJunta(JuntaAccionistaModel juntaAccionistaModel) {
		// TODO Auto-generated method stub
		return juntaAccionistaConverter.entidad2Model(juntaAccionistaJpaRepository.save(juntaAccionistaConverter.modelo2Entidad(juntaAccionistaModel)));
	}
	

}
