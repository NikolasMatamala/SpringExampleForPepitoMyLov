package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Apoderado;
import com.accionistas.accionsis.model.AccionistaModel;
import com.accionistas.accionsis.model.ApoderadoModel;

/**
 * Interface ApoderadoService.
 */
public interface ApoderadoService {
	
	/**
	 * Agregar apoderado.
	 * @param apoderadoModel 
	 * @return apoderado model
	 */
	public abstract ApoderadoModel addApoderado(ApoderadoModel apoderadoModel);
	
	/**
	 * Listar apoderados.
	 * @return list de apoderados
	 */
	public abstract List<ApoderadoModel> listAllApoderados();

	public abstract List<Apoderado> listAllApoderadosEntity();
	
	/**
	 * Buscar apoderado por rut.
	 * @param id 
	 * @return apoderado
	 */
	public abstract Apoderado findApoderadoById(int id);

	/**
	 * Buscar apoderado por accionista.
	 * @param accionista
	 * @return apoderado
	 */
	public abstract ApoderadoModel findApoderadoByAccionista(AccionistaModel accionista);
	/**
	 * Eliminar apoderado.
	 * @param rut
	 */
	public abstract void removeApoderado(String rut);
	
	/**
	 * Entontrar apoderado a travez de id model.
	 * @param id 
	 * @return apoderado model
	 */
	public abstract ApoderadoModel findApoderadoByIdModel(int id);
	
	public abstract ApoderadoModel findApoderadoByRut(String rut);
	
	public abstract Apoderado updateApoderado(Apoderado apoderado);
	
	

}
