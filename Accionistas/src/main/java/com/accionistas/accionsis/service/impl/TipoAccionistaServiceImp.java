package com.accionistas.accionsis.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.accionistas.accionsis.component.TipoAccionistaConverter;
import com.accionistas.accionsis.entity.TipoAccionista;
import com.accionistas.accionsis.model.TipoAccionistaModel;
import com.accionistas.accionsis.repository.TipoAccionistaJpaRepository;
import com.accionistas.accionsis.service.TipoAccionistaService;

@Service("tipoAccionistaServiceImp")
public class TipoAccionistaServiceImp implements TipoAccionistaService {
	
	/** LOG constante. */
	private static final Log LOG = LogFactory.getLog(TipoAccionistaServiceImp.class);
	
	/** The tipoAccionista jpa repository. */
	@Autowired
	@Qualifier("tipoAccionistaJpaRepository")
	private TipoAccionistaJpaRepository tipoAccionistaJpaRepository;
	
	/** The tipoAccionista converter. */
	@Autowired
	@Qualifier("tipoAccionistaConverter")
	private TipoAccionistaConverter tipoAccionistaConverter;

	@Override
	public List<TipoAccionistaModel> listAllTiposAccionistas() {
		LOG.info("Call: " + "listAllTiposAccionistas() -- desde service");
		List<TipoAccionista> tiposAccionistas = tipoAccionistaJpaRepository.findAll();
		List<TipoAccionistaModel> tiposAccionistasModel = new ArrayList<TipoAccionistaModel>();
		for (TipoAccionista tipoAccionista: tiposAccionistas ) {
			tiposAccionistasModel.add(tipoAccionistaConverter.entidad2Model(tipoAccionista));
		}		 
		return tiposAccionistasModel;
		
	}

	@Override
	public TipoAccionista findTipoAccionistaById(int id) {
		return tipoAccionistaJpaRepository.findByIdTipoAccionista(id);
	}

	@Override
	public TipoAccionistaModel findTipoAccionistaModelByIdModel(int id) {
		return tipoAccionistaConverter.entidad2Model(findTipoAccionistaById(id));
	}

	@Override
	public List<TipoAccionista> listAllTipoAccionista() {
		List<TipoAccionista> tA = tipoAccionistaJpaRepository.findAll();
		return tA;
	}

}
