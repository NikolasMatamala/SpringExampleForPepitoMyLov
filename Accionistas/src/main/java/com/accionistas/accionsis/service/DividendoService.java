package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.entity.Dividendo;
import com.accionistas.accionsis.model.DividendoModel;


/**
	 * Interface DividendoService.
	 */


public interface DividendoService {
	
		public abstract DividendoModel addDividendo(DividendoModel dividendoModel);
		
		/**
		 * Listar dividendo.
		 * @return Lista de dividendo
		 */
		public abstract List<DividendoModel> listAllDividendo();
		
		public abstract List<Dividendo> listAllDividendoEntity();

		
		/**
		 * Buscar dividendo por rut.
		 * @param rut 
		 * @return dividendo
		 */
		public abstract Dividendo findDividendoByAccionista(Accionista accionista);
		
		/**
		 * Eliminar dividendo
		 * @param rut
		 */
		public abstract void removeDividendo(Accionista accionista);
		
}
