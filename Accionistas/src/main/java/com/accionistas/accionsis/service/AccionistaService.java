package com.accionistas.accionsis.service;

import java.util.List;

import com.accionistas.accionsis.entity.Accionista;
import com.accionistas.accionsis.model.AccionistaModel;

/**
 * Interface AccionistaService.
 */
public interface AccionistaService {
	
	/**
	 * agregar accionista.
	 * @param accionistaModel 
	 * @return accionista model
	 */
	public abstract Accionista addAccionista(Accionista accionista);
	
	/**
	 * Listar accionistas.
	 * @return Lista de accionistas
	 */
	public abstract List<AccionistaModel> listAllAccionista();

	
	/**
	 * Buscar accionista a travez de rut.
	 * @param rut 
	 * @return accionista
	 */
	public abstract Accionista findAccionistaByAccionista(Accionista accionista);
	
	/**
	 * Eliminar accionista.
	 * @param rut 
	 */
	public abstract void removeAccionista(Accionista accionista);
	
	/**
	 * Buscar accionista a travez de rut model.
	 * @param accionista 
	 * @return accionista model
	 */
	Accionista findAccionistaByRut(Accionista accionista);
	Accionista findAccionistaByRut(String rut);
	AccionistaModel findAccionistaModelByRut(String rut);
	
	
	public abstract Accionista findAccionistaByAccionistaModel(Accionista accionista);

	public abstract List<AccionistaModel> listAllAccionista_active();
	
	public abstract Accionista updateAccionista(Accionista accionista);
	
	public abstract AccionistaModel findAccionistaModelById(int id);
	
	public abstract List<Accionista> listAllAccionistaEntity();
	
	public abstract List<Accionista> listAllAccionistaEntity_active();
	
	List<AccionistaModel> listAccionistasForGraficoTop10AccionTypeA();
	List<AccionistaModel> listAccionistasForGraficoTop10AccionTypeB();
	
	List<Accionista> listAccionistasForTop10AccionTypeA();
	List<Accionista> listAccionistasForTop10AccionTypeB();

}
